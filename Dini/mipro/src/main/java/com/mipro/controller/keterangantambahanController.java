package com.mipro.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mipro.model.keterangantambahanModel;
import com.mipro.service.keterangantambahanService;

@Controller
public class keterangantambahanController {

	@Autowired
	private keterangantambahanService ks;
	
	@RequestMapping("keterangantambahan")
	public String keterangantambahan() {
		return "keterangantambahan/keterangantambahan";
	}
	
	@RequestMapping("keterangantambahanlist")
	public String keterangantambahanlist(Model model) {
		List<keterangantambahanModel>semuaketerangantambahan=ks.semuaketerangantambahan();
		model.addAttribute("semuaketerangantambahan", semuaketerangantambahan);
		return "keterangantambahan/keterangantambahanlist";
	}
	
	@RequestMapping("keterangantambahanadd")
	public String keterangantambahanadd() {
		return "keterangantambahan/keterangantambahanadd";
	}
	
	@ResponseBody
	@RequestMapping("simpanketerangantambahan")
	public String simpanketerangantambahan(@ModelAttribute()keterangantambahanModel keterangantambahanModel) {
		ks.simpanketerangantambahan(keterangantambahanModel);
		return "";
	}
	
	@ResponseBody
	@RequestMapping("ubahketerangantambahan")
	public String ubahketerangantambahan(@ModelAttribute()keterangantambahanModel keterangantambahanModel) {
		ks.simpanketerangantambahan(keterangantambahanModel);
		return "";
	}
	
	@ResponseBody
	@RequestMapping("hapusketerangantambahan/{ids}")
	public String hapusketerangantambahan (@PathVariable(name="ids") Long ids) {
		ks.hapusketerangantambahan(ids);
		return "";
	}
	
	@RequestMapping("keterangantambahanedit/{ids}")
	public String keterangantambahanedit(Model model,
						@PathVariable(name="ids") Long ids) {
		keterangantambahanModel keterangantambahanbyid = ks.keterangantambahanbyid(ids);
		model.addAttribute("keterangantambahanbyid", keterangantambahanbyid);
		return "keterangantambahan/keterangantambahanedit";
	}

	@RequestMapping("keterangantambahandelete/{ids}")
	public String keterangantambahandelete(Model model,
						@PathVariable(name="ids") Long ids) {
		keterangantambahanModel keterangantambahanbyid = ks.keterangantambahanbyid(ids);
		model.addAttribute("keterangantambahanbyid", keterangantambahanbyid);
		return "keterangantambahan/keterangantambahandelete" ;
	}
}
