package com.mipro.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mipro.dto.joinket;
import com.mipro.dto.joinlevelId;
import com.mipro.dto.joints;
import com.mipro.model.jenjangpendidikanModel;
import com.mipro.model.keterangantambahanModel;
import com.mipro.model.pendidikanModel;
import com.mipro.model.timesheetModel;
import com.mipro.model.timesheetassesmentModel;
import com.mipro.service.clientService;
import com.mipro.service.jenjangpendidikanService;
import com.mipro.service.pendidikanService;
import com.mipro.service.placementService;
import com.mipro.service.timesheetService;
import com.mipro.service.timesheetassesmentService;

@Controller
public class timesheetController {

	@Autowired
	private timesheetService as;
	@Autowired
	private timesheetassesmentService bs;
	
	
	@RequestMapping("timesheet")
	public String timesheet(Model model) {
		isitahun(model);
		
		return "timesheet/timesheet";
	}
	
	//join
	@RequestMapping("timesheetlist/{yr}/{mnt}")
	public String timesheetlist(Model model,
								@PathVariable(name="yr")Integer yr,
								@PathVariable(name="mnt")Integer mnt) {
		List<joints>semuatimesheetassesment=as.semuatimesheet(yr,mnt);
		model.addAttribute("semuatimesheetassesment", semuatimesheetassesment);
		
		
		return "timesheet/timesheetlist";
	}
	
	public void isitahun(Model model) {
		Calendar cal = Calendar.getInstance();
		int thn = cal.get(Calendar.YEAR);
		List<Integer> tahunk = new ArrayList();
		model.addAttribute("tahun", tahunk);
		thn = thn - 10;
		for (int i = 0; i <= 10; i++) {
			tahunk.add(thn + i);
		}

		model.addAttribute("tahun", tahunk);
	}
	
	@RequestMapping("detail/{ids}")
	public String detail(Model model, 
						@PathVariable(name="ids")Long ids) {
		joints ts = as.semuatimesheetassesmentJ1(ids);
		model.addAttribute("ts", ts);
		return "timesheet/detail";
	}
	
	@RequestMapping("penilaian/{yr}/{mnt}/{idtsa}")
	public String penilaian(@PathVariable(name="yr")Integer yr,
			@PathVariable(name="mnt")Integer mnt, 
			@PathVariable(name="idtsa")Integer idtsa,Model model) {
		model.addAttribute("yr", yr);
		model.addAttribute("mnt", mnt);
		model.addAttribute("idtsa", idtsa);
		return "timesheet/penilaian";
	}
	
	
	//penilaian
	@ResponseBody
	@RequestMapping("simpanpenilaian/{yr}/{mnt}")
	public String simpanpenilaian(@ModelAttribute()timesheetassesmentModel timesheetassesmentModel,
			@PathVariable(name="yr")Integer yr,
			@PathVariable(name="mnt")Integer mnt){
		Long loguser = (long) 111;
		timesheetassesmentModel.setIsDelete(false);
//		timesheetassesmentModel.setPlacementId(loguser);
		timesheetassesmentModel.setCreatedBy(loguser);
		Timestamp wktsekarang = new Timestamp(System.currentTimeMillis());
		timesheetassesmentModel.setCreatedOn(wktsekarang);
		bs.simpantimesheetassesment(timesheetassesmentModel);
		return "";
	}
	
	@ResponseBody
	@RequestMapping("simpantimesheet/{id}/{yr}/{mnt}")
	public String simpantimesheet(
								@PathVariable(name="id")Long id,
								@PathVariable(name="yr")Integer yr,
								@PathVariable(name="mnt")Integer mnt) {
		
	
		as.submit(id, mnt , yr);

		return "";
	}
	
	
	//tolak
	@ResponseBody
	@RequestMapping("tolak/{id}/{yr}/{mnt}")
	public String tolak(
								@PathVariable(name="id")Long id,
								@PathVariable(name="yr")Integer yr,
								@PathVariable(name="mnt")Integer mnt) {
		
	
		as.tolak(id, mnt , yr);

		return "";
	}
	

	
}
