package com.mipro.dto;

import java.util.Date;

public class joints {

	private Long id;
	private String status;
	private Date timesheetDate;
	private String start;
	private String end;
	private Boolean overtime;
	private String startOt;
	private String endOt;
	private String activity;
	private String name;
	private Long placementId;
	public joints(Long id, String status, Date timesheetDate, String start, String end, Boolean overtime,
			String startOt, String endOt, String activity, String name, Long placementId) {
		this.id = id;
		this.status = status;
		this.timesheetDate = timesheetDate;
		this.start = start;
		this.end = end;
		this.overtime = overtime;
		this.startOt = startOt;
		this.endOt = endOt;
		this.activity = activity;
		this.name = name;
		this.placementId = placementId;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getTimesheetDate() {
		return timesheetDate;
	}
	public void setTimesheetDate(Date timesheetDate) {
		this.timesheetDate = timesheetDate;
	}
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		this.start = start;
	}
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	public Boolean getOvertime() {
		return overtime;
	}
	public void setOvertime(Boolean overtime) {
		this.overtime = overtime;
	}
	public String getStartOt() {
		return startOt;
	}
	public void setStartOt(String startOt) {
		this.startOt = startOt;
	}
	public String getEndOt() {
		return endOt;
	}
	public void setEndOt(String endOt) {
		this.endOt = endOt;
	}
	public String getActivity() {
		return activity;
	}
	public void setActivity(String activity) {
		this.activity = activity;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getPlacementId() {
		return placementId;
	}
	public void setPlacementId(Long placementId) {
		this.placementId = placementId;
	}
	
	
}
