package com.mipro.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "x_employee")
public class employeeModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", length=11, nullable=false)
	private Long id;
	
	@Column(name = "biodata_id", length=11, nullable=false)
	private Long biodataId;

	
	@Column(name = "is_idle", nullable=true)
	private Boolean isIdle;
	
	@Column(name = "is_ero",  nullable=true)
	private Boolean isEro;
	
	@Column(name = "is_user_client", nullable=true)
	private Boolean isUserClient;
	
	@Column(name = "ero_email", length=100, nullable=true)
	private String eroEmail;
	
	@Column(name = "created_by", length=11, nullable=false)
	private Long createdBy;
	
	@Column(name = "created_on",  nullable=false)
	private Date createdOn;
	
	@Column(name = "modified_by", length=11, nullable=true)
	private Long modifiedBy;
	
	@Column(name = "modified_on", nullable=true)
	private Date modifiedOn;
	
	@Column(name = "deleted_by", length=11, nullable=true)
	private Long deletedBy;
	
	@Column(name = "deleted_on",  nullable=true)
	private Date deletedOn;
	
	@Column(name = "is_delete", nullable=false, columnDefinition = "boolean default false")
	private Boolean isDelete;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getBiodataId() {
		return biodataId;
	}

	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}

	public Boolean getIsIdle() {
		return isIdle;
	}

	public void setIsIdle(Boolean isIdle) {
		this.isIdle = isIdle;
	}

	public Boolean getIsEro() {
		return isEro;
	}

	public void setIsEro(Boolean isEro) {
		this.isEro = isEro;
	}

	public Boolean getIsUserClient() {
		return isUserClient;
	}

	public void setIsUserClient(Boolean isUserClient) {
		this.isUserClient = isUserClient;
	}

	public String getEroEmail() {
		return eroEmail;
	}

	public void setEroEmail(String eroEmail) {
		this.eroEmail = eroEmail;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	
	
}
