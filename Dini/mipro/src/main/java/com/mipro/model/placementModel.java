package com.mipro.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "x_placement")
public class placementModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", length=11, nullable=false)
	private Long id;
	
	@Column(name = "client_id", length=11, nullable=false)
	private Long clientId;
	
	@Column(name = "employee_id", length=11, nullable=false)
	private Long employeeId;
	
	@Column(name = "is_placement_active", nullable=false)
	private Boolean isPlacementActive;
	
	@Column(name = "created_by", length=11, nullable=false)
	private Long createdBy;
	
	@Column(name = "created_on",  nullable=false)
	private Date createdOn;
	
	@Column(name = "modified_by", length=11, nullable=true)
	private Long modifiedBy;
	
	@Column(name = "modified_on", nullable=true)
	private Date modifiedOn;
	
	@Column(name = "deleted_by", length=11, nullable=true)
	private Long deletedBy;
	
	@Column(name = "deleted_on",  nullable=true)
	private Date deletedOn;
	
	@Column(name = "is_delete", nullable=false, columnDefinition = "boolean default false")
	private Boolean isDelete;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getClientId() {
		return clientId;
	}

	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public Boolean getIsPlacementActive() {
		return isPlacementActive;
	}

	public void setIsPlacementActive(Boolean isPlacementActive) {
		this.isPlacementActive = isPlacementActive;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	

}
