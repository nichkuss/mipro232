package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.mipro.dto.bid;
import com.mipro.dto.joinket;
import com.mipro.model.keterangantambahanModel;
import com.mipro.model.referensiModel;

public interface keterangantambahanRepository extends JpaRepository<keterangantambahanModel, Long> {

	
	@Query("select k from keterangantambahanModel k order by k.id")
	List<keterangantambahanModel>semuaketerangantambahan();
	
	@Query("select new com.mipro.dto.joinket(k.id, k.biodataId, k.emergencyContactName, k.emergencyContactPhone, "
			+ "k.expectedSalary, k.isNegotiable, k.startWorking, k.isApplyOtherPlace, k.isReadyToOutoftown, k.applyPlace, "
			+ "k.selectionPhase, k.isEverBadlySick, k.diseaseName, k.diseaseTime, k.isEverPsychotest, k.psychotestNeeds, "
			+ "k.psychotestTime, k.requirementesRequired, k.otherNotesotherNotes) from keterangantambahanModel k INNER join "
			+ "pelamarModel p on k.biodataId = p.id where k.isDelete=false and k.biodataId=?1 order by k.id")
	List<joinket>semuaketeranganJ(Long ids);
	
	@Query("select k from keterangantambahanModel k where k.biodataId=?1")
	Long biodataid(Long ids);
	
	@Query("select k from keterangantambahanModel k where k.id=?1")
	keterangantambahanModel keterangantambahanbyid(Long ids);
	
	@Modifying
	@Query("update keterangantambahanModel set isDelete=true where id=?1")
	int deleteflag(Long ids);
}
