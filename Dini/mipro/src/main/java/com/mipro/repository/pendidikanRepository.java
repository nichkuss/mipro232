package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.mipro.dto.joinlevelId;
import com.mipro.model.pendidikanModel;


public interface pendidikanRepository extends JpaRepository<pendidikanModel, Long> {

	@Query("select k from pendidikanModel k where k.isDelete=false order by k.id")
	List<pendidikanModel>semuapendidikan();
	
	@Query("select new com.mipro.dto.joinlevelId(k.id, k.biodataId, k.schoolname, j.name, k.entryYear, "
			+ "k.graduationYear, k.major, k.gpa) from pendidikanModel k INNER JOIN jenjangpendidikanModel  j "
			+ "on k.educationLevelId =j.id INNER JOIN pelamarModel p on k.biodataId = p.id where k.isDelete= false and k.biodataId=?1")
	List<joinlevelId>semuapendidikanJ(Long ids);
	
	@Query("select k from pendidikanModel k where k.id=?1")
	pendidikanModel pendidikanbyid(Long ids);
	
//	@Query("select k from pendidikanModel k where k.id=?1")
//	List<pendidikanModel> pendidikanbyid2(Long ids);
	
	@Modifying
	@Query("update pendidikanModel set isDelete=true where id=?1")
	int deleteflag(Long ids);

}
