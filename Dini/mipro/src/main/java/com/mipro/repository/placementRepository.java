package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mipro.model.clientModel;
import com.mipro.model.jenjangpendidikanModel;
import com.mipro.model.placementModel;

public interface placementRepository extends JpaRepository<placementModel, Long> {

	@Query("select j from placementModel j where j.isDelete=false")
	List<placementModel>semuaplacement();
}
