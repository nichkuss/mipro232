package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.mipro.dto.bid;
import com.mipro.model.pendidikanModel;
import com.mipro.model.referensiModel;

public interface referensiRepository extends JpaRepository<referensiModel, Long>{

	@Query("select k from referensiModel k where isDelete=false order by k.id")
	List<referensiModel>semuareferensi();
	
	//biodata
	@Query("select new com.mipro.dto.bid(k.id, k.biodataId,k.name, k.position, k.addressPhone, k.relation) from referensiModel k INNER join pelamarModel p on k.biodataId = p.id where k.isDelete=false and k.biodataId=?1 order by k.id")
	List<bid>semuareferensiJ(Long ids);
	
	//biodata
	@Query("select k from referensiModel k where k.biodataId=?1")
	Long biodataid(Long ids);
	
	@Query("select k from referensiModel k where k.id=?1")
	referensiModel referensibyid(Long ids);
	
	@Modifying
	@Query("update referensiModel set isDelete=true where id=?1")
	int deleteflag(Long ids);
		
}
