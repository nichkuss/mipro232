package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.model.jenjangpendidikanModel;
import com.mipro.repository.jenjangpendidikanRepository;

@Service
@Transactional
public class jenjangpendidikanService {

	@Autowired
	private jenjangpendidikanRepository j;
	
	public 	List<jenjangpendidikanModel>semuajenjangpendidikan(){
		return j.semuajenjangpendidikan();
	}
}
