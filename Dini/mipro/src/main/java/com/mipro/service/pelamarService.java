package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.model.pelamarModel;
import com.mipro.repository.pelamarRepository;

@Service
@Transactional
public class pelamarService {

	@Autowired
	private pelamarRepository pr;

	public List<pelamarModel> semuapelamar() {
		return pr.semuapelamar();
	}
	
	public pelamarModel pelamarbyid(long ids) {
		return pr.pelamarbyid(ids);
	}
}
