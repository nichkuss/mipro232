package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.dto.joinlevelId;
import com.mipro.model.pendidikanModel;
import com.mipro.repository.pendidikanRepository;


@Service
@Transactional
public class pendidikanService {

	@Autowired
	private pendidikanRepository pr;
	
	public List<pendidikanModel>semuapendidikan(){
		return pr.semuapendidikan();
	}
	
	public pendidikanModel simpanpendidikan(pendidikanModel pendidikanModel) {
		return pr.save(pendidikanModel);
	}
	
	public pendidikanModel pendidikanbyid(Long ids) {
		return pr.pendidikanbyid(ids);
	}
	
	public void hapuspendidikan(Long ids) {
		pr.deleteflag(ids);
	}
	
	public List<joinlevelId>semuapendidikanJ(Long ids){
		return pr.semuapendidikanJ(ids);
	}
	
}
