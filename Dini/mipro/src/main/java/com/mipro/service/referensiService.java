package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.dto.bid;
import com.mipro.model.pendidikanModel;
import com.mipro.model.referensiModel;
import com.mipro.repository.pendidikanRepository;
import com.mipro.repository.referensiRepository;

@Service
@Transactional
public class referensiService {

	@Autowired
	private referensiRepository rr;
	
	public List<referensiModel>semuareferensi(){
		return rr.semuareferensi();
	}
	
	public referensiModel simpanreferensi(referensiModel referensiModel) {
		return rr.save(referensiModel);
	}
	
	public referensiModel referensibyid(Long ids) {
		return rr.referensibyid(ids);
	}
	
	public void hapusreferensi(Long ids) {
		rr.deleteflag(ids);
	}
	
	//biodata
	public List<bid> semuareferensiJ(Long ids){
		return rr.semuareferensiJ(ids);
	}
	
	public Long biodataid(Long ids) {
		return rr.biodataid(ids);
	}
	
}
