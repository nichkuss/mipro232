package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.dto.joinsend;
import com.mipro.repository.sendRepository;

@Service
@Transactional
public class sendService {

	@Autowired
	private sendRepository tr;
	
	
	public List<joinsend>semuatimesheetJ(int year,int month){
		return tr.semuatimesheetJ(year, month);
	}
	
	public int submit(Integer Bulan, Integer tahun) {
		return tr.submit(Bulan, tahun);
	}

	public joinsend semuatimesheetJ1(int year,int month) {
		return tr.semuatimesheetJ1(year, month);
	}
}
