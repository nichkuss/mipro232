package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.dto.joints;
import com.mipro.model.jenjangpendidikanModel;
import com.mipro.model.timesheetModel;
import com.mipro.model.timesheetassesmentModel;
import com.mipro.repository.jenjangpendidikanRepository;
import com.mipro.repository.timesheetRepository;
import com.mipro.repository.timesheetassesmentRepository;

@Service
@Transactional
public class timesheetassesmentService {
	
	@Autowired
	private timesheetassesmentRepository tr;
	
	public List<timesheetassesmentModel>semuatimesheetassesment(){
		return tr.semuatimesheetassesment();
	}
	
	public timesheetassesmentModel simpantimesheetassesment(timesheetassesmentModel timesheetassesmentModel) {
		return tr.save(timesheetassesmentModel);
	}
	
	public timesheetassesmentModel timesheetassesmentbyid(Long ids) {
		return tr.timesheetassesmentbyid(ids);
	}
	
	public void hapustimesheetassesment() {
		tr.deleteflag();
	}
	public void approved(Long ids) {
		tr.user(ids);
	}
	public void rejected(Long ids){
		tr.user1(ids);
	}
	
//	public List<joints>semuatimesheetassesmentJ(int year,int month){
//		return tr.semuatimesheetassesmentJ(year,month);
//	}
//	
//	public joints semuatimesheetassesmentJ1(Long ids){
//		return tr.semuatimesheetassesmentJ1(ids);
//	}

}
