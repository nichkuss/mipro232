$('#datanewpendidikan').submit(function() {
	
	formdata = $('#datanewpendidikan').serialize();	
	var rfs= $('#biodataId').val();
	if (kumpulancek()==1) {
		alert("ada salah");
		return false;
	}
	
	$.ajax({
		url : '/simpanpendidikan',
		type : 'POST',
		data : formdata,
		success : function(result) {
			refreshpdk(rfs);
			alert("Simpan pendidikan berhasil");
			tutupmodal();
		}
	});
	return false; // agar tidak pindah halaman
});

function tutupmodal(){
	  $('#pendidikanModal').modal('hide');
	  $('#delpendidikanModal').modal('hide');
}


$('#sekolahan').keyup(function() {
	cekSekolah();
});

$('#educationLevelId').change(function() {
	jenjangpend();
});

$('#entryYear').change(function() {
	cekthn();
});

$('#graduationYear').change(function() {
	cekthn();
});


function kumpulancek() {
	var t=0;
	
	if (cekSekolah()==false) {
		t=1;
	}else{
		t=0;
	}	
	
	if (jenjangpend()==false) {
		t=1;
	}
	
//	if ( cekthn()==false) {
//		t=1;
//	}
	return t;
}

function cekSekolah() {
	var sk = true;
	if ($('#sekolahan').val()=="") {
		sk = false;
		document.getElementById("sekolahan").style.borderColor = "red";
		document.getElementById("ket1").hidden = "";
	}else{
		sk = true;
		document.getElementById("sekolahan").style.borderColor = "";
		document.getElementById("ket1").hidden = "true";
	}
	return sk;
}

function jenjangpend() {
	var jjp = true;
	if ($('#educationLevelId').val()=="") {
		jjp=false;
		document.getElementById("educationLevelId").style.borderColor = "red";
		document.getElementById("ket2").hidden = "";
	}
	else{
		jjp = true;
		document.getElementById("educationLevelId").style.borderColor = "";
		document.getElementById("ket2").hidden = "true";
	}
	return jjp;
}

function cekthn() {
	var thnmsk = $('#entryYear').val();
	var thnklr = $('#graduationYear').val();
	var cth=true;
	if (thnmsk > thnklr) {
		cth=false;
		document.getElementById("entryYear").style.borderColor = "red";
		document.getElementById("graduationYear").style.borderColor = "red";
		document.getElementById("ket3").hidden = "";
	} else{
		cth = true;
		document.getElementById("entryYear").style.borderColor = "";
		document.getElementById("graduationYear").style.borderColor = "";
		document.getElementById("ket3").hidden = "true";
	}
	return cth;
}
