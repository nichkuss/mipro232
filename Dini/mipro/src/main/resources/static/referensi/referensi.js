referensilist();

function referensilist(){
	$.ajax({
		url : '/referensilist',
		type : 'get',
		dataType : 'html',
		success : function(result) {
				$('#referensilist').html(result);
		}

	});	
}



function addform(){
	$.ajax({
		url : '/referensiadd',
		type : 'get',
		dataType : 'html',
		success : function(result) {
				$('#referensiModal').modal();
				$('#nonreferensilist').html(result);
				
		}

	});
}

function ubahket(){
	$.ajax({
		url : '/keteranganedit',
		type : 'get',
		dataType : 'html',
		success : function(result) {
				$('#keteranganModal').modal();
				$('#keteranganlist').html(result);
				
		}

	});
}

function closemodal(){
	$('#referensiModal').hide();
	$('#delreferensiModal').hide();
	$('.modal-backdrop').hide();
	$('.modal-backdrop').remove();
}

function refreshpage(){
	$.ajax({
		url : '/referensi',
		type : 'get',
		dataType : 'html',
		success : function(result) {
			closemodal();
			$('#fragment').html(result);
		}
	});
	return false;
}



