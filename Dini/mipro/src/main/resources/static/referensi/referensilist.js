
function editreferensi(t){
	var alamat = t.getAttribute("href");
	$.ajax({
		url : alamat,
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#referensiModal').modal();
			$('#nonreferensilist').html(result);
		}

	});
	
}

function editreferensi(t){
	var alamat = t.getAttribute("href");
	$.ajax({
		url : alamat,
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#editreferensiModal').modal();
			$('#noneditreferensilist').html(result);
		}

	});
	
}

function deletereferensi(t){
	var alamat = t.getAttribute("href");
	$.ajax({
		url : alamat,
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#delreferensiModal').modal();
			$('#delreferensilist').html(result);
		}

	});
	
}

function addform(){
	var ids = $('#idbiodata').val();
	$.ajax({
		url : '/referensiadd/' +ids,
		type : 'get',
		dataType : 'html',
		success : function(result) {
				$('#referensiModal').modal();
				$('#nonreferensilist').html(result);
				
		}

	});
}

function tutupmodal(){
	  $('#referensiModal').modal('hide');
	  $('#editreferensiModal').modal('hide');
	  $('#keteranganModal').modal('hide');
	  $('#delreferensiModal').modal('hide');
	  $('#keteranganlist').modal('hide');
	  $('#tampilkansemua').modal('hide');
	  
}

function editketerangan(t){
	var alamat = t.getAttribute("href");
	$.ajax({
		url : alamat,
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#keteranganModal').modal();
			$('#keteranganlist').html(result);
		}

	});
	
}

function semuaketerangan(t){
	var alamat = t.getAttribute("href");
	$.ajax({
		url : alamat,
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#tampilkansemua').modal();
			$('#listsemua').html(result);
		}

	});
	
}

function ubahket(){
	$.ajax({
		url : '/keteranganedit',
		type : 'get',
		dataType : 'html',
		success : function(result) {
				$('#keteranganModal').modal();
				$('#keteranganlist').html(result);
				
		}

	});
}


