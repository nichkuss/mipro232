package com.mipro.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mipro.model.familyTreeTypeModel;
import com.mipro.service.familyTreeTypeService;

@Controller
public class familyTreeTypeController 
{
	@Autowired
	private familyTreeTypeService ftts;
	
	@RequestMapping("familyTreeType")
	public String familyTreeType()
	{
		return "familyTreeType/familyTreeType";
	}
	
	@RequestMapping("familyTreeTypeList")
	public String familyTreeTypeList(Model model)
	{
		List <familyTreeTypeModel> semuaFamilyTreeType = ftts.semuaFamilyTreeType();
		model.addAttribute("semuaFamilyTreeType", semuaFamilyTreeType);
		return "familyTreeType/familyTreeTypeList";
	}
	
	@RequestMapping("familyTreeTypeAdd")
	public String familyTreeTypeAdd()
	{
		return "familyTreeType/familyTreeTypeAdd";
	}
	
	@ResponseBody
	@RequestMapping("simpanFamilyTreeType")
	public String simpanFamilyTreeType(@ModelAttribute() familyTreeTypeModel familyTreeTypeModel)
	{
		Long logUser = (long) 55415052;
		familyTreeTypeModel.setCreatedBy(new Long (logUser));
		familyTreeTypeModel.setCreatedOn(new Date());
		ftts.simpanFamilyTreeType(familyTreeTypeModel);
		return "";
	}
	
	@RequestMapping("familyTreeTypeEdit/{id}")
	public String familyTreeTypeEdit(Model model, @PathVariable(name = "id") Long id)
	{
		familyTreeTypeModel familyTreeTypeById = ftts.familyTreeTypeById(id);
		model.addAttribute("familyTreeTypeById", familyTreeTypeById);
		return "familyTreeType/familyTreeTypeEdit";
	}
	
	@ResponseBody
	@RequestMapping("ubahFamilyTreeType")
	public String ubahFamilyTreeType(@ModelAttribute() familyTreeTypeModel familyTreeTypeModel)
	{
		Long logUser = (long) 55415052;
		familyTreeTypeModel familyTreeTypeById = ftts.familyTreeTypeById(familyTreeTypeModel.getId());
		familyTreeTypeModel.setCreatedBy(familyTreeTypeById.getCreatedBy());
		familyTreeTypeModel.setCreatedOn(familyTreeTypeById.getCreatedOn());
		familyTreeTypeModel.setModifiedBy(new Long(logUser));
		familyTreeTypeModel.setModifiedOn(new Date());
		ftts.simpanFamilyTreeType(familyTreeTypeModel);
		return "";
	}
	
	@RequestMapping("familyTreeTypeDelete/{id}")
	public String familyTreeTypeDelete(Model model, @PathVariable(name = "id") Long id)
	{
		familyTreeTypeModel familyTreeTypeById = ftts.familyTreeTypeById(id);
		model.addAttribute("familyTreeTypeById", familyTreeTypeById);
		return "familyTreeType/familyTreeTypeDelete";
	}
	
	@ResponseBody
	@RequestMapping("hapusFamilyTreeType/{id}")
	public String hapusFamilyTreeType(@PathVariable(name = "id") Long id, @ModelAttribute familyTreeTypeModel familyTreeTypeModel)
	{
		Long logUser = (long) 55415052;
		familyTreeTypeModel familyTreeTypeById = ftts.familyTreeTypeById(id);
		familyTreeTypeById.setDeletedBy(new Long(logUser));
		familyTreeTypeById.setDeletedOn(new Date());
		ftts.simpanFamilyTreeType(familyTreeTypeById);
		ftts.hapusFamilyTreeType(id);
		return "";
	}
}
