package com.mipro.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mipro.model.familyTypeModel;
import com.mipro.service.familyTypeService;

@Controller
public class familyTypeController 
{
	@Autowired
	private familyTypeService fts;
	
	@RequestMapping("familyType")
	public String familyType()
	{
		return "familyType/familyType";
	}
	
	@RequestMapping("familyTypeList")
	public String familyTypeList(Model model)
	{
		List <familyTypeModel> semuaFamilyType = fts.semuaFamilyType();
		model.addAttribute("semuaFamilyType", semuaFamilyType);
		return "familyType/familyTypeList";
	}
	
	@RequestMapping("familyTypeListById")
	public String familyTypeListById(Model model)
	{
		List <familyTypeModel> semuaFamilyType = fts.semuaFamilyType();
		model.addAttribute("semuaFamilyType", semuaFamilyType);
		return "familyType/familyTypeListById";
	}
	
	@RequestMapping("familyTypeAdd")
	public String familyTypeAdd()
	{
		return "familyType/familyTypeAdd";
	}
	
	@ResponseBody
	@RequestMapping("simpanFamilyType")
	public String simpanFamilyType(@ModelAttribute() familyTypeModel familyTypeModel)
	{
		Long logUser = (long) 55415052;
		familyTypeModel.setCreatedBy(new Long (logUser));
		familyTypeModel.setCreatedOn(new Date());
		fts.simpanFamilyType(familyTypeModel);
		return "";
	}
	
	@RequestMapping("familyTypeEdit/{id}")
	public String familyTypeEdit(Model model, @PathVariable(name = "id") Long id)
	{
		familyTypeModel familyTypeById = fts.familyTypeById(id);
		model.addAttribute("familyTypeById", familyTypeById);
		return "familyType/familyTypeEdit";
	}
	
	@ResponseBody
	@RequestMapping("ubahFamilyType")
	public String ubahFamilyType(@ModelAttribute() familyTypeModel familyTypeModel)
	{
		Long logUser = (long) 55415052;
		familyTypeModel familyTypeById = fts.familyTypeById(familyTypeModel.getId());
		familyTypeModel.setCreatedBy(familyTypeById.getCreatedBy());
		familyTypeModel.setCreatedOn(familyTypeById.getCreatedOn());
		familyTypeModel.setModifiedBy(new Long(logUser));
		familyTypeModel.setModifiedOn(new Date());
		fts.simpanFamilyType(familyTypeModel);
		return "";
	}
	
	@RequestMapping("familyTypeDelete/{id}")
	public String familyTypeDelete(Model model, @PathVariable(name = "id") Long id)
	{
		familyTypeModel familyTypeById = fts.familyTypeById(id);
		model.addAttribute("familyTypeById", familyTypeById);
		return "familyType/familyTypeDelete";
	}
	
	@ResponseBody
	@RequestMapping("hapusFamilyType/{id}")
	public String hapusFamilyType(@PathVariable(name = "id") Long id, @ModelAttribute familyTypeModel familyTypeModel)
	{
		Long logUser = (long) 55415052;
		familyTypeModel familyTypeById = fts.familyTypeById(id);
		familyTypeById.setDeletedBy(new Long(logUser));
		familyTypeById.setDeletedOn(new Date());
		fts.simpanFamilyType(familyTypeById);
		fts.hapusFamilyType(id);
		return "";
	}
}
