package com.mipro.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mipro.dto.bid;
import com.mipro.model.organisasiModel;
import com.mipro.service.organisasiService;

@Controller
public class organisasiController 
{
	@Autowired
	private organisasiService os;
	
	@RequestMapping("organisasi")
	public String organisasi()
	{
		return "organisasi/organisasi";
	}
	
	@RequestMapping("organisasiList/{id}")
	public String organisasiList(Model model, @PathVariable(name = "id") Long id)
	{
		List <bid> semuaOrganisasi = os.semuaOrganisasiJ(id);
		model.addAttribute("organisasiById", id);
		model.addAttribute("semuaOrganisasi", semuaOrganisasi);
		return "organisasi/organisasiList";
	}
	
	@RequestMapping("organisasiAdd/{id}")
	public String organisasiAdd(Model model, @PathVariable(name = "id") Long id)
	{
		isiTahunMasuk(model);
		isiTahunKeluar(model);
		
		//under testing
		model.addAttribute("id", id);
		return "organisasi/organisasiAdd";
	}
	
	@ResponseBody
	@RequestMapping("simpanOrganisasi")
	public String simpanOrganisasi(@ModelAttribute() organisasiModel organisasiModel)
	{
		Long logUser = (long) 101;
		organisasiModel.setCreatedBy(new Long(logUser));
		organisasiModel.setCreatedOn(new Date());
		os.simpanOrganisasi(organisasiModel);
		return "";
	}
	
	public void isiTahunMasuk(Model model) 
	{
		List <Integer> tahunMasuk = new ArrayList();
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		
		//dimulai dari 1990 sampai tahun sekarang
		for (int i = year; i > 1990; i--) 
		{
			tahunMasuk.add(i);
		}
		model.addAttribute("tahunMasuk", tahunMasuk);
	}
	
	public void isiTahunKeluar(Model model)
	{
		List <Integer> tahunKeluar = new ArrayList();
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		
		//dimulai dari 1990 sampai tahun sekarang
		for(int i = year; i > 1990; i--)
		{
			tahunKeluar.add(i);
		}
		model.addAttribute("tahunKeluar", tahunKeluar);
	}
	
	@RequestMapping("organisasiEdit/{id}")
	public String organisasiEdit(Model model, @PathVariable(name = "id") Long id)
	{
		isiTahunMasuk(model);
		isiTahunKeluar(model);
		organisasiModel organisasiById = os.organisasiById(id);
		model.addAttribute("organisasiById", organisasiById);
		return "organisasi/organisasiEdit";
	}
	
	@ResponseBody
	@RequestMapping("ubahOrganisasi")
	public String ubahOrganisasi(@ModelAttribute() organisasiModel organisasiModel)
	{	
		Long logUser = (long) 101;
		organisasiModel organisasiById = os.organisasiById(organisasiModel.getId());
		organisasiModel.setBiodataId(organisasiById.getBiodataId());
		organisasiModel.setCreatedBy(organisasiById.getCreatedBy());
		organisasiModel.setCreatedOn(organisasiById.getCreatedOn());
		organisasiModel.setModifiedBy(new Long(logUser));
		organisasiModel.setModifiedOn(new Date());
		os.simpanOrganisasi(organisasiModel);
		return "";
	}
	
	@RequestMapping("organisasiDelete/{id}")
	public String organisasiDelete(Model model, @PathVariable(name = "id") Long id)
	{
		isiTahunMasuk(model);
		isiTahunKeluar(model);
		
		organisasiModel organisasiById = os.organisasiById(id);
		model.addAttribute("organisasiById", organisasiById);
		return "organisasi/organisasiDelete";
	}
	
	@ResponseBody
	@RequestMapping("hapusOrganisasi/{id}")
	public String hapusOrganisasi(@PathVariable(name = "id") Long id, @ModelAttribute() organisasiModel organisasiModel)
	{
		organisasiModel organisasiById = os.organisasiById(id);
		Long logUser = (long) 101;
		organisasiById.setDeletedBy(new Long(logUser));
		organisasiById.setDeletedOn(new Date());
		os.simpanOrganisasi(organisasiById);
		os.hapusOrganisasi(id);
		return "";
	}
}