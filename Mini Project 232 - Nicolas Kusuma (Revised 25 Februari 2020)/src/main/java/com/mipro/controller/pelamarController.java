package com.mipro.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mipro.model.pelamarModel;
import com.mipro.service.pelamarService;

@Controller
public class pelamarController 
{	
	@Autowired
	private pelamarService ps;
	
	@RequestMapping("/pelamarlist/{cr}")
	public String pelamarlist(Model model, @PathVariable(name = "cr") String cr)
	{
		List <pelamarModel> semuaPelamar = ps.semuaPelamar(cr);
		model.addAttribute("semuaPelamar", semuaPelamar);
		return "pelamar/pelamarlist";
	}
	
	@RequestMapping("/pelamar")
	public String pelamar()
	{
		return "pelamar/pelamar";
	}
	
	@RequestMapping("/pelamardetail/{id}")
	public String pelamardetail(Model model, @PathVariable(name = "id") Long id)
	{
		pelamarModel pelamarById = ps.pelamarById(id);
		model.addAttribute("pelamarById", pelamarById);
		return "pelamar/pelamardetail";
	}
}
