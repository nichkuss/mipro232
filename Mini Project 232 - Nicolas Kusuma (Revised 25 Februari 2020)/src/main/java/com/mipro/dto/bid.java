package com.mipro.dto;

public class bid 
{
	private Long id;
	private Long biodataId;
	private String name;
	private String position;
	private String entryYear;
	private String exitYear;
	
	public bid(Long id, Long biodataId, String name, 
			String position, String entryYear, String exitYear)
	{
		this.id = id;
		this.biodataId = biodataId;
		this.name = name;
		this.position = position;
		this.entryYear = entryYear;
		this.exitYear = exitYear;
	}

	public Long getId() 
	{
		return id;
	}

	public void setId(Long id) 
	{
		this.id = id;
	}

	public Long getBiodataId() 
	{
		return biodataId;
	}

	public void setBiodataId(Long biodataId) 
	{
		this.biodataId = biodataId;
	}

	public String getName() 
	{
		return name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}

	public String getPosition() 
	{
		return position;
	}

	public void setPosition(String position) 
	{
		this.position = position;
	}

	public String getEntryYear() 
	{
		return entryYear;
	}

	public void setEntryYear(String entryYear) 
	{
		this.entryYear = entryYear;
	}

	public String getExitYear() 
	{
		return exitYear;
	}

	public void setExitYear(String exitYear) 
	{
		this.exitYear = exitYear;
	}
}
