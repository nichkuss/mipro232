package com.mipro.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "x_employee_training")
public class employeeTrainingModel 
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", length = 11, nullable = false)
	private Long id;
	
	@Column(name = "created_by", length = 11, nullable = false)
	private Long createdBy;
	
	@Column(name = "created_on", nullable = false)
	private Date createdOn;
	
	@Column(name = "modified_by", length = 11, nullable = true)
	private Long modifiedBy;
	
	@Column(name = "modified_on", nullable = true)
	private Date modifiedOn;
	
	@Column(name = "deleted_by", length = 11, nullable = true)
	private Long deletedBy;
	
	@Column(name = "deleted_on", nullable = true)
	private Date deletedOn;
	
	@Column(name = "is_delete", columnDefinition = "boolean default false", nullable = false)
	private boolean isDelete;
	
	@Column(name = "employee_id", length = 11, nullable = false)
	private Long employeeId;
	
	@Column(name = "training_id", length = 11, nullable = false)
	private Long trainingId;
	
	@Column(name = "training_organizer_id", length = 11, nullable = true)
	private Long trainingOrganizerId;
	
	@Column(name = "training_date", nullable = true)
	private Date trainingDate;
	
	@Column(name = "training_type_id", length = 11, nullable = true)
	private Long trainingTypeId;
	
	@Column(name = "certification_type_id", length = 11, nullable = false)
	private Long certificationTypeId;
	
	@Column(name = "status", length = 15, nullable = false)
	private String status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public Long getTrainingId() {
		return trainingId;
	}

	public void setTrainingId(Long trainingId) {
		this.trainingId = trainingId;
	}

	public Long getTrainingOrganizerId() {
		return trainingOrganizerId;
	}

	public void setTrainingOrganizerId(Long trainingOrganizerId) {
		this.trainingOrganizerId = trainingOrganizerId;
	}

	public Date getTrainingDate() {
		return trainingDate;
	}

	public void setTrainingDate(Date trainingDate) {
		this.trainingDate = trainingDate;
	}

	public Long getTrainingTypeId() {
		return trainingTypeId;
	}

	public void setTrainingTypeId(Long trainingTypeId) {
		this.trainingTypeId = trainingTypeId;
	}

	public Long getCertificationTypeId() {
		return certificationTypeId;
	}

	public void setCertificationTypeId(Long certificationTypeId) {
		this.certificationTypeId = certificationTypeId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
