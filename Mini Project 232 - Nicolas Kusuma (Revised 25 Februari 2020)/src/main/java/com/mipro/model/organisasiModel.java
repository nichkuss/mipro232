package com.mipro.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "x_organisasi")
public class organisasiModel 
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", length = 11, nullable = false)
	private Long id;
	
	@Column(name = "created_by", length = 11, nullable = false)
	private Long createdBy;
	
	@Column(name = "created_on", nullable = false)
	private Date createdOn;
	
	@Column(name = "modified_by", length = 11, nullable = true)
	private Long modifiedBy;
	
	@Column(name = "modified_on", nullable = true)
	private Date modifiedOn;
	
	@Column(name = "deleted_by", length = 11, nullable = true)
	private Long deletedBy;

	@Column(name = "deleted_on", nullable = true)
	private Date deletedOn;
	
	@Column(name = "is_delete", columnDefinition = "boolean default false", nullable = false)
	private boolean isDelete;
	
	@Column(name = "biodata_id", length = 11, nullable = false)
	private Long biodataId;
	
	@Column(name = "name", length = 100, nullable = true)
	private String name;
	
	@Column(name = "position", length = 100, nullable = true)
	private String position;
	
	@Column(name = "entry_year", length = 10, nullable = true)
	private String entryYear;
	
	@Column(name = "exit_year", length = 10, nullable = true)
	private String exitYear;
	
	@Column(name = "responsbility", length = 100, nullable = true)
	private String responsbility;
	
	@Column(name = "notes", length = 1000, nullable = true)
	private String notes;

	public Long getId() 
	{
		return id;
	}

	public void setId(Long id) 
	{
		this.id = id;
	}

	public Long getCreatedBy() 
	{
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) 
	{
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() 
	{
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) 
	{
		this.createdOn = createdOn;
	}

	public Long getModifiedBy() 
	{
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) 
	{
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() 
	{
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) 
	{
		this.modifiedOn = modifiedOn;
	}

	public Long getDeletedBy() 
	{
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) 
	{
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() 
	{
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) 
	{
		this.deletedOn = deletedOn;
	}

	public boolean isDelete() 
	{
		return isDelete;
	}

	public void setDelete(boolean isDelete) 
	{
		this.isDelete = isDelete;
	}

	public Long getBiodataId() 
	{
		return biodataId;
	}

	public void setBiodataId(Long biodataId) 
	{
		this.biodataId = biodataId;
	}

	public String getName() 
	{
		return name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}

	public String getPosition() 
	{
		return position;
	}

	public void setPosition(String position) 
	{
		this.position = position;
	}

	public String getEntryYear() 
	{
		return entryYear;
	}

	public void setEntryYear(String entryYear) 
	{
		this.entryYear = entryYear;
	}

	public String getExitYear() 
	{
		return exitYear;
	}

	public void setExitYear(String exitYear) 
	{
		this.exitYear = exitYear;
	}

	public String getResponsbility() 
	{
		return responsbility;
	}

	public void setResponsbility(String responsbility) 
	{
		this.responsbility = responsbility;
	}

	public String getNotes() 
	{
		return notes;
	}

	public void setNotes(String notes) 
	{
		this.notes = notes;
	}
}
