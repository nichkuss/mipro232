package com.mipro.model;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "x_biodata")
public class pelamarModel
{
	@Id
	@NotNull
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", length = 11, nullable = false)
	private Long id;

	@Column(name = "created_by", length = 11, nullable = false)
	private Long createdBy;

	@Column(name = "created_on", nullable = false)
	private Date createdOn;
	
	@Column(name = "fullname", length = 255, nullable = false)
	private String fullName;

	@Column(name = "nick_name", length = 100, nullable = false)
	private String nickName;

	@Column(name = "email", length = 100, nullable = false)
	private String email;

	@Column(name = "phone_number_1", length = 50, nullable = false)
	private String phoneNumber1;

	@Column(name = "school_name", length = 50, nullable = false)
	private String schoolName;

	public Long getId() 
	{
		return id;
	}

	public void setId(Long id) 
	{
		this.id = id;
	}

	public Long getCreatedBy() 
	{
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) 
	{
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() 
	{
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) 
	{
		this.createdOn = createdOn;
	}

	public String getFullName() 
	{
		return fullName;
	}

	public void setFullName(String fullName) 
	{
		this.fullName = fullName;
	}

	public String getNickName() 
	{
		return nickName;
	}

	public void setNickName(String nickName) 
	{
		this.nickName = nickName;
	}

	public String getEmail() 
	{
		return email;
	}

	public void setEmail(String email) 
	{
		this.email = email;
	}

	public String getPhoneNumber1() 
	{
		return phoneNumber1;
	}

	public void setPhoneNumber1(String phoneNumber1) 
	{
		this.phoneNumber1 = phoneNumber1;
	}

	public String getSchoolName() 
	{
		return schoolName;
	}

	public void setSchoolName(String schoolName) 
	{
		this.schoolName = schoolName;
	}
}
