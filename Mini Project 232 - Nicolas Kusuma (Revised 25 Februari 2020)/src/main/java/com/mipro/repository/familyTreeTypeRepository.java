package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.mipro.model.familyTreeTypeModel;

public interface familyTreeTypeRepository extends JpaRepository <familyTreeTypeModel, Long> 
{	
	@Query("select ftt from familyTreeTypeModel ftt where ftt.isDelete = false order by ftt.id")
	List <familyTreeTypeModel> semuaFamilyTreeType();
	
	@Query("select ftt from familyTreeTypeModel ftt where ftt.id=?1")
	familyTreeTypeModel familyTreeTypeById (Long id);
	
	@Modifying
	@Query("update familyTreeTypeModel set isDelete = true where id=?1")
	int deleteFlag(Long id);
}
