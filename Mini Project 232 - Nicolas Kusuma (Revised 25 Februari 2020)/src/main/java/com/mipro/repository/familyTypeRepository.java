package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.mipro.model.familyTypeModel;

public interface familyTypeRepository extends JpaRepository <familyTypeModel, Long>
{
	@Query("select ft from familyTypeModel ft where ft.isDelete = false order by ft.id")
	List <familyTypeModel> semuaFamilyType();
	
	@Query("select ft from familyTypeModel ft where ft.id=?1")
	familyTypeModel familyTypeById (Long id);
	
	@Modifying
	@Query("update familyTypeModel set isDelete = true where id=?1")
	int deleteFlag(Long id);
}
