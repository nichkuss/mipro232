package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.mipro.dto.bid;
import com.mipro.model.organisasiModel;

public interface organisasiRepository extends JpaRepository <organisasiModel, Long>
{
	@Query("select o from organisasiModel o where o.isDelete = false order by o.id")
	List <organisasiModel> semuaOrganisasi();
	
	@Query("select new com.mipro.dto.bid(o.id, o.biodataId, o.name, o.position, o.entryYear, o.exitYear) "
			+ "from organisasiModel o INNER JOIN pelamarModel p on o.biodataId = p.id where o.isDelete = false "
			+ "AND o.biodataId=?1 order by o.id")
	List <bid> semuaOrganisasiJ(Long id);
	
	@Query("select o from organisasiModel o where o.id=?1")
	organisasiModel organisasiById (Long id);
	
	@Modifying
	@Query("update organisasiModel set isDelete = true where id=?1")
	int deleteFlag(Long id);
}
