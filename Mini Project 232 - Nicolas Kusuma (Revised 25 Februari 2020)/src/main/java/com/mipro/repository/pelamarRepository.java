package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mipro.model.pelamarModel;

public interface pelamarRepository extends JpaRepository <pelamarModel, Long>
{
	@Query("select p from pelamarModel p where p.fullName like %?1% order by p.id")
	List <pelamarModel> semuaPelamar(String cr);
	
//	@Query("select p from pelamarModel p order by p.id")
//	List <pelamarModel> semuaPelamar();

	@Query("select p from pelamarModel p where p.id=?1")
	pelamarModel pelamarById(Long id);
}
