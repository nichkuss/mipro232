package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.model.familyTreeTypeModel;
import com.mipro.repository.familyTreeTypeRepository;

@Service
@Transactional
public class familyTreeTypeService 
{
	@Autowired
	private familyTreeTypeRepository fttr;
	
	public List <familyTreeTypeModel> semuaFamilyTreeType()
	{
		return fttr.semuaFamilyTreeType();
	}
	
	//Save by Jpa
	public familyTreeTypeModel simpanFamilyTreeType(familyTreeTypeModel familyTreeTypeModel)
	{
		return fttr.save(familyTreeTypeModel);
	}
	
	public familyTreeTypeModel familyTreeTypeById(Long id)
	{
		return fttr.familyTreeTypeById(id);
	}
	
	public void hapusFamilyTreeType(Long id)
	{
		fttr.deleteFlag(id);
	}
}
