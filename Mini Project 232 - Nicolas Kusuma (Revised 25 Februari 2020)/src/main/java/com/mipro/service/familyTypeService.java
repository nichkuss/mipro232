package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.model.familyTypeModel;
import com.mipro.repository.familyTypeRepository;

@Service
@Transactional
public class familyTypeService 
{
	@Autowired
	private familyTypeRepository ftr;
	
	public List <familyTypeModel> semuaFamilyType()
	{
		return ftr.semuaFamilyType();
	}
	
	//save by Jpa
	public familyTypeModel simpanFamilyType(familyTypeModel familyTypeModel)
	{
		return ftr.save(familyTypeModel);
	}
	
	public familyTypeModel familyTypeById(Long id)
	{
		return ftr.familyTypeById(id);
	}
	
	public void hapusFamilyType(Long id)
	{
		ftr.deleteFlag(id);
	}
}
