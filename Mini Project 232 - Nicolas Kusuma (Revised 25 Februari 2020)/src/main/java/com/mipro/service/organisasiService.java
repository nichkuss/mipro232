package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.dto.bid;
import com.mipro.model.organisasiModel;
import com.mipro.repository.organisasiRepository;

@Service
@Transactional
public class organisasiService 
{
	@Autowired
	private organisasiRepository or;
	
	public List <organisasiModel> semuaOrganisasi()
	{
		return or.semuaOrganisasi();
	}
	
	//save by Jpa
	public organisasiModel simpanOrganisasi(organisasiModel organisasiModel)
	{
		return or.save(organisasiModel);
	}
	
	public organisasiModel organisasiById(Long id)
	{
		return or.organisasiById(id);
	}
	
	public void hapusOrganisasi(Long id)
	{
		or.deleteFlag(id);
	}
	
	public List <bid> semuaOrganisasiJ(Long id)
	{
		return or.semuaOrganisasiJ(id);
	}
}
