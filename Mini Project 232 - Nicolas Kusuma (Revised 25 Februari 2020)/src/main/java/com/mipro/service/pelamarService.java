package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.model.pelamarModel;
import com.mipro.repository.pelamarRepository;

@Service
@Transactional
public class pelamarService
{
	@Autowired
	public pelamarRepository pr;

	public List <pelamarModel> semuaPelamar(String cr)
	{
		return pr.semuaPelamar(cr);
	}

	public pelamarModel pelamarById(Long id)
	{
		return pr.pelamarById(id);
	}
}
