familyTreeTypeList();

function familyTreeTypeList()
{
	$.ajax({
		url : '/familyTreeTypeList',
		type : 'get',
		dataType: 'html',
		success: function(result)
		{
			$('#familyTreeTypeList').html(result);
		}
	});
}

function addForm()
{
	$.ajax({
		url : '/familyTreeTypeAdd',
		type : 'get',
		dataType : 'html',
		success : function(result) 
		{
			$('#familyTreeTypeModal').modal();
			$('#nonFamilyTreeTypeList').html(result);	
		}
	});
}

function closeModal()
{
	$('#familyTreeTypeModal').hide();
	$('.modal-backdrop').hide();
	$('.modal-backdrop').remove();
	familyTreeTypeList();
}

function refreshPage()
{
	$.ajax({
		url : '/familyTreeType',
		type : 'get',
		dataType : 'html',
		success : function(result) 
		{
			closeModal();
			$('#fragment').html(result);
		}
	});
	return false;
}