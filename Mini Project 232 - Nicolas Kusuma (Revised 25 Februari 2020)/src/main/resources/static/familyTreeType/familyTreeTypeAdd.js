function cekHubunganKeluarga()
{
	var name = document.getElementById("name").value;
	var errorMsg = "Hubungan Keluarga Tidak Boleh Kosong";
	var warna = errorMsg.fontcolor("#CC0000");
	
	if(name = "")
	{
		$('#family_tree_type_error').html(warna);
		$('#family_tree_type_error').show();
		document.getElementById("hubunganKeluarga").style.color = "red";
		document.getElementById("name").style.borderColor = "red";
		document.getElementById("btn_simpan").disabled = "true";
	}
}

$('#dataNewFamilyTreeType').submit(function() 
{
	var hubunganKeluarga = $('#name').val();
	if(hubunganKeluarga == "")
	{
		alert("Hubungan Keluarga Harus Diisi")
		return false;
	}
	else
	{
		formdata = $('#dataNewFamilyTreeType').serialize();
		$.ajax({
			url : '/simpanFamilyTreeType',
			type : 'POST',
			data : formdata,
			success : function(result) 
			{
				alert("Hubungan Keluarga Berhasil Disimpan!");
				refreshPage();
			}
		});
	}
	return false;
});