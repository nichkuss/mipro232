function editFamilyTreeType(t)
{
	var alamat = t.getAttribute("href");
	$.ajax({
		url : alamat,
		type : 'get',
		dataType : 'html',
		success : function(result) 
		{	
			$('#familyTreeTypeModal').modal();
			$('#nonFamilyTreeTypeList').html(result);
		}
	});	
}

function deleteFamilyTreeType(t)
{
	var alamat = t.getAttribute("href");
	$.ajax({
		url: alamat,
		type: 'get',
		dataType: 'html',
		success: function(result)
		{
			$('#familyTreeTypeDeleteModal').modal();
			$('#hapusFamilyTreeTypeList').html(result);
		}
	});
}

$(document).ready(function() 
{
	$('#tb_family_tree_type').DataTable({
		"bInfo" : false
		});
	});

$("#maju2").click(function()
{
	var table = $('#tb_family_tree_type').DataTable();
	table.page('next').draw('page');
});

$("#mundur2").click(function()
{
	var table = $('#tb_family_tree_type').DataTable();
	table.page('previous').draw('page');
});