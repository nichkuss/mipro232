familyTypeList();
familyTreeTypeList();

function familyTypeList()
{
	$.ajax({
		url : '/familyTypeList',
		type : 'get',
		dataType: 'html',
		success: function(result)
		{
			$('#familyTypeList').html(result);
		}
	});
}

function familyTreeTypeList() 
{
	$.ajax({
		url : '/familyTreeTypeList',
		type : 'get',
		dataType: 'html',
		success: function(result)
		{
			$('#familyTreeTypeList').html(result);
		}
	});
}

function addForm() 
{
	$.ajax({
		url : '/familyTypeAdd',
		type : 'get',
		dataType : 'html',
		success : function(result) 
		{
			$('#familyTypeModal').modal();
			$('#nonFamilyTypeList').html(result);	
		}
	});
}

function addForm2() 
{
	$.ajax({
		url : '/familyTreeTypeAdd',
		type : 'get',
		dataType : 'html',
		success : function(result) 
		{
			$('#familyTreeTypeModal').modal();
			$('#nonFamilyTreeTypeList').html(result);	
		}
	});
}

function closeModal()
{
	$('#familyTypeModal').hide();
	$('.modal-backdrop').hide();
	$('.modal-backdrop').remove();
	familyTypeList();
}

function refreshPage()
{
	$.ajax({
		url : '/familyType',
		type : 'get',
		dataType : 'html',
		success : function(result) 
		{
			closeModal();
			$('#fragment').html(result);
		}
	});
	return false;
}

$("#pgsize li").click(function()
{
	var table = $('#tb_family_type').DataTable();
	table.page.len($(this).text()).draw();
});

$("#asc").click(function()
{
	var table = $('#tb_family_type').DataTable();
	table.order([0, 'asc']).draw();
});

$("#desc").click(function()
{
	var table = $('#tb_family_type').DataTable();
	table.order([0, 'desc']).draw();
});

$("#pgsize2 li").click(function()
{
	var table = $('#tb_family_tree_type').DataTable();
	table.page.len($(this).text()).draw();
});

$("#asc2").click(function()
{
	var table = $('#tb_family_tree_type').DataTable();
	table.order([0, 'asc']).draw();
});

$("#desc2").click(function()
{
	var table = $('#tb_family_tree_type').DataTable();
	table.order([0, 'desc']).draw();
});


