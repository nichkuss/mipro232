function cekSusunanKeluarga()
{
	var name = document.getElementById("name").value;
	var errorMsg = "Susunan / Tipe Keluarga Tidak Boleh Kosong";
	var warna = errorMsg.fontcolor("#CC0000");
	
	if(name == "")
	{
		$('#family_type_error').html(warna);
		$('#family_type_error').show();
		document.getElementById("susunanKeluarga").style.color = "red";
		document.getElementById("name").style.borderColor = "red";
		document.getElementById("btn_simpan").disabled = true;
	}
	else
	{
		$('#family_type_error').hide();
		document.getElementById("susunanKeluarga").style.color = "";
		document.getElementById("name").style.borderColor = "";
		document.getElementById("btn_simpan").disabled = false;
	}
}

$('#dataNewFamilyType').submit(function() 
{
	var susunanKeluarga = $('#name').val();
	if(susunanKeluarga == "")
	{
		alert("Susunan / Tipe Keluarga Harus Diisi")
		return false;
	}
	else
	{
		formdata = $('#dataNewFamilyType').serialize();
		$.ajax({
			url : '/simpanFamilyType',
			type : 'POST',
			data : formdata,
			success : function(result) 
			{
				alert("Susunan / Tipe Keluarga Berhasil Disimpan!");
				refreshPage();
			}
		});
	}
	return false;
});
