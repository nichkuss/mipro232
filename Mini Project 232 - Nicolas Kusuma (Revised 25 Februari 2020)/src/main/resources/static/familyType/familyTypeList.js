function editFamilyType(t)
{
	var alamat = t.getAttribute("href");
	$.ajax({
		url : alamat,
		type : 'get',
		dataType : 'html',
		success : function(result) 
		{	
			$('#familyTypeModal').modal();
			$('#nonFamilyTypeList').html(result);
		}
	});	
}

function editFamilyTreeType(t)
{
	var alamat = t.getAttribute("href");
	$.ajax({
		url : alamat,
		type : 'get',
		dataType : 'html',
		success : function(result) 
	{	
			$('#familyTreeTypeModal').modal();
			$('#nonFamilyTreeTypeList').html(result);
		}
	});	
}

function deleteFamilyTreeType(t)
{
//	var id = document.getElementById("id").value;
	var alamat = t.getAttribute("href");
	$.ajax({
		url: alamat,
		type: 'get',
		dataType: 'html',
		success: function(result)
		{
			$('#familyTreeTypeDeleteModal').modal();
			$('#hapusFamilyTreeTypeList').html(result);
		}
	});
}

function deleteFamilyType(t)
{
	var alamat = t.getAttribute("href");
	$.ajax({
		url: alamat,
		type: 'get',
		dataType: 'html',
		success: function(result)
		{
			$('#familyTypeDeleteModal').modal();
			$('#hapusFamilyTypeList').html(result);
		}
	});
}

$(document).ready(function() 
{
    $('#tb_family_type').DataTable({
    	"bInfo" : false
    });
});

$("#maju").click(function()
{
	var table = $('#tb_family_type').DataTable();
	table.page('next').draw('page');
});

$("#mundur").click(function()
{
	var table = $('#tb_family_type').DataTable();
	table.page('previous').draw('page');
});