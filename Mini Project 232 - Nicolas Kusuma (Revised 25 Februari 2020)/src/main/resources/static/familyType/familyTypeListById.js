familyTypeListById();

function familyTypeListById()
{
	$.ajax({
		url : '/familyTypeListById',
		type : 'get',
		dataType: 'html',
		success: function(result)
		{
			$('#familyTypeListById').html(result);
		}
	});
}

function addForm()
{
	$.ajax({
		url : '/familyTypeListByIdAdd',
		type : 'get',
		dataType : 'html',
		success : function(result) 
		{
			$('#familyTypeByIdModal').modal();
			$('#nonFamilyTypeList').html(result);	
		}
	});
}

function closeModal()
{
	$('#familyTypeListByIdModal').hide();
	$('.modal-backdrop').hide();
	$('.modal-backdrop').remove();
	familyTypeListById();
}

function refreshPage()
{
	$.ajax({
		url : '/familyType',
		type : 'get',
		dataType : 'html',
		success : function(result) 
		{
			closeModal();
			$('#fragment').html(result);
		}
	});
}