organisasiList();

function organisasiList() 
{
	$.ajax({
		url : '/organisasiList',
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#organisasiList').html(result);
		}
	});
}

function tutupModal()
{
	$('#organisasiModal').modal('hide');
	$('#organisasiEditModal').modal('hide');
	$('#organisasiDeleteModal').modal('hide');
	
}

function closeModal()
{
	$('#organisasiModal').hide();
	$('.modal-backdrop').hide();
	$('.modal-backdrop').remove();
}

function refreshPage()
{
	$.ajax({
		url : '/organisasi',
		type : 'get',
		dataType : 'html',
		success : function(result) 
		{
			closeModal();
			$('#pelamarmodal').modal({backdrop: 'static', keyboard: false});
			$('#pelamarisi').html(result);
		}
	});
	return false;
}