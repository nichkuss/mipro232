function cekOrganisasi()
{
	var organisasi = document.getElementById("name").value;
	var errorMsg = "Organization name cannot be empty";
	var warna = errorMsg.fontcolor("#CC0000");
		
	if(organisasi == "")
	{
		$('#organization_error').html(warna);
		$('#organization_error').show();
		document.getElementById("organisasi").style.color = "red";
		document.getElementById("name").style.borderColor = "red";
	}
	else
	{
		$('#organization_error').hide();
		document.getElementById("organisasi").style.color = "";
		document.getElementById("name").style.borderColor = "";
	}
}	

function cekJabatan()
{
	var jabatan = document.getElementById("position").value;
	var errorMsg = "Position name cannot be empty";
	var warna = errorMsg.fontcolor("#CC0000");
		
	if(jabatan == "")
	{
		$('#position_error').html(warna);
		$('#position_error').show();
		document.getElementById("jabatan").style.color = "red";
		document.getElementById("position").style.borderColor = "red";
	}
	else
	{
		$('#position_error').hide();
		document.getElementById("jabatan").style.color = "";
		document.getElementById("position").style.borderColor = "";
	}
}
	
function cekTahunMasuk()
{
	var entry_year = $('#entryYear').val();
	var exit_year = $('#exitYear').val();
	var errorMsg = "Exit year cannot be less than exit year";
	var warna = errorMsg.fontcolor("#CC0000");
	var errorYear;
	
	if(entry_year !="" && exit_year == "")
	{
		$('#exit_year_error').hide();
		document.getElementById("keluar").style.color = "";
		document.getElementById("exitYear").style.borderColor = "";
		errorYear = false;
	} 
	else if (entry_year > exit_year)
	{
		$('#exit_year_error').html(warna);
		$('#exit_year_error').show();
		document.getElementById("keluar").style.color = "#CC0000";
		document.getElementById("exitYear").style.borderColor = "#CC0000";
		errorYear = true;
	} 
	else
	{
		$('#exit_year_error').hide();
		document.getElementById("keluar").style.color = "";
		document.getElementById("exitYear").style.borderColor = "";
		errorYear = false;
	}
}

function cekTahunKeluar()
{
	var entry_year = $('#entryYear').val();
	var exit_year = $('#exitYear').val();
	var errorMsg = "Exit year cannot be less than exit year";
	var warna = errorMsg.fontcolor("#CC0000");
	var errorYear;
	
	if(entry_year > exit_year)
	{
		$('#exit_year_error').html(warna);
		$('#exit_year_error').show();
		document.getElementById("keluar").style.color = "#CC0000";
		document.getElementById("exitYear").style.borderColor = "#CC0000";
		errorYear = true;
	}
	else
	{
		$('#exit_year_error').hide();
		document.getElementById("keluar").style.color = "";
		document.getElementById("exitYear").style.borderColor = "";
		errorYear = false;
	}
}

function tutupModal2()
{
	$('#organisasiModal').modal('hide');
	$('#organisasiEditModal').modal('hide');
	$('#organisasiDeleteModal').modal('hide');
}

function refreshdp() 
{
	var url="/";
	$.ajax({
		url : url,
		type : 'get',
		dataType : 'html',
		success : function(result) 
		{
			if(url=="/")
			{
			window.location='/';
			}
			else
			{
				$('#pelamardetailisi').html(result);
			}
		}
	});
	return false;
};

$('#dataEditOrganisasi').submit(function() 
{
	var organisasi = $('#name').val();
	var jabatan = $('#position').val();
	var ety = $('#entryYear').val();
	var exy = $('#exitYear').val();

	if ((ety > exy) || (organisasi == "") || (jabatan == ""))
	{
		alert("One or more fields is incorrect");
		return false;
	} 
	else 
	{
		formdata = $('#dataEditOrganisasi').serialize();
		var rfs = $('#biodataId').val();
		$.ajax({
			url: '/ubahOrganisasi',
			type: 'POST',
			data: formdata,
			success: function(result)
			{
				tutupModal2();
				alert("Organisasi berhasil diperbaharui");
				refreshOrganisasi(rfs);
			}
		});
		return false;
	}				
});