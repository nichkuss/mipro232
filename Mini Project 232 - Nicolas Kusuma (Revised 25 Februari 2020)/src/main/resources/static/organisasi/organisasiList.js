function addForm()
{
	var id = $('#biodataId').val();
	$.ajax({
		url: '/organisasiAdd/'+id,
		type: 'get',
		dataType: 'html',
		success: function(result)
		{
			$('#organisasiModal').modal();
			$('#nonOrganisasiList').html(result);
		}
	});
}	

function tutupModal()
{
	$('#organisasiModal').modal('hide');
	$('#organisasiEditModal').modal('hide');
	$('#organisasiDeleteModal').modal('hide');
	$('#pelamarmodal').modal('show');
}

function editOrganisasi(t) 
{
	var alamat = t.getAttribute("href");
	$.ajax({
		url : alamat,
		type : 'get',
		dataType : 'html',
		success : function(result) 
		{
			$('#organisasiEditModal').modal();
			$('#nonOrganisasiEditList').html(result);
		}
	});
}

function deleteOrganisasi(t) 
{
	var alamat = t.getAttribute("href");
	$.ajax({
		url : alamat,
		type : 'get',
		dataType : 'html',
		success : function(result) 
		{
			$('#hapusOrganisasiList').html(result);
			$('#organisasiDeleteModal').modal();
		}
	});
}