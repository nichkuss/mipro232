pelamarlist();

function pelamarlist()
{
	var cr = $('#cari').val();
	$.ajax({
		url : '/pelamarlist/' + cr,
		type : 'get',
		dataType : 'html',
		success : function(result) 
		{
			console.log(result);
			$('#pelamarlist').html(result);
		}
	});
}

$('#btn_cari').click(function()
{
	pelamarlist();
});


function reset()
{
	document.getElementById('cari').value = '';
	$("table").hide();
	$("#pager").hide();
}

$("#pgsize li").click(function()
{
	var table = $('#tabel_pelamar').DataTable();
	table.page.len($(this).text()).draw();
});

$("#asc").click(function()
{
	var table = $('#tabel_pelamar').DataTable();
	table.order([0, 'asc']).draw();
});

$("#desc").click(function()
{
	var table = $('#tabel_pelamar').DataTable();
	table.order([0, 'desc']).draw();
});


