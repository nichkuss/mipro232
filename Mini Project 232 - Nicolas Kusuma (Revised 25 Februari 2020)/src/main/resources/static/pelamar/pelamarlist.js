function detpelamar(t) 
{
	var alamat = t.getAttribute("href");
	$.ajax({
		url : alamat,
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#pelamarmodal').modal({backdrop: 'static', keyboard: false}) 
			$('#pelamarisi').html(result);
		}
	});
}

$(document).ready(function() 
{
    $('#tabel_pelamar').DataTable({
        "lengthMenu": [[1, 2, 3, -1], [1, 2, 3, "All"]],
    	"bInfo" : false
    });
});

$("#maju").click(function()
{
	var table = $('#tabel_pelamar').DataTable();
	table.page('next').draw('page');
});

$("#mundur").click(function()
{
	var table = $('#tabel_pelamar').DataTable();
	table.page('previous').draw('page');
});


