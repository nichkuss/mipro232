package com.mipro;

import java.io.File;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.mipro.MiproApplication;
import com.mipro.controller.dokumenController;

@SpringBootApplication
public class MiproApplication {

	public static void main(String[] args) {
		new File(dokumenController.uploadingDir).mkdirs();
		SpringApplication.run(MiproApplication.class, args);
	}

}
