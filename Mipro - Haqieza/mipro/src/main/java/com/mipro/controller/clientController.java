package com.mipro.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mipro.model.clientModel;
import com.mipro.service.clientService;

@Controller
public class clientController {
	
	@Autowired
	private clientService cs;
	
	@RequestMapping("clientlist")
	public String clientlist(Model model) {
		List<clientModel>semuaclient=cs.semuaclient();
		model.addAttribute("semuaclient", semuaclient);
		return "client/clientlist";
	}
	
	@RequestMapping("client")
	public String client() {
		return "client/client";
	}
	
	@RequestMapping("clientadd")
	public String clientadd() {
		return "client/clientadd";
	}
	
	@ResponseBody
	@RequestMapping("simpanclient")
	public String simpanclient(@ModelAttribute()clientModel clientModel) {
		long loguser=232;
		//created by
		clientModel.setCreatedBy(new Long (loguser));
		//created on
		clientModel.setCreatedOn(new Date());
		cs.simpanclient(clientModel);
		return "";
	}
	
	@ResponseBody
	@RequestMapping("ubahclient")
	public String ubahclient(@ModelAttribute()clientModel clientModel) {
		long loguser=232;
		//set create
		clientModel clientbyid = cs.clientbyid(clientModel.getId());
		clientModel.setCreatedBy(clientbyid.getCreatedBy());
		clientModel.setCreatedOn(clientbyid.getCreatedOn());
		//update by
		clientModel.setModifiedBy(new Long (loguser));
		//update on
		clientModel.setModifiedOn(new Date());
		cs.simpanclient(clientModel);
		return "";
	}
	
	@ResponseBody
	@RequestMapping("hapusclient/{ids}")
	public String hapusclient(@ModelAttribute()clientModel clientModel,
			@PathVariable(name = "ids")long ids) {
		clientModel clientbyid = cs.clientbyid(ids);
		long loguser=232;
		clientbyid.setDeleteBy(new Long(loguser));
		clientbyid.setDeleteOn(new Date());
		cs.simpanclient(clientbyid);
		cs.hapusclient(ids);
		return "";
	}
	
	@RequestMapping("clientedit/{ids}")
	public String clientedit(Model model,
			@PathVariable(name="ids")long ids) {
		clientModel clientbyid=cs.clientbyid(ids);
		model.addAttribute("clientbyid", clientbyid);
		return "client/clientedit";
	}
	
	@RequestMapping("clientdelete/{ids}")
	public String clientdelete(Model model,
			@PathVariable(name="ids")long ids) {
		clientModel clientbyid=cs.clientbyid(ids);
		model.addAttribute("clientbyid", clientbyid);
		return "client/clientdelete";
	}

}