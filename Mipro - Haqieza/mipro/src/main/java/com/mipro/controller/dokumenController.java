package com.mipro.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.ServletContext;

import org.aspectj.apache.bcel.classfile.Module.Require;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.mipro.dto.dokumenBid;
import com.mipro.model.dokumenModel;
import com.mipro.service.dokumenService;

@Controller
public class dokumenController {
	
	@Autowired
	private dokumenService ds;
	
	@Autowired
	ServletContext context;
	public static final String uploadingDir = System.getProperty("user.dir")+"/uploadingDir/";
	
	@RequestMapping("dokumenlist/{ids}")
	public String dokumenlist(Model model,
			@PathVariable(name="ids")Long ids) {
//		List<dokumenModel>semuadokumen=ds.semuadokumen();
		List<dokumenBid> semuadokumen = ds.semuadokumenJ(ids);
//		dokumenModel dokumenbyid=ds.dokumenbyid(ids);
		model.addAttribute("dokumenbyid", ids);
		model.addAttribute("semuadokumen", semuadokumen);
		return "dokumen/dokumenlist";
	}
	
	@RequestMapping("dokumen")
	public String dokumen() {
		return "dokumen/dokumen";
	}
	
	@RequestMapping("dokumenadd/{ids}")
	public String dokumenadd(@ModelAttribute()dokumenModel dokumenModel, Model model,
			@PathVariable(name = "ids")Long ids) {
		try (Stream<Path> walk = Files.walk(Paths.get(uploadingDir))) {
//			System.out.println(walk);
			List<String> fresult = new ArrayList<String>();
			List<String> result = walk.filter(Files::isRegularFile)
					.map(x -> x.toString()).collect(Collectors.toList());
			for (String item:result) {
				fresult.add(item.substring(item.lastIndexOf("\\") + 1));
			}
			model.addAttribute("fresult", fresult);
			model.addAttribute("ids", ids);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "dokumen/dokumenadd";
	}
	
	@ResponseBody
	@RequestMapping("naikinfile")
	public String naikinfile(@RequestParam("uploadingFiles") MultipartFile[] uploadingFiles,
							 @RequestParam("biodataId") String ids,
							 @RequestParam("waktu") String dm,
							 @ModelAttribute()dokumenModel dokumenModel) throws IOException {
		 for(MultipartFile uploadedFile : uploadingFiles) {
	            File file = new File(uploadingDir + ids + "_" + dm + "_" +uploadedFile.getOriginalFilename());
	            System.out.println("ini3"+file);
	            uploadedFile.transferTo(file);
	        }
		return "";
	}
	
	@ResponseBody
	@RequestMapping("naikingbr")
	public String naikingbr(@RequestParam("uploadingGbr") MultipartFile[] uploadingFiles,
			 				@RequestParam("biodataId") String ids) throws IOException {
		for(MultipartFile uploadedFile : uploadingFiles) {
	            File file = new File(uploadingDir + ids + "_" + uploadedFile.getOriginalFilename());
	            uploadedFile.transferTo(file);
	        }
		return "";
	}
	
	@ResponseBody
	@RequestMapping("simpandokumen")
	public String simpandokumen(@ModelAttribute()dokumenModel dokumenModel,
								@RequestParam(value = "ispoto",required = false) String ispoto) {
		System.out.println(ispoto);
		if (ispoto.equals("1")) {
			dokumenModel.setPhoto(true);
		}
		String path=dokumenModel.getFilePath();
		//System.out.println(path);
		path = path.substring(path.lastIndexOf("\\")+1);
//		String extensi = path.substring(path.lastIndexOf(".")+1);
		//System.out.println(extensi);
		dokumenModel.setFileName(path);
		String mpath=uploadingDir+path;
		dokumenModel.setFilePath(mpath);
		long loguser=232;
		//created by
		dokumenModel.setCreatedBy(loguser);
		//created on
		dokumenModel.setCreatedOn(new Date());
		ds.simpandokumen(dokumenModel);
		return "";
	}
	
	@ResponseBody
	@RequestMapping("ubahdokumen")
	public String ubahdokumen(@ModelAttribute()dokumenModel dokumenModel,
							  @RequestParam(value = "ispoto",required = false) String ispoto) {
		System.out.println(ispoto);
		if (ispoto.equals("1")) {
			dokumenModel.setPhoto(true);
		}
		String path=dokumenModel.getFilePath();
		path = path.substring(path.lastIndexOf("\\")+1);
		dokumenModel.setFileName(path);
		String mpath=uploadingDir+path;
		dokumenModel.setFilePath(mpath);
		long loguser=232;
		//set create
		dokumenModel dokumenbyid = ds.dokumenbyid(dokumenModel.getId());
		dokumenModel.setBiodataId(dokumenbyid.getBiodataId());
		dokumenModel.setCreatedBy(dokumenbyid.getCreatedBy());
		dokumenModel.setCreatedOn(dokumenbyid.getCreatedOn());
		//update by
		dokumenModel.setModifiedBy(new Long (loguser));
		//update on
		dokumenModel.setModifiedOn(new Date());
		ds.simpandokumen(dokumenModel);
		return "";
	}
	
	@ResponseBody
	@RequestMapping("hapusdokumen/{ids}")
	public String hapusdokumen(@ModelAttribute()dokumenModel dokumenModel,
			@PathVariable(name = "ids")long ids) {
		dokumenModel dokumenbyid = ds.dokumenbyid(ids);
		long loguser=232;
		dokumenbyid.setDeleteBy(new Long (loguser));
		dokumenbyid.setDeleteOn(new Date());
		ds.simpandokumen(dokumenbyid);
		ds.hapusdokumen(ids);
		return "";
	}
	
	@RequestMapping("dokumenedit/{ids}")
	public String dokumenedit(@ModelAttribute()dokumenModel dokumenModel, Model model,
			@PathVariable(name="ids")long ids) {
		try (Stream<Path> walk = Files.walk(Paths.get(uploadingDir))) {
			List<String> fresult = new ArrayList<String>();
			List<String> result = walk.filter(Files::isRegularFile)
					.map(x -> x.toString()).collect(Collectors.toList());
			for (String item:result) {
				fresult.add(item.substring(item.lastIndexOf("\\") + 1));
			}
			model.addAttribute("fresult", fresult);
			model.addAttribute("ids", ids);
		} catch (IOException e) {
			e.printStackTrace();
		}
		dokumenModel dokumenbyid=ds.dokumenbyid(ids);
		model.addAttribute("dokumenbyid", dokumenbyid);
		return "dokumen/dokumenedit";
	}
	
	@RequestMapping("dokumendelete/{ids}")
	public String dokumendelete(Model model,
			@PathVariable(name="ids")long ids) {
		dokumenModel dokumenbyid=ds.dokumenbyid(ids);
		model.addAttribute("dokumenbyid", dokumenbyid);
		return "dokumen/dokumendelete";
	}

}