package com.mipro.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mipro.model.pelamarModel;
import com.mipro.service.pelamarService;

@Controller
public class pelamarController {
	
	@Autowired
	private pelamarService ps;
	
	@RequestMapping("pelamarlist")
	public String pelamarlist(Model model) {
		List<pelamarModel>semuapelamar=ps.semuapelamar();
		model.addAttribute("semuapelamar", semuapelamar);
		return "pelamar/pelamarlist";
	}
	
	@RequestMapping("pelamar")
	public String pelamar() {
		return "pelamar/pelamar";
	}

	@RequestMapping("pelamardetail/{ids}")
	public String pelamardetail(Model model, @PathVariable(name="ids")long ids) {
		pelamarModel pelamarbyid=ps.pelamarbyid(ids);
		model.addAttribute("pelamarbyid", pelamarbyid);
		return"pelamar/pelamardetail";
	}
}