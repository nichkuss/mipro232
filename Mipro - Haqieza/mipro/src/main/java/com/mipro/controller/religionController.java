package com.mipro.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mipro.model.religionModel;
import com.mipro.service.religionService;

@Controller
public class religionController {
	
	@Autowired
	private religionService rs;
	
	@RequestMapping("religionlist")
	public String religionlist(Model model) {
		List<religionModel>semuareligion=rs.semuareligion();
		model.addAttribute("semuareligion", semuareligion);
		return "religion/religionlist";
	}
	
	@RequestMapping("religion")
	public String religion() {
		return "religion/religion";
	}
	
	@RequestMapping("religionadd")
	public String religionadd() {
		return "religion/religionadd";
	}
	
	@ResponseBody
	@RequestMapping("simpanreligion")
	public String simpanreligion(@ModelAttribute()religionModel religionModel) {
		long loguser=232;
		//created by
		religionModel.setCreatedBy(new Long (loguser));
		//created on
		religionModel.setCreatedOn(new Date());
		rs.simpanreligion(religionModel);
		return "";
	}
	
	@ResponseBody
	@RequestMapping("ubahreligion")
	public String ubahreligion(@ModelAttribute()religionModel religionModel) {
		long loguser=232;
		//set create
		religionModel religionbyid = rs.religionbyid(religionModel.getId());
		religionModel.setCreatedBy(religionbyid.getCreatedBy());
		religionModel.setCreatedOn(religionbyid.getCreatedOn());
		//update by
		religionModel.setModifiedBy(new Long (loguser));
		//update on
		religionModel.setModifiedOn(new Date());
		rs.simpanreligion(religionModel);
		return "";
	}
	
	@ResponseBody
	@RequestMapping("hapusreligion/{ids}")
	public String hapusreligion(@ModelAttribute()religionModel religionModel,
			@PathVariable(name = "ids")long ids) {
		religionModel religionbyid = rs.religionbyid(ids);
		long loguser=232;
		religionbyid.setDeleteBy(new Long(loguser));
		religionbyid.setDeleteOn(new Date());
		rs.simpanreligion(religionbyid);
		rs.hapusreligion(ids);
		return "";
	}
	
	@RequestMapping("religionedit/{ids}")
	public String religionedit(Model model,
			@PathVariable(name="ids")long ids) {
		religionModel religionbyid=rs.religionbyid(ids);
		model.addAttribute("religionbyid", religionbyid);
		return "religion/religionedit";
	}
	
	@RequestMapping("religiondelete/{ids}")
	public String religiondelete(Model model,
			@PathVariable(name="ids")long ids) {
		religionModel religionbyid=rs.religionbyid(ids);
		model.addAttribute("religionbyid", religionbyid);
		return "religion/religiondelete";
	}

}