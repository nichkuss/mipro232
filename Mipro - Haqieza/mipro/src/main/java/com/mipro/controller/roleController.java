package com.mipro.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mipro.model.roleModel;
import com.mipro.service.roleService;

@Controller
public class roleController {
	
	@Autowired
	private roleService rs;
	
	@RequestMapping("rolelist")
	public String rolelist(Model model) {
		List<roleModel>semuarole=rs.semuarole();
		model.addAttribute("semuarole", semuarole);
		return "role/rolelist";
	}
	
	@RequestMapping("role")
	public String role() {
		return "role/role";
	}
	
	@RequestMapping("roleadd")
	public String roleadd() {
		return "role/roleadd";
	}
	
	@ResponseBody
	@RequestMapping("simpanrole")
	public String simpanrole(@ModelAttribute()roleModel roleModel) {
		long loguser=232;
		//created by
		roleModel.setCreatedBy(new Long (loguser));
		//created on
		roleModel.setCreatedOn(new Date());
		//cek nama
		String cekname=rs.cekname(roleModel.getName());
		String cekcode=rs.cekcode(roleModel.getCode().toUpperCase());
		String hasil="0";
		if (cekname==null && cekcode==null) {
			rs.simpanrole(roleModel);
		} else {
			hasil="1";
		}
		return hasil;
	}
		
	@ResponseBody
	@RequestMapping("ubahrole")
	public String ubahrole(@ModelAttribute()roleModel roleModel) {
		long loguser=232;
		//set create
		roleModel rolebyid = rs.rolebyid(roleModel.getId());
		roleModel.setCreatedBy(rolebyid.getCreatedBy());
		roleModel.setCreatedOn(rolebyid.getCreatedOn());
		//update by
		roleModel.setModifiedBy(new Long (loguser));
		//update on
		roleModel.setModifiedOn(new Date());
		//cek nama
		String cekname=null;
		if (roleModel.getName().equals(rolebyid.getName())) {
			//'do nothing'
		} else {
			cekname=rs.cekname(roleModel.getName());
		}

		String cekcode=null;
		if (roleModel.getCode().equals(rolebyid.getCode())) {
			//'do nothing'
		} else {
			cekcode=rs.cekcode(roleModel.getCode());
		}
		
		String hasil="0";
		if (cekname==null && cekcode==null) {
			rs.simpanrole(roleModel);
		} else {
			hasil="1";
		}
		return hasil;
	}
	
	@ResponseBody
	@RequestMapping("hapusrole/{ids}")
	public String hapusrole(@ModelAttribute()roleModel roleModel,
			@PathVariable(name = "ids")long ids) {
		roleModel rolebyid = rs.rolebyid(ids);
		long loguser=232;
		rolebyid.setDeleteBy(new Long(loguser));
		rolebyid.setDeleteOn(new Date());
		rs.simpanrole(rolebyid);
		rs.hapusrole(ids);
		return "";
	}
	
	@RequestMapping("roleedit/{ids}")
	public String roleedit(Model model,
			@PathVariable(name="ids")long ids) {
		roleModel rolebyid=rs.rolebyid(ids);
		model.addAttribute("rolebyid", rolebyid);
		return "role/roleedit";
	}
	
	@RequestMapping("roledelete/{ids}")
	public String roledelete(Model model,
			@PathVariable(name="ids")long ids) {
		roleModel rolebyid=rs.rolebyid(ids);
		model.addAttribute("rolebyid", rolebyid);
		return "role/roledelete";
	}

}