package com.mipro.dto;

public class dokumenBid {
	private long id;
	private long biodataId;
	private String fileName;
	private String notes;
	public dokumenBid(long id, long biodataId, String fileName, String notes) {
		this.id = id;
		this.biodataId = biodataId;
		this.fileName = fileName;
		this.notes = notes;
	}
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getBiodataId() {
		return biodataId;
	}
	public void setBiodataId(long biodataId) {
		this.biodataId = biodataId;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
}
