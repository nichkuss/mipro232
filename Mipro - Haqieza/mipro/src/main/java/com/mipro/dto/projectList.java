package com.mipro.dto;

import java.util.Date;

public class projectList {
	private long id;
	private long clientId;
	private String name;
	private String location;
	private String department;
	private String picName;
	private String projectName;
	private Date startProject;
	private Date endProject;
	private String projectRole;
	private String projectPhase;
	private String projectDescription;
	private String projectTechnology;
	private String mainTask;
	public projectList(long id, long clientId, String name, String location, String department, String picName,
			String projectName, Date startProject, Date endProject, String projectRole, String projectPhase,
			String projectDescription, String projectTechnology, String mainTask) {
		this.id = id;
		this.clientId = clientId;
		this.name = name;
		this.location = location;
		this.department = department;
		this.picName = picName;
		this.projectName = projectName;
		this.startProject = startProject;
		this.endProject = endProject;
		this.projectRole = projectRole;
		this.projectPhase = projectPhase;
		this.projectDescription = projectDescription;
		this.projectTechnology = projectTechnology;
		this.mainTask = mainTask;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getClientId() {
		return clientId;
	}
	public void setClientId(long clientId) {
		this.clientId = clientId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getPicName() {
		return picName;
	}
	public void setPicName(String picName) {
		this.picName = picName;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public Date getStartProject() {
		return startProject;
	}
	public void setStartProject(Date startProject) {
		this.startProject = startProject;
	}
	public Date getEndProject() {
		return endProject;
	}
	public void setEndProject(Date endProject) {
		this.endProject = endProject;
	}
	public String getProjectRole() {
		return projectRole;
	}
	public void setProjectRole(String projectRole) {
		this.projectRole = projectRole;
	}
	public String getProjectPhase() {
		return projectPhase;
	}
	public void setProjectPhase(String projectPhase) {
		this.projectPhase = projectPhase;
	}
	public String getProjectDescription() {
		return projectDescription;
	}
	public void setProjectDescription(String projectDescription) {
		this.projectDescription = projectDescription;
	}
	public String getProjectTechnology() {
		return projectTechnology;
	}
	public void setProjectTechnology(String projectTechnology) {
		this.projectTechnology = projectTechnology;
	}
	public String getMainTask() {
		return mainTask;
	}
	public void setMainTask(String mainTask) {
		this.mainTask = mainTask;
	}
	
}
