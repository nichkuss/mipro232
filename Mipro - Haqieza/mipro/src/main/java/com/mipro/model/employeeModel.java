package com.mipro.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name="x_employee")
public class employeeModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column (name="id", length = 11, nullable = false)
	private long id;
	
	@Column (name="created_by", length = 11, nullable = false)
	private Long createdBy;
	
	@Column (name="created_on", nullable = false)
	private Date createdOn;
	
	@Column (name="modified_by", length = 11)
	private Long modifiedBy;
	
	@Column (name="modified_on")
	private Date modifiedOn;
	
	@Column (name="delete_by", length = 11)
	private Long deleteBy;
	
	@Column (name="delete_on")
	private Date deleteOn;
	
	@Column (name="is_delete", columnDefinition = "boolean default false", nullable = false)
	private boolean isDelete;
	
	@Column (name="biodata_id", length = 11, nullable = false)
	private Long biodataId;
	
	@Column (name="is_idle")
	private boolean isIdle;
	
	@Column (name="is_ero")
	private boolean isEro;
	
	@Column (name="is_user_client")
	private boolean isUserClient;
	
	@Column(name="ero_email", length = 100)
	private String eroEmail;

	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getDeleteBy() {
		return deleteBy;
	}

	public void setDeleteBy(Long deleteBy) {
		this.deleteBy = deleteBy;
	}

	public Date getDeleteOn() {
		return deleteOn;
	}

	public void setDeleteOn(Date deleteOn) {
		this.deleteOn = deleteOn;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Long getBiodataId() {
		return biodataId;
	}

	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}

	public boolean isIdle() {
		return isIdle;
	}

	public void setIdle(boolean isIdle) {
		this.isIdle = isIdle;
	}

	public boolean isEro() {
		return isEro;
	}

	public void setEro(boolean isEro) {
		this.isEro = isEro;
	}

	public boolean isUserClient() {
		return isUserClient;
	}

	public void setUserClient(boolean isUserClient) {
		this.isUserClient = isUserClient;
	}

	public String getEroEmail() {
		return eroEmail;
	}

	public void setEroEmail(String eroEmail) {
		this.eroEmail = eroEmail;
	}
	
}
