package com.mipro.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table (name="x_resource_project")
public class resourceModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column (name="id", length = 11, nullable = false)
	private long id;
	
	@Column (name="created_by", length = 11, nullable = false)
	private Long createdBy;
	
	@Column (name="created_on", nullable = false)
	private Date createdOn;
	
	@Column (name="modified_by", length = 11)
	private Long modifiedBy;
	
	@Column (name="modified_on")
	private Date modifiedOn;
	
	@Column (name="delete_by", length = 11)
	private Long deleteBy;
	
	@Column (name="delete_on")
	private Date deleteOn;
	
	@Column (name="is_delete", columnDefinition = "boolean default false", nullable = false)
	private boolean isDelete;
	
	@Column (name="client_id", length = 11, nullable = false)
	private long clientId;
	
	@Column (name="location", length = 100)
	private String location;
	
	@Column (name="department", length = 100)
	private String department;
	
	@Column (name="pic_name", length = 100)
	private String picName;
	
	@Column (name="project_name", length = 100, nullable = false)
	private String projectName;
	

	@Column (name="start_project", nullable = false)
	private Date startProject;
	

	@Column (name="end_project", nullable = false)
	private Date endProject;
	
	@Column (name="project_role", length = 255, nullable = false)
	private String projectRole;
	
	@Column (name="project_phase", length = 255, nullable = false)
	private String projectPhase;
	
	@Column (name="project_description", length = 255, nullable = false)
	private String projectDescription;
	
	@Column (name="project_technology", length = 255, nullable = false)
	private String projectTechnology;
	
	@Column (name="main_task", length = 255, nullable = false)
	private String mainTask;

	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getDeleteBy() {
		return deleteBy;
	}

	public void setDeleteBy(Long deleteBy) {
		this.deleteBy = deleteBy;
	}

	public Date getDeleteOn() {
		return deleteOn;
	}

	public void setDeleteOn(Date deleteOn) {
		this.deleteOn = deleteOn;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public long getClientId() {
		return clientId;
	}

	public void setClientId(long clientId) {
		this.clientId = clientId;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getPicName() {
		return picName;
	}

	public void setPicName(String picName) {
		this.picName = picName;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public Date getStartProject() {
		return startProject;
	}

	public void setStartProject(Date startProject) {
		this.startProject = startProject;
	}

	public Date getEndProject() {
		return endProject;
	}

	public void setEndProject(Date endProject) {
		this.endProject = endProject;
	}

	public String getProjectRole() {
		return projectRole;
	}

	public void setProjectRole(String projectRole) {
		this.projectRole = projectRole;
	}

	public String getProjectPhase() {
		return projectPhase;
	}

	public void setProjectPhase(String projectPhase) {
		this.projectPhase = projectPhase;
	}

	public String getProjectDescription() {
		return projectDescription;
	}

	public void setProjectDescription(String projectDescription) {
		this.projectDescription = projectDescription;
	}

	public String getProjectTechnology() {
		return projectTechnology;
	}

	public void setProjectTechnology(String projectTechnology) {
		this.projectTechnology = projectTechnology;
	}

	public String getMainTask() {
		return mainTask;
	}

	public void setMainTask(String mainTask) {
		this.mainTask = mainTask;
	}

}
