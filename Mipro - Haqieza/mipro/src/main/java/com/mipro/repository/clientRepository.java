package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.mipro.model.clientModel;

public interface clientRepository extends JpaRepository<clientModel, Long> {
	@Query("select c from clientModel c where c.isDelete=false order by c.id")
	List<clientModel>semuaclient();
	
	@Query("select c from clientModel c where c.id=?1") //si clientModel disingkat menjadi "k"
	clientModel clientbyid(long ids);
	
	@Modifying
	@Query("update clientModel set isDelete = true where id=?1")
	long deleteflag(long ids);
	
}
