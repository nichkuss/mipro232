package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.mipro.dto.dokumenBid;
import com.mipro.model.dokumenModel;

public interface dokumenRepository extends JpaRepository<dokumenModel, Long> {
	//repository untuk query
	@Query("select d from dokumenModel d where d.isDelete=false order by d.id")
	List<dokumenModel>semuadokumen();
	
	@Query("select new com.mipro.dto.dokumenBid (d.id, d.biodataId, d.fileName, d.notes) from dokumenModel d INNER join pelamarModel p on d.biodataId = p.id where d.isDelete=false and d.biodataId=?1")
	List<dokumenBid>semuadokumenJ(Long ids);
	
	@Query("select d from dokumenModel d where d.biodataId=?1")
	Long biodataid(Long ids);
	
	@Query("select d from dokumenModel d where d.id=?1") //si dokumenModel disingkat menjadi "k"
	dokumenModel dokumenbyid(long ids);
	
	@Modifying
	@Query("update dokumenModel set isDelete = true where id=?1")
	int deleteflag(long ids);
	
}
