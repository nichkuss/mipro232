package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.mipro.model.religionModel;

public interface religionRepository extends JpaRepository<religionModel, Long> {
	//repository untuk query
	@Query("select r from religionModel r where r.isDelete=false order by r.id")
	List<religionModel>semuareligion();
	
	@Query("select r from religionModel r where r.id=?1") //si religionModel disingkat menjadi "k"
	religionModel religionbyid(long ids);
	
	@Modifying
	@Query("update religionModel set isDelete = true where id=?1")
	int deleteflag(long ids);
	
}
