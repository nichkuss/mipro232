package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.mipro.model.roleModel;

public interface roleRepository extends JpaRepository<roleModel, Long> {
	//repository untuk query
	@Query("select r from roleModel r where r.isDelete=false order by r.id")
	List<roleModel>semuarole();
	
	@Query("select r from roleModel r where r.id=?1") //si roleModel disingkat menjadi "k"
	roleModel rolebyid(long ids);
	
	@Modifying
	@Query("update roleModel set isDelete = true where id=?1")
	int deleteflag(long ids);
	
	//ambil name yang ada
	@Query("select r.name from roleModel r where lower(r.name)=lower(?1) and r.isDelete=false")
	String cekname(String name);
	
	//ambil code yang ada
	@Query("select r.code from roleModel r where lower(r.code)=lower(?1) and r.isDelete=false")
	String cekcode(String code);
	
}
