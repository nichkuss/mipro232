package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.model.clientModel;
import com.mipro.repository.clientRepository;

@Service
@Transactional
public class clientService {
	
	@Autowired
	private clientRepository cr;
	
	//by custom
	public List<clientModel> semuaclient(){
		return cr.semuaclient();
	}
	
	//save by jpa
	public clientModel simpanclient(clientModel clientModel) {
		return cr.save(clientModel);
	}
	
	public clientModel clientbyid(long ids) {
		return cr.clientbyid(ids);
	}
	
	public void hapusclient(long ids) {
		cr.deleteflag(ids);
	}

}
