package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.dto.dokumenBid;
import com.mipro.model.dokumenModel;
import com.mipro.repository.dokumenRepository;

@Service
@Transactional
public class dokumenService {
	
	@Autowired
	private dokumenRepository dr;
	
	//by custom
	public List<dokumenModel> semuadokumen(){
		return dr.semuadokumen();
	}
	
	//save by jpa
	public dokumenModel simpandokumen(dokumenModel dokumenModel) {
		return dr.save(dokumenModel);
	}
	
	public dokumenModel dokumenbyid(long ids) {
		return dr.dokumenbyid(ids);
	}
	
	public void hapusdokumen(Long ids) {
		dr.deleteflag(ids);
	}

	public List<dokumenBid> semuadokumenJ(Long ids){
		return dr.semuadokumenJ(ids);
	}
	
	public Long biodataid(Long ids) {
		return dr.biodataid(ids);
	}
}
