package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.model.pelamarModel;
import com.mipro.repository.pelamarRepository;

@Service
@Transactional
public class pelamarService {
	
	@Autowired
	private pelamarRepository pr;
	
	//by custom
	public List<pelamarModel> semuapelamar(){
		return pr.semuapelamar();
	}
	
//	//save by jpa
//	public pelamarModel simpanpelamar(pelamarModel pelamarModel) {
//		return pr.save(pelamarModel);
//	}
	
	public pelamarModel pelamarbyid(long ids) {
		return pr.pelamarbyid(ids);
	}
	
//	public void hapuspelamar(long ids) {
//		pr.deleteflag(ids);
//	}

}
