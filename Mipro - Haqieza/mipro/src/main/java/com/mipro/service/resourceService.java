package com.mipro.service;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.dto.dokumenBid;
import com.mipro.dto.projectList;
import com.mipro.model.resourceModel;
import com.mipro.repository.resourceRepository;

@Service
@Transactional
public class resourceService {
	
	@Autowired
	private resourceRepository rr;
	
	//by custom
	public List<resourceModel> semuaresource(){
		return rr.semuaresource();
	}
	
	//save by jpa
	public resourceModel simpanresource(resourceModel resourceModel) {
		return rr.save(resourceModel);
	}
	
	public resourceModel resourcebyid(long ids) {
		return rr.resourcebyid(ids);
	}
	
//	public projectList projectbyid(long ids) {
//		return rr.projectbyid(ids);
//	}
	
	public void hapusresource(long ids) {
		rr.deleteflag(ids);
	}
	
	public List<projectList> semuaresourceJ(){
		return rr.semuaresourceJ();
	}
	
	public List<projectList> semuaresourceJsrc(Date start,Date end){
		return rr.semuaresourceJsrc(start,end);
	}
	
	public projectList resourceJ(long ids) {
		return rr.resourceJ(ids);
	}
	
	public Long clientid(Long ids) {
		return rr.clientid(ids);
	}

}
