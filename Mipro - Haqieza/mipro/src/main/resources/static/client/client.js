clientlist();
function clientlist() {
	$.ajax({
		url : '/clientlist',
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#clientlist').html(result);	
		}
	});
}

function addform() {
	$.ajax({
		url : '/clientadd',
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#clientModal').modal();
			$('#nonclientlist').html(result);	
		}
	});
}

function closemodal() {
	$('#clientModal').hide();
	$('#delclientModal').hide();
	$('.modal-backdrop').hide();
	$('.modal-backdrop').remove();
	clientlist();
}

function refreshpage(){
	$.ajax({
		url : '/client',
		type : 'get',
		dataType : 'html',
		success : function(result) {
			closemodal();
			$('#fragment').html(result);
		}
	});
	return false;
}