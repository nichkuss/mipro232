dokumenlist();
function dokumenlist() {
	$.ajax({
		url : '/dokumenlist',
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#dokumenlist').html(result);	
		}
	});
}

//function addform() {
//	$.ajax({
//		url : '/dokumenadd',
//		type : 'get',
//		dataType : 'html',
//		success : function(result) {
//			$('#dokumenModal').modal();
//			$('#nondokumenlist').html(result);
//			$('#judul').html("Add Dokumen");
//		}
//	});
//}

function tutupmodal(){
	  $('#dokumenModal').modal('hide');
	  $('#deldokumenModal').modal('hide');
}

function closemodal() {
	$('#dokumenModal').hide();
	$('#deldokumenModal').hide();
	$('.modal-backdrop').hide();
	$('.modal-backdrop').remove();
	dokumenlist();
}

function refreshpage(){
	$.ajax({
		url : '/dokumen',
		type : 'get',
		dataType : 'html',
		success : function(result) {
			closemodal();
//			$('#fragment').html(result);
			$('#pelamarmodal').modal({backdrop: 'static', keyboard: false}) 
			$('#pelamarisi').html(result);
		}
	});
	return false;
}