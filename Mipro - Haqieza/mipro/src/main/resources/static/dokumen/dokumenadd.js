//$(document).ready(function() {
//	$('#naikin').bootstrapValidator({
//		// To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
//
//		feedbackIcons: {
//			valid: 'glyphicon glyphicon-ok',
//			invalid: 'glyphicon glyphicon-remove',
//			validating: 'glyphicon glyphicon-refresh'
//		},
//		fields: {
//			fileName: {
//				validators: {
//					notEmpty: {
//						message: 'Nama File tidak boleh kosong'
//					}
//				}
//			}
//		},
//		onSuccess: function(e, data) {
//			
//	
//
//		}
//
//	});    
//});

var flaguploadfile = 1;
var uploadfile = document.getElementById("filenya");
uploadfile.onchange = function() {
	if (this.files[0].size > 512000) {
		flaguploadfile = 0;
	}else {
		flaguploadfile = 1;
		document.getElementById("btn1").disabled="";
		document.getElementById("btn2").disabled="";
		document.getElementById("ket").hidden="true";
		document.getElementById("namafile").style.borderColor="";
	};
};

$('#naikinfile').submit(function() {
	formdata = $('#naikinfile').serialize();
	var bid = $('#biodataId').val();
//	alert(formdata);
	$.ajax({
		url : '/simpandokumen',
		type : 'POST',
		data : formdata,
		success : function(result) {
			refreshstl(bid);
			alert ("Simpan Dokumen Berhasil");
			tutupmodal();
		}

	});
	return false; // agar tidak pindah halaman

});

//$('#naikinfile').submit(function() {
//	formdata = $('#naikinfile').serialize();
//	var bid = $('#biodataId').val(); 
//	$.ajax({
//		url : '/simpandokumen',
//		type : 'POST',
//		data : formdata,
//		success : function(result) {
//			refreshstl(bid);
//			alert ("Simpan Dokumen Berhasil");
//			tutupmodal();
//		}
//
//	});
//	return false; // agar tidak pindah halaman
//});

var flaguploadgbr = 1;
var uploadgbr = document.getElementById("filefoto");
uploadgbr.onchange = function() {
	if (this.files[0].size > 512000) {
		flaguploadgbr = 0;
	}else {
		flaguploadgbr = 1;
		document.getElementById("btn1").disabled="";
		document.getElementById("btn2").disabled="";
		document.getElementById("ket").hidden="true";
		document.getElementById("namafile").style.borderColor="";
	};
};

$('#naikingbr').submit(function() {
	formdata = $('#naikingbr').serialize();
	var bid = $('#biodataId').val();
//	alert(formdata);
	$.ajax({
		url : '/simpandokumen',
		type : 'POST',
		data : formdata,
		success : function(result) {
			refreshstl(bid);
			alert ("Simpan Dokumen Berhasil");
			tutupmodal();
		}

	});
	return false; // agar tidak pindah halaman

});


$('#naikinfile').submit(function() {
	// Get form
    var form = $('#naikinfile')[0];
	// Create an FormData object 
    var data = new FormData(form);
	$.ajax({
		type: "POST",
        enctype: 'multipart/form-data',
        url: "/naikinfile",
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
		success : function(result) {
			tutupmodal();
			
		}
	});
	return false; // agar tidak pindah halaman
});

$('#naikingbr').submit(function() {
	// Get form
    var form = $('#naikingbr')[0];
	// Create an FormData object 
    var data = new FormData(form);
//    alert(data);
	$.ajax({
		type: "POST",
        enctype: 'multipart/form-data',
        url: "/naikingbr",
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
		success : function(result) {
//			$('#dokumenlist').html(result);	
			tutupmodal();
//			var a = true;
//			var b = false;
//			  
//			console.log(a.toString()); // true
//			console.log(b.toString()); // false
//			  
//			console.log(a.valueOf()); // true
//			console.log(b.valueOf()); // false
			
		}
	});
	return false; // agar tidak pindah halaman
});


$("#filenya").change(function(){
	$('#filePath').val($('#filenya').val());
	$('#namafile').val($('#filenya').val().split('\\').pop());
});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#gbr').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#filefoto").change(function(){
    readURL(this);
    $('#filePath2').val($('#filefoto').val());
    $('#namafile').val($('#filefoto').val().split('\\').pop());
//	$('#namagbr').val($('#filefoto').val().split('\\').pop());
});

function cek1() {
	var namafile=document.getElementById("namafile").value;
	if(namafile=="" || flaguploadfile == 0) {
		document.getElementById("btn1").disabled="true";
		document.getElementById("btn2").disabled="true";
		document.getElementById("ket").hidden="";
		document.getElementById("namafile").style.borderColor="red";
	} else {
		document.getElementById("btn1").disabled="";
		document.getElementById("btn2").disabled="";
		document.getElementById("ket").hidden="true"
		document.getElementById("namafile").style.borderColor="";
	}
}

function cek2() {
	var namafile=document.getElementById("namafile").value;
	if(namafile=="" || flaguploadgbr == 0) {
		document.getElementById("btn1").disabled="true";
		document.getElementById("btn2").disabled="true";
		document.getElementById("ket").hidden="";
		document.getElementById("namafile").style.borderColor="red";
	} else {
		document.getElementById("btn1").disabled="";
		document.getElementById("btn2").disabled="";
		document.getElementById("ket").hidden="true"
		document.getElementById("namafile").style.borderColor="";
	}
}
