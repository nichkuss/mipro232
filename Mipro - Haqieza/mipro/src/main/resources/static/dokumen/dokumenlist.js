//$(document).ready(function() {
//	$('#tbdokumen').dataTable( {
//		"lengthMenu": [[2, 3, 5, -1], [2, 3, 5, "All"]]
//		} );
//} );

function addform() {
	var ids = $('#idbiodata').val();
	$.ajax({
		url : '/dokumenadd/'+ids,
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#dokumenModal').modal();
			$('#nondokumenlist').html(result);
			$('#judul').html("Add Dokumen");
		}
	});
}

function tutupmodal(){
	  $('#dokumenModal').modal('hide');
	  $('#deldokumenModal').modal('hide');
}

function editdokumen(t) {
	var alamat=t.getAttribute("href");
	$.ajax({
		url : alamat,
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#dokumenModal').modal();
			$('#nondokumenlist').html(result);
			$('#judul').html("Edit Dokumen");
		}
	});
}

function deletedokumen(t) {
	var alamat=t.getAttribute("href");
	$.ajax({
		url : alamat,
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#deldokumenModal').modal();
			$('#deldokumenlist').html(result);		
		}
	});
}