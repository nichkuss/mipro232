religionlist();
var b;
function religionlist() {
	$.ajax({
		url : '/religionlist',
		type : 'get',
		dataType : 'html',
		success : function(result) {
			b = result.split('<tr>');
			console.log(b.length-1);
			$('#religionlist').html(result);	
		}
	});
}

function addform() {
	$.ajax({
		url : '/religionadd',
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#religionModal').modal();
			$('#nonreligionlist').html(result);
			$('#judul').html("Add Agama");
		}
	});
}

function tutupmodal(){
	  $('#religionModal').modal('hide');
	  $('#delreligionModal').modal('hide');
}

function closemodal() {
	$('#religionModal').hide();
	$('#delreligionModal').hide();
	$('.modal-backdrop').hide();
	$('.modal-backdrop').remove();
	religionlist();
}

function refreshpage(){
	$.ajax({
		url : '/religion',
		type : 'get',
		dataType : 'html',
		success : function(result) {
			closemodal();
			$('#fragment').html(result);
		}
	});
	return false;
}