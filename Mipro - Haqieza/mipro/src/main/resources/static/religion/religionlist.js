//oTable =$('#tbreligion').dataTable();
//function mySearch() {
//	 oTable.search($('#tableSearch').val()).draw() ;
//};
//$(".dataTables_filter").hide();

//$(document).ready(function() {
//	$('#tbreligion').dataTable( {
//		"lengthMenu": [[2, 3, 5, -1], [2, 3, 5, "All"]]
//		} );
//} );
//$(document).ready(function(){
//  $("#tableSearch").on("keyup", function() {
//    var value = $(this).val().toLowerCase();
//    $("#religiontb tr").filter(function() {
//      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
//    });
//  });
//});

// function mySearch() {
//	  var input, filter, table, tr, td, i, txtValue;
//	  input = document.getElementById("tableSearch");
//	  filter = input.value.toUpperCase();
//	  table = document.getElementById("tbreligion");
//	  tr = table.getElementsByTagName("tr");
	  
	  

	  // Loop through all table rows, and hide those who don't match the search query
//	  for (i = 0; i < tr.length; i++) {
//	    td = tr[i].getElementsByTagName("td")[0];
//	    if (td) {
//	      txtValue = td.textContent || td.innerText;
//	      if (txtValue.toUpperCase().indexOf(filter) > -1) {
//	        tr[i].style.display = "";
//	      } else {
//	        tr[i].style.display = "none";
//	      }
//	    }
//	  }
//	}

//function Ascending() {
//	  var table, rows, switching, i, x, y, shouldSwitch;
//	  table = document.getElementById("tbreligion");
//	  switching = true;
//	  /* Make a loop that will continue until
//	  no switching has been done: */
//	  while (switching) {
//	    // Start by saying: no switching is done:
//	    switching = false;
//	    rows = table.rows;
//	    /* Loop through all table rows (except the
//	    first, which contains table headers): */
//	    for (i = 1; i < (rows.length - 1); i++) {
//	      // Start by saying there should be no switching:
//	      shouldSwitch = false;
//	      /* Get the two elements you want to compare,
//	      one from current row and one from the next: */
//	      x = rows[i].getElementsByTagName("TD")[0];
//	      y = rows[i + 1].getElementsByTagName("TD")[0];
//	      // Check if the two rows should switch place:
//	      if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
//	        // If so, mark as a switch and break the loop:
//	        shouldSwitch = true;
//	        break;
//	      }
//	    }
//	    if (shouldSwitch) {
//	      /* If a switch has been marked, make the switch
//	      and mark that a switch has been done: */
//	      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
//	      switching = true;
//	    }
//	  }
//	}

//function Descending() {
//	  var table, rows, switching, i, x, y, shouldSwitch;
//	  table = document.getElementById("tbreligion");
//	  switching = true;
//	  /* Make a loop that will continue until
//	  no switching has been done: */
//	  while (switching) {
//	    // Start by saying: no switching is done:
//	    switching = false;
//	    rows = table.rows;
//	    /* Loop through all table rows (except the
//	    first, which contains table headers): */
//	    for (i = 1; i < (rows.length - 1); i++) {
//	      // Start by saying there should be no switching:
//	      shouldSwitch = false;
//	      /* Get the two elements you want to compare,
//	      one from current row and one from the next: */
//	      x = rows[i].getElementsByTagName("TD")[0];
//	      y = rows[i + 1].getElementsByTagName("TD")[0];
//	      // Check if the two rows should switch place:
//	      if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
//	        // If so, mark as a switch and break the loop:
//	        shouldSwitch = true;
//	        break;
//	      }
//	    }
//	    if (shouldSwitch) {
//	      /* If a switch has been marked, make the switch
//	      and mark that a switch has been done: */
//	      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
//	      switching = true;
//	    }
//	  }
//	}


oTable = $('#tbreligion').DataTable({
	"lengthMenu": [[-1, 1, 2, 3], ["All", 1, 2, 3]],
	"pagingType": "simple"
});   //pay attention to capital D, which is mandatory to retrieve "api" datatables' object, as @Lionel said

$('#btn_search').click(function(){
      oTable.search($('#tableSearch').val()).draw() ;
})

//var table = $('#tbreligion').DataTable();
// 
//// #column3_search is a <input type="text"> element
//$('#column3').on( 'keyup', function () {
//    table
//        .columns( 3 )
//        .search( this.value )
//        .draw();
//} );

$("#clear").click(function() {
	$('#tbreligion').DataTable().search('').draw();
	$('#tableSearch').val('');
});

$("#pgsize li").click(function() {
	var table = $('#tbreligion').DataTable();
	table.page.len( $(this).text() ).draw();
	checkData();
});

$("#asc").click(function() {
	var table = $('#tbreligion').DataTable();
	table.order( [ 0, 'asc' ]).draw();
});

$("#desc").click(function() {
	var table = $('#tbreligion').DataTable();
	table.order( [ 0, 'desc' ] ).draw();
});

$("#maju").click(function() {
	var table = $('#tbreligion').DataTable();
	table.page( 'next' ).draw( 'page' );
	checkData();
})

$("#mundur").click(function() {
	var table = $('#tbreligion').DataTable();
	table.page( 'previous' ).draw( 'page' );
	checkData();
})

function checkData(){
	
	var table = $('#tbreligion').DataTable();
//	alert(table.page() + ' - ' + ((b.length-1)/table.page.len()));
	if(table.page()+1  == Math.ceil((b.length-1)/table.page.len())){
	    $("#maju").addClass('disabled');
	} else {
	    $("#maju").removeClass('disabled');
	}
	
	if(table.page() == 0){
	    $("#mundur").addClass('disabled');
	} else {
		$("#mundur").removeClass('disabled');
	}
}

//var table = $('#tbreligion').DataTable();
//var buttons = table.page( ['#maju', '#mundur'] );
// 
//if ( table.rows( { selected: true } ).indexes().length === 0 ) {
//    buttons.disable();
//}
//else {
//    buttons.enable();
//}

//$("#kosong").click(function() {
//	var table = $('#tbreligion').DataTable();
//	table.page( 'reset' ).draw();
//})

//function reset() {
//	document.getElementById("tableSearch").value="";
//}


function editreligion(t) {
	var alamat=t.getAttribute("href");
	$.ajax({
		url : alamat,
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#religionModal').modal();
			$('#nonreligionlist').html(result);	
			$('#judul').html("Edit Agama");
		}
	});
}

function deletereligion(t) {
	var alamat=t.getAttribute("href");
	$.ajax({
		url : alamat,
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#delreligionModal').modal();
			$('#delreligionlist').html(result);		
		}
	});
}