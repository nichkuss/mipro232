resourcelist();
function resourcelist() {
	$.ajax({
		url : '/resourcelist',
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#resourcelist').html(result);	
		}
	});
}

function addform() {
	$.ajax({
		url : '/resourceadd',
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#resourceModal').modal();
			$('#nonresourcelist').html(result);	
			$('#judul').html("Add Resource Project");
		}
	});
}

function closemodal() {
	$('#resourceModal').hide();
	$('#delresourceModal').hide();
	$('.modal-backdrop').hide();
	$('.modal-backdrop').remove();
	resourcelist();
}

function refreshpage(){
	$.ajax({
		url : '/resource',
		type : 'get',
		dataType : 'html',
		success : function(result) {
			closemodal();
			$('#fragment').html(result);
		}
	});
	return false;
}

$("#btn_search").click(function() {
	var from = $('#from').val();
	var to = $('#to').val();
	
	$.ajax({
		url : '/resourcelist/'+from+'/'+to,
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#resourcelist').html(result);	
		}
	});
});

$(document).ready(function() {
	$('#from').datepicker( {
		dateFormat: 'MM dd, yy',
		} );
	$('#to').datepicker( {
		dateFormat: 'MM dd, yy',
		} );
	} );