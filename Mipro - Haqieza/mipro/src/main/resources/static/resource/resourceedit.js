//$('#dataeditresource').submit(function() {
//	formdata=$('#dataeditresource').serialize();
//	$.ajax({
//		url : '/ubahresource',
//		type : 'POST',
//		data : formdata,
//		success : function(result) {
//			alert("Edit Resource Sukses!!!")
//			refreshpage();
//		}
//	});
//	return false;
//});

$(document).ready(function() {
	$('#startProject').datepicker( {
		dateFormat: 'MM dd, yy'
		} );
	$('#endProject').datepicker( {
		dateFormat: 'MM dd, yy'
		} );
	} );

var c1 = 0;
var c2 = 0;
var c3 = 0;
var c4 = 0;
var c5 = 0;
var c6 = 0;
var c7 = 0;
var c8 = 0;
var c9 = 0;

//if ($('#clientId').val() == ""){
//	c1=0;
//} else {
//	c1=1;
//}
//
//if ($('#projectName').val() == ""){
//	c2=0;
//} else {
//	c2=1;
//}
//
//if ($('#startProject').val() == ""){
//	c3=0;
//} else {
//	c3=1;
//}
//
//if ($('#endProject').val() == ""){
//	c4=0;
//} else {
//	c4=1;
//}
//
//if ($('#projectRole').val() == ""){
//	c5=0;
//} else {
//	c5=1;
//}
//
//if ($('#projectPhase').val() == ""){
//	c6=0;
//} else {
//	c6=1;
//}
//
//if ($('#projectDescription').val() == ""){
//	c7=0;
//} else {
//	c7=1;
//}
//
//if ($('#projectTechnology').val() == ""){
//	c8=0;
//} else {
//	c8=1;
//}
//
//if ($('#mainTask').val() == ""){
//	c9=0;
//} else {
//	c9=1;
//}

function cek1() {
	var errorMsg = "Client Tidak Boleh Kosong";
	var warna = errorMsg.fontcolor("#cc0000");
	
	if ($('#clientId').val() == "") {
		$('#cekerror').html(warna);
		$('#cekerror').show();
		document.getElementById("clientId").style.borderColor = "#cc0000";
		c1 = 0;
	} else {
		$('#cekerror').hide();
		document.getElementById("clientId").style.borderColor = "";
		c1 = 1;
	}
}

function cek2() {
	var errorMsg = "Project Name Tidak Boleh Kosong";
	var warna = errorMsg.fontcolor("#cc0000");
	
	if ($('#projectName').val() == "") {
		$('#cekerror2').html(warna);
		$('#cekerror2').show();
		document.getElementById("projectName").style.borderColor = "#cc0000";
		c2 = 0;
	} else {
		$('#cekerror2').hide();
		document.getElementById("projectName").style.borderColor = "";
		c2 = 1;
	}
}

function cek3() {
	var errorMsg = "Start Tidak Boleh Kosong";
	var warna = errorMsg.fontcolor("#cc0000");
	
	if ($('#startProject').val() == "") {
		$('#cekerror3').html(warna);
		$('#cekerror3').show();
		document.getElementById("startProject").style.borderColor = "#cc0000";
		c3 = 0;
	} else {
		$('#cekerror3').hide();
		document.getElementById("startProject").style.borderColor = "";
		c3 = 1;
	}
}

function cek4() {
	var errorMsg = "End Tidak Boleh Kosong";
	var warna = errorMsg.fontcolor("#cc0000");
	
	if ($('#endProject').val() == "") {
		$('#cekerror4').html(warna);
		$('#cekerror4').show();
		document.getElementById("endProject").style.borderColor = "#cc0000";
		c4 = 0;
	} else {
		$('#cekerror4').hide();
		document.getElementById("endProject").style.borderColor = "";
		c4 = 1;
	}
	
	var errorMsg2 = "Tanggal Awal tidak boleh lebih besar dari tanggal akhir";
	var warna2 = errorMsg2.fontcolor("#cc0000");
	if (new Date($('#startProject').val()) > new Date($('#endProject').val())) {
		$('#cekerror10').html(warna2);
		$('#cekerror10').show();
		document.getElementById("startProject").style.borderColor = "#cc0000";
		document.getElementById("endProject").style.borderColor = "#cc0000";
		c4 = 0;
	} else {
		$('#cekerror10').hide();
		document.getElementById("startProject").style.borderColor = "";
		document.getElementById("endProject").style.borderColor = "";
		c4 = 1;
	}
}

function cek5() {
	var errorMsg = "Project Role Tidak Boleh Kosong";
	var warna = errorMsg.fontcolor("#cc0000");
	
	if ($('#projectRole').val() == "") {
		$('#cekerror5').html(warna);
		$('#cekerror5').show();
		document.getElementById("projectRole").style.borderColor = "#cc0000";
		c5 = 0;
	} else {
		$('#cekerror5').hide();
		document.getElementById("projectRole").style.borderColor = "";
		c5 = 1;
	}
}

function cek6() {
	var errorMsg = "Project Phase Tidak Boleh Kosong";
	var warna = errorMsg.fontcolor("#cc0000");
	
	if ($('#projectPhase').val() == "") {
		$('#cekerror6').html(warna);
		$('#cekerror6').show();
		document.getElementById("projectPhase").style.borderColor = "#cc0000";
		c6 = 0;
	} else {
		$('#cekerror6').hide();
		document.getElementById("projectPhase").style.borderColor = "";
		c6 = 1;
	}
}

function cek7() {
	var errorMsg = "Project Description Tidak Boleh Kosong";
	var warna = errorMsg.fontcolor("#cc0000");
	
	if ($('#projectDescription').val() == "") {
		$('#cekerror7').html(warna);
		$('#cekerror7').show();
		document.getElementById("projectDescription").style.borderColor = "#cc0000";
		c7 = 0;
	} else {
		$('#cekerror7').hide();
		document.getElementById("projectDescription").style.borderColor = "";
		c7 = 1;
	}
}

function cek8() {
	var errorMsg = "Project Technology Tidak Boleh Kosong";
	var warna = errorMsg.fontcolor("#cc0000");
	
	if ($('#projectTechnology').val() == "") {
		$('#cekerror8').html(warna);
		$('#cekerror8').show();
		document.getElementById("projectTechnology").style.borderColor = "#cc0000";
		c8 = 0;
	} else {
		$('#cekerror8').hide();
		document.getElementById("projectTechnology").style.borderColor = "";
		c8 = 1;
	}
}

function cek9() {
	var errorMsg = "Main Task Tidak Boleh Kosong";
	var warna = errorMsg.fontcolor("#cc0000");
	
	if ($('#mainTask').val() == "") {
		$('#cekerror9').html(warna);
		$('#cekerror9').show();
		document.getElementById("mainTask").style.borderColor = "#cc0000";
		c9 = 0;
	} else {
		$('#cekerror9').hide();
		document.getElementById("mainTask").style.borderColor = "";
		c9 = 1;
	}
}


function btn1() {
	if (c1 == 1 && c2 == 1 && c3 == 1 && c4 == 1 && c5 == 1
		&& c6 == 1 && c7 == 1 && c8 == 1 && c9 == 1) {
		document.getElementById("btns").type = "submit";
		$('#dataeditresource').submit(function() {
			formdata=$('#dataeditresource').serialize();
			$.ajax({
				url : '/ubahresource',
				type : 'POST',
				data : formdata,
				success : function(result) {
					alert("Edit Resource Sukses!!!")
					refreshpage();
				}
			});
			return false;
		});
	} else {
		cek1();
		cek2();
		cek3();
		cek4();
		cek5();
		cek6();
		cek7();
		cek8();
		cek9();
	}
}