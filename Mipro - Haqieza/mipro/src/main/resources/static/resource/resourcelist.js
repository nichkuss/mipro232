//$(document).ready(function() {
//	$('#tbresource').dataTable( {
//		"lengthMenu": [[2, 3, 5, -1], [2, 3, 5, "All"]]
//		} );
//} );

//$(document).ready(function(){
//  $("#tableSearch").on("keyup", function() {
//    var value = $(this).val().toLowerCase();
//    $("#resourcetb tr").filter(function() {
//      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
//    });
//  });
//});

//function mySearch() {
//	  var input, filter, table, tr, td, i, txtValue;
//	  input = document.getElementById("tableSearch");
//	  filter = input.value.toUpperCase();
//	  table = document.getElementById("tbresource");
//	  tr = table.getElementsByTagName("tr");
//
//	  // Loop through all table rows, and hide those who don't match the search query
//	  for (i = 0; i < tr.length; i++) {
//	    td = tr[i].getElementsByTagName("td")[0];
//	    if (td) {
//	      txtValue = td.textContent || td.innerText;
//	      if (txtValue.toUpperCase().indexOf(filter) > -1) {
//	        tr[i].style.display = "";
//	      } else {
//	        tr[i].style.display = "none";
//	      }
//	    }
//	  }
//	}

//function Ascending() {
//	  var table, rows, switching, i, x, y, shouldSwitch;
//	  table = document.getElementById("tbresource");
//	  switching = true;
//	  /* Make a loop that will continue until
//	  no switching has been done: */
//	  while (switching) {
//	    // Start by saying: no switching is done:
//	    switching = false;
//	    rows = table.rows;
//	    /* Loop through all table rows (except the
//	    first, which contains table headers): */
//	    for (i = 1; i < (rows.length - 1); i++) {
//	      // Start by saying there should be no switching:
//	      shouldSwitch = false;
//	      /* Get the two elements you want to compare,
//	      one from current row and one from the next: */
//	      x = rows[i].getElementsByTagName("TD")[0];
//	      y = rows[i + 1].getElementsByTagName("TD")[0];
//	      // Check if the two rows should switch place:
//	      if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
//	        // If so, mark as a switch and break the loop:
//	        shouldSwitch = true;
//	        break;
//	      }
//	    }
//	    if (shouldSwitch) {
//	      /* If a switch has been marked, make the switch
//	      and mark that a switch has been done: */
//	      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
//	      switching = true;
//	    }
//	  }
//	}

//function Descending() {
//	  var table, rows, switching, i, x, y, shouldSwitch;
//	  table = document.getElementById("tbresource");
//	  switching = true;
//	  /* Make a loop that will continue until
//	  no switching has been done: */
//	  while (switching) {
//	    // Start by saying: no switching is done:
//	    switching = false;
//	    rows = table.rows;
//	    /* Loop through all table rows (except the
//	    first, which contains table headers): */
//	    for (i = 1; i < (rows.length - 1); i++) {
//	      // Start by saying there should be no switching:
//	      shouldSwitch = false;
//	      /* Get the two elements you want to compare,
//	      one from current row and one from the next: */
//	      x = rows[i].getElementsByTagName("TD")[0];
//	      y = rows[i + 1].getElementsByTagName("TD")[0];
//	      // Check if the two rows should switch place:
//	      if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
//	        // If so, mark as a switch and break the loop:
//	        shouldSwitch = true;
//	        break;
//	      }
//	    }
//	    if (shouldSwitch) {
//	      /* If a switch has been marked, make the switch
//	      and mark that a switch has been done: */
//	      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
//	      switching = true;
//	    }
//	  }
//	}

oTable = $('#tbresource').DataTable({
	"lengthMenu": [[-1, 1, 2, 3], ["All", 1, 2, 3]],
	"pagingType": "simple"
});

$('#tableSearch').keyup(function(){
      oTable.search($(this).val()).draw() ;
})

$("#clear").click(function() {
	refreshpage();
});

//$("#clear").click(function() {
//	$('#tbresource').DataTable().search('').draw();
//	$('#tableSearch').val('');
//});

//$('#btn_search').click(function(){
//    let f = $("#from").val();
//    let t = $("#to").val();
//    
//    var rangeTanggal = [];
//    var getDates = function(startDate, endDate) {
//		var dates = [],
//		currentDate = startDate,
//		addDays = function(days) {
//			var date = new Date(this.valueOf());
//			date.setDate(date.getDate() + days);
//			return date;
//		};
//		while (currentDate <= endDate) {
//			dates.push(currentDate);
//			currentDate = addDays.call(currentDate, 1);
//		}
//		return dates;
//	};
//
//	// Date Range for User Validation
//	var dates = getDates(new Date($("#from").val()), new Date($("#to").val()));                                                                                                           
//	dates.forEach(function(date) {
//		rangeTanggal.push(date);
//	});
//	
//	for(var i = 0; i < rangeTanggal.length; i++){
//		var convertRangeTanggal = rangeTanggal[i].toString();
//		var splitRangeTanggal = convertRangeTanggal.split(' ');
//		switch(splitRangeTanggal[1]) {
//	    case "Jan" : splitRangeTanggal[1] = "01"; break;
//	    case "Feb" : splitRangeTanggal[1] = "02"; break;
//	    case "Mar" : splitRangeTanggal[1] = "03"; break;
//	    case "Apr" : splitRangeTanggal[1] = "04"; break;
//	    case "May" : splitRangeTanggal[1] = "05"; break;
//	    case "Jun" : splitRangeTanggal[1] = "06"; break;
//	    case "Jul" : splitRangeTanggal[1] = "07"; break;
//	    case "Aug" : splitRangeTanggal[1] = "08"; break;
//	    case "Sep" : splitRangeTanggal[1] = "09"; break;
//	    case "Oct" : splitRangeTanggal[1] = "10"; break;
//	    case "Nov" : splitRangeTanggal[1] = "11"; break;
//	    case "Dec" : splitRangeTanggal[1] = "12"; break;
//	    }
//		var remakeRangeTanggal = splitRangeTanggal[3]+'-'+splitRangeTanggal[1]+'-'+splitRangeTanggal[2];
//		var rangeTanggalValue = remakeRangeTanggal.toString();
//	
//		//alert(rangeTanggalValue + ", \n");
//	}
//})
//
//$('#star').keyup(function(){
//      oTable.columns(0).search($(this).val()).draw() ;
//})
//
//$('#end').keyup(function(){
//      oTable.columns(0).search($(this).val()).draw() ;
//})

$("#pgsize li").click(function() {
	var table = $('#tbresource').DataTable();
	table.page.len( $(this).text() ).draw();
});

$("#asc").click(function() {
	var table = $('#tbresource').DataTable();
	table.order( [ 0, 'asc' ]).draw();
});

$("#desc").click(function() {
	var table = $('#tbresource').DataTable();
	table.order( [ 0, 'desc' ] ).draw();
});

$("#maju").click(function() {
	var table = $('#tbresource').DataTable();
	table.page( 'next' ).draw( 'page' );
})

$("#mundur").click(function() {
	var table = $('#tbresource').DataTable();
	table.page( 'previous' ).draw( 'page' );
})


$(document).ready(function() {
	$('#startProjectL').datepicker( {
		dateFormat: 'MM dd, yy'
		} );
	$('#endProjectL').datepicker( {
		dateFormat: 'MM dd, yy'
		} );
	} );

function detailresource(t) {
	var alamat=t.getAttribute("href");
	$.ajax({
		url : alamat,
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#resourceModal').modal();
			$('#nonresourcelist').html(result);	
			$('#judul').html("Detail Resource Project");
		}
	});
}

function editresource(t) {
	var alamat=t.getAttribute("href");
	$.ajax({
		url : alamat,
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#resourceModal').modal();
			$('#nonresourcelist').html(result);
			$('#judul').html("Edit Resource Project");
		}
	});
}

function deleteresource(t) {
	var alamat=t.getAttribute("href");
	var name = t.getAttribute("value");
	$.ajax({
		url : alamat,
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#delresourceModal').modal();
			$('#delresourcelist').html(result);
			$('#lempar').text(name+' ?');
		}
	});
}