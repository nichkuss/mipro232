rolelist();
function rolelist() {
	$.ajax({
		url : '/rolelist',
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#rolelist').html(result);	
		}
	});
}

function addform() {
	$.ajax({
		url : '/roleadd',
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#roleModal').modal();
			$('#nonrolelist').html(result);
			$('#judul').html("Add Role");
		}
	});
}

function tutupmodal(){
	  $('#roleModal').modal('hide');
	  $('#delroleModal').modal('hide');
}

function closemodal() {
	$('#roleModal').hide();
	$('#delroleModal').hide();
	$('.modal-backdrop').hide();
	$('.modal-backdrop').remove();
	rolelist();
}

function refreshpage(){
	$.ajax({
		url : '/role',
		type : 'get',
		dataType : 'html',
		success : function(result) {
			closemodal();
			$('#fragment').html(result);
		}
	});
	return false;
}