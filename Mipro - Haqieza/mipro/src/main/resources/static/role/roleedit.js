//$(document).ready(function() {
//	$('#dataeditrole').bootstrapValidator({
//		// To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
//		feedbackIcons: {
//			valid: 'glyphicon glyphicon-ok',
//			invalid: 'glyphicon glyphicon-remove',
//			validating: 'glyphicon glyphicon-refresh'
//		},
//		fields: {
//			name: {
//				validators: {
//					notEmpty: {
//						message: 'Nama Agama tidak boleh kosong'
//					}
//				}
//			}
//		},
//		onSuccess: function(e, data) {
//			$('#dataeditrole').submit(function() {
//				formdata=$('#dataeditrole').serialize();
//				$.ajax({
//					url : '/ubahrole',
//					type : 'POST',
//					data : formdata,
//					success : function(result) {
//						alert("Edit role Sukses!!!")
//						refreshpage();
//					}
//				});
//				return false;
//			});
//
//		}
//
//	});    
//});

//$('#dataeditrole').submit(function() {
//	formdata=$('#dataeditrole').serialize();
//	$.ajax({
//		url : '/ubahrole',
//		type : 'POST',
//		data : formdata,
//		success : function(result) {
//			alert("Edit Role Sukses!!!")
//			refreshpage();
//		}
//	});
//	return false;
//});

var c1 = 0;
var c2 = 0;

//if ($('#code').val() == ""){
//	c1=0;
//} else {
//	c1=1;
//}
//
//if ($('#name').val() == ""){
//	c2=0;
//} else {
//	c2=1;
//}

function cek1() {
	var errorMsg = "Kode Tidak Boleh Kosong";
	var warna = errorMsg.fontcolor("#cc0000");
	
	if ($('#code').val().trim() == "") {
		$('#cekerror').html(warna);
		$('#cekerror').show();
		document.getElementById("code").style.borderColor = "#cc0000";
		c1 = 0;
	} else {
		$('#cekerror').hide();
		document.getElementById("code").style.borderColor = "";
		c1 = 1;
	}
}

function cek2() {
	var re = /^[A-Za-z]+$/;
	var errorMsg = "Nama Tidak Boleh Kosong/Tidak Boleh Ada Angka";
	var warna = errorMsg.fontcolor("#cc0000");
	var adaangka=false;
	    if(re.test(document.getElementById("name").value))
	    	adaangka=false;
	    else
	    	adaangka=true;      
	if ($('#name').val().trim() == "" || adaangka==true) {
		$('#cekerror2').html(warna);
		$('#cekerror2').show();
		document.getElementById("name").style.borderColor = "#cc0000";
		c2 = 0;
	} else {
		$('#cekerror2').hide();
		document.getElementById("name").style.borderColor = "";
		c2 = 1;
	}
}

function btn1() {
	if (c1 == 1 && c2 == 1) {
		document.getElementById("btns").type = "submit";
		$('#dataeditrole').submit(function() {
			formdata=$('#dataeditrole').serialize();
			$.ajax({
				url : '/ubahrole',
				type : 'POST',
				data : formdata,
				success : function(result) {
					if(result=="0") {
						alert("Edit Role Sukses!!!")
//						$('#roleModal').modal('toggle');
						refreshpage();
					} else {
						alert("Kode/Nama Sudah Ada");
					}
				}
			});
			return false;
		});
	} else {
		cek1();
		cek2();
	}
}