//$(document).ready(function() {
//	$('#tbrole').dataTable( {
//		"lengthMenu": [[2, 3, 5, -1], [2, 3, 5, "All"]]
//		} );
//} );
//$(document).ready(function(){
//  $("#tableSearch").on("keyup", function() {
//    var value = $(this).val().toLowerCase();
//    $("#roletb tr").filter(function() {
//      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
//    });
//  });
//});

//function mySearch() {
//	  var input, filter, table, tr, td, i, txtValue;
//	  input = document.getElementById("tableSearch");
//	  filter = input.value.toUpperCase();
//	  table = document.getElementById("tbrole");
//	  tr = table.getElementsByTagName("tr");
//
//	  // Loop through all table rows, and hide those who don't match the search query
//	  for (i = 0; i < tr.length; i++) {
//	    td = tr[i].getElementsByTagName("td")[0];
//	    if (td) {
//	      txtValue = td.textContent || td.innerText;
//	      if (txtValue.toUpperCase().indexOf(filter) > -1) {
//	        tr[i].style.display = "";
//	      } else {
//	        tr[i].style.display = "none";
//	      }
//	    }
//	  }
//	}

//function Ascending() {
//	  var table, rows, switching, i, x, y, shouldSwitch;
//	  table = document.getElementById("tbrole");
//	  switching = true;
//	  /* Make a loop that will continue until
//	  no switching has been done: */
//	  while (switching) {
//	    // Start by saying: no switching is done:
//	    switching = false;
//	    rows = table.rows;
//	    /* Loop through all table rows (except the
//	    first, which contains table headers): */
//	    for (i = 1; i < (rows.length - 1); i++) {
//	      // Start by saying there should be no switching:
//	      shouldSwitch = false;
//	      /* Get the two elements you want to compare,
//	      one from current row and one from the next: */
//	      x = rows[i].getElementsByTagName("TD")[0];
//	      y = rows[i + 1].getElementsByTagName("TD")[0];
//	      // Check if the two rows should switch place:
//	      if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
//	        // If so, mark as a switch and break the loop:
//	        shouldSwitch = true;
//	        break;
//	      }
//	    }
//	    if (shouldSwitch) {
//	      /* If a switch has been marked, make the switch
//	      and mark that a switch has been done: */
//	      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
//	      switching = true;
//	    }
//	  }
//	}
//
//function Descending() {
//	  var table, rows, switching, i, x, y, shouldSwitch;
//	  table = document.getElementById("tbrole");
//	  switching = true;
//	  /* Make a loop that will continue until
//	  no switching has been done: */
//	  while (switching) {
//	    // Start by saying: no switching is done:
//	    switching = false;
//	    rows = table.rows;
//	    /* Loop through all table rows (except the
//	    first, which contains table headers): */
//	    for (i = 1; i < (rows.length - 1); i++) {
//	      // Start by saying there should be no switching:
//	      shouldSwitch = false;
//	      /* Get the two elements you want to compare,
//	      one from current row and one from the next: */
//	      x = rows[i].getElementsByTagName("TD")[0];
//	      y = rows[i + 1].getElementsByTagName("TD")[0];
//	      // Check if the two rows should switch place:
//	      if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
//	        // If so, mark as a switch and break the loop:
//	        shouldSwitch = true;
//	        break;
//	      }
//	    }
//	    if (shouldSwitch) {
//	      /* If a switch has been marked, make the switch
//	      and mark that a switch has been done: */
//	      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
//	      switching = true;
//	    }
//	  }
//	}

oTable = $('#tbrole').DataTable({
	"lengthMenu": [[-1, 1, 2, 3], ["All", 1, 2, 3]],
	"pagingType": "simple"
});   //pay attention to capital D, which is mandatory to retrieve "api" datatables' object, as @Lionel said

$('#btn_search').click(function(){
      oTable.search($('#tableSearch').val()).draw() ;
})

//var table = $('#tbreligion').DataTable();
// 
//// #column3_search is a <input type="text"> element
//$('#column3').on( 'keyup', function () {
//    table
//        .columns( 3 )
//        .search( this.value )
//        .draw();
//} );

$("#clear").click(function() {
	$('#tbrole').DataTable().search('').draw();
	$('#tableSearch').val('');
});

$("#pgsize li").click(function() {
	var table = $('#tbrole').DataTable();
	table.page.len( $(this).text() ).draw();
	checkData();
});

$("#asc").click(function() {
	var table = $('#tbrole').DataTable();
	table.order( [ 0, 'asc' ]).draw();
});

$("#desc").click(function() {
	var table = $('#tbrole').DataTable();
	table.order( [ 0, 'desc' ] ).draw();
});

function editrole(t) {
	var alamat=t.getAttribute("href");
	$.ajax({
		url : alamat,
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#roleModal').modal();
			$('#nonrolelist').html(result);
			$('#judul').html("Edit Role");
		}
	});
}

function deleterole(t) {
	var alamat=t.getAttribute("href");
	$.ajax({
		url : alamat,
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#delroleModal').modal();
			$('#delrolelist').html(result);		
		}
	});
}