sertifikasilist();

function sertifikasilist() {
	$.ajax({
		url : '/sertifikasilist',
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#sertifikasilist').html(result);

		}

	});
}

function addform() {
	$.ajax({
		url : '/sertifikasiadd',
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#sertifikasiModal').modal();
			$('#nonsertifikasilist').html(result);
			
		}
	
	});
}

function tutupmodal(){
		  $('#sertifikasiModal').modal('hide');
		  $('#delsertifikasiModal').modal('hide');
}

function closemodal() {
	$('#sertifikasiModal').hide();
	$('.modal-backdrop').hide();
	$('.modal-backdrop').remove();
}

function refreshpage(){
	$.ajax({
		url : '/sertifikasi',
		type : 'get',
		dataType : 'html',
		success : function(result) {
			closemodal();
//			$('#fragment').html(result);
			$('#pelamarmodal').modal({backdrop: 'static', keyboard: false}) 
			$('#pelamarisi').html(result);

		}
	});
	return false;
}