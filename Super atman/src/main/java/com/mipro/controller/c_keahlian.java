package com.mipro.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mipro.dto.vwahli;
import com.mipro.model.m_keahlian;
import com.mipro.model.m_skillLevel;
import com.mipro.service.s_keahlian;
import com.mipro.service.s_skillLevel;




@Controller
public class c_keahlian {
	
	@Autowired
	private s_keahlian sk;
	@Autowired
	private s_skillLevel sl;
	
	@RequestMapping("keahlianlist/{ids}")
	public String keahlianlist(Model model, @PathVariable(name="ids") Long ids) {

		List<vwahli>semuakeahlian=sk.semuakeahlianj(ids);
		model.addAttribute("keahlianbyid", ids);
		model.addAttribute("semuakeahlian",semuakeahlian);
		return "keahlian/keahlianlist";
	}
	
	@RequestMapping("keahlian")
	public String keahlian() {
		return "keahlian/keahlian";
	}
	
	@RequestMapping("keahlianadd/{ids}")
	public String keahlianadd(Model model, @PathVariable(name="ids") Long ids) {
		//bid
		model.addAttribute("ids", ids);
		
		List<m_skillLevel>listskill=sl.semuaskill();		
		model.addAttribute("listskill",listskill);
		return "keahlian/keahlianadd";
	}
	
	@ResponseBody
	@RequestMapping("simpankeahlian")
	public String simpankeahlian(@ModelAttribute()m_keahlian keahlianModel) {
		
		//System.out.println(keahlianModel.getSkillLevelId());
		//munculin creator
		keahlianModel.setCreatedBy(new Long(1122));
		
		//munculin waktu update
		keahlianModel.setCreatedOn(new Date());
		
		sk.simpankeahlian(keahlianModel);
		return "";
	}
	
	@ResponseBody
	@RequestMapping("ubahkeahlian")
	public String ubahkeahlian(@ModelAttribute()m_keahlian keahlianModel) {
		
		//created
		m_keahlian ahlibyid=sk.keahlianbyid(keahlianModel.getId());
		keahlianModel.setBiodataId(ahlibyid.getBiodataId());
		
		keahlianModel.setCreatedBy(ahlibyid.getCreatedBy());
		keahlianModel.setCreatedOn(ahlibyid.getCreatedOn());
		//modifby
		keahlianModel.setModifiedBy(new Long(1122));
		//date
		keahlianModel.setModifiedOn(new Date());
		
		sk.simpankeahlian(keahlianModel);
		return "";
	}
	
	@ResponseBody
	@RequestMapping("hapuskeahlian/{ids}")
	public String hapuskeahlian(@PathVariable(name="ids")Long ids) {
		m_keahlian keahlianbyid=sk.keahlianbyid(ids);
		
		//deleteby		
		keahlianbyid.setDeletedBy(new Long(1122));
		//time
		keahlianbyid.setDeletedOn(new Date());
		
		sk.simpankeahlian(keahlianbyid);
		sk.hapuskeahlian(ids);
		return "";
	}
	
	@RequestMapping("keahlianedit/{ids}")
	public String keahlianedit(Model model,
							@PathVariable(name = "ids")Long ids) {
		m_keahlian keahlianbyid = sk.keahlianbyid(ids);
		model.addAttribute("ahlibyid", keahlianbyid);
		
		List<m_skillLevel>listskill=sl.semuaskill();		
		model.addAttribute("listskill",listskill);
		
		return "keahlian/keahlianedit";
	}
	@RequestMapping("keahliandelete/{ids}")
	public String keahliandelete(Model model, @PathVariable(name= "ids")Long ids) {
		m_keahlian keahlianbyid = sk.keahlianbyid(ids);
		model.addAttribute("ahlibyid", keahlianbyid);
		return "keahlian/keahliandelete";
	}

}
