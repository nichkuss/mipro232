package com.mipro.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mipro.dto.vwnote;
import com.mipro.model.catatanModel;
import com.mipro.model.catatanTypeModel;
import com.mipro.service.catatanService;
import com.mipro.service.catatanTypeService;

@Controller
public class catatanController {
	@Autowired
	private catatanService cs;
	@Autowired
	private catatanTypeService cts;
	
	@RequestMapping("catatanlist/{ids}")
	public String catatanlist(Model model, @PathVariable(name="ids") Long ids) {		
	
		List<vwnote>semuacatatan=cs.semuanotej(ids);
		model.addAttribute("catatanbyid",ids);
		model.addAttribute("semuacatatan",semuacatatan);
		return "catatan/catatanlist";
	}
	
	@RequestMapping("catatan")
	public String catatan() {
		return "catatan/catatan";
	}
	
	@RequestMapping("catatanadd/{ids}")
	public String catatanadd(Model model, @PathVariable(name="ids") Long ids) {
		//bid
		model.addAttribute("ids", ids);
				
				
		List<catatanTypeModel>tipenote=cts.semuatipe();		
		model.addAttribute("tipenote",tipenote);
				
		return "catatan/catatanadd";
	}
	
	@ResponseBody
	@RequestMapping("simpancatatan")
	public String simpancatatan(@ModelAttribute()catatanModel catatanmodel) {
				
		//munculin creator		
		catatanmodel.setCreatedBy(new Long(catatanmodel.getBiodataId()));
		
		//munculin waktu update
		catatanmodel.setCreatedOn(new Date());
		
		cs.simpancatatan(catatanmodel);
		return "";
	}
	
	@ResponseBody
	@RequestMapping("ubahcatatan")
	public String ubahkeahlian(@ModelAttribute()catatanModel catatanmodel) {
		
		//created
		catatanModel notebyid=cs.catatanbyid(catatanmodel.getId());
		
		catatanmodel.setBiodataId(notebyid.getBiodataId());
		
		catatanmodel.setCreatedBy(notebyid.getCreatedBy());
		catatanmodel.setCreatedOn(notebyid.getCreatedOn());
		//modifby
		catatanmodel.setModifiedBy(new Long(catatanmodel.getBiodataId()));
		//date
		catatanmodel.setModifiedOn(new Date());
		
		cs.simpancatatan(catatanmodel);
		return "";
	}
	
	@ResponseBody
	@RequestMapping("hapuscatatan/{ids}")
	public String hapuscatatan(@PathVariable(name="ids")Long ids) {
		catatanModel notebyid=cs.catatanbyid(ids);
		
		//deleteby		
		notebyid.setDeletedBy(new Long(1));
		//time
		notebyid.setDeletedOn(new Date());
		
		cs.simpancatatan(notebyid);
		cs.hapuscatatan(ids);
		return "";
	}
	
	@RequestMapping("catatanedit/{ids}")
	public String catatanedit(Model model,
							@PathVariable(name = "ids")Long ids) {
		catatanModel notebyid = cs.catatanbyid(ids);
		model.addAttribute("notebyid", notebyid);
		
		List<catatanTypeModel>tipenote=cts.semuatipe();		
		model.addAttribute("tipenote",tipenote);
		
		return "catatan/catatanedit";
	}
	@RequestMapping("catatandelete/{ids}")
	public String catatandelete(Model model, @PathVariable(name= "ids")Long ids) {
		catatanModel notebyid = cs.catatanbyid(ids);
		model.addAttribute("notebyid", notebyid);
		return "catatan/catatandelete";
	}


}
