package com.mipro.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


import com.mipro.dto.vwprojek;
import com.mipro.model.pengalamankerjaModel;
import com.mipro.model.projekModel;
import com.mipro.model.timeperiodModel;
import com.mipro.service.pengalamankerjaService;
import com.mipro.service.projekService;
import com.mipro.service.timeperiodService;

@Controller
public class projekController {

	@Autowired
	private projekService p;
	
	@Autowired
	private pengalamankerjaService ps;
	
	@Autowired
	private timeperiodService tp;
	
	public void isitahun(Model model) {
		List<Integer>tahun= new ArrayList();
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		for (int i = 0; i <= 30; i++) {
			tahun.add(year - i);
		}
		model.addAttribute("tahun", tahun);
	}
	
	@RequestMapping("projeklist")
	public String projeklist(Model model) {
		List<vwprojek>semuaprojek = p.semuaprojek();
		model.addAttribute("semuaprojek", semuaprojek);
		
		return "projek/projeklist";
	}
	
	
	@RequestMapping("projekadd/{ids}")
	public String projekadd(Model model, @PathVariable(name = "ids") Long ids) {
		pengalamankerjaModel pengalamankerjabyid = ps.pengalamankerjabyid(ids);
		model.addAttribute("pengalamankerjabyid", pengalamankerjabyid);
		isitahun(model);
		
		List<timeperiodModel>pilih=tp.semuatimeperiod();
		model.addAttribute("pilih", pilih);
		
		return "projek/projekadd";
	}
	
	@ResponseBody
	@RequestMapping("simpanprojek")
	public String simpanprojek(@ModelAttribute() projekModel projekmodel) {
		
		//create by		
		projekmodel.setCreatedBy(new Long (1));
		//create on
		projekmodel.setCreatedOn(new Date());
		
		p.simpanprojek(projekmodel);
		return "";
	}
	
	
	@ResponseBody
	@RequestMapping("ubahprojek")
	public String ubahprojek(@ModelAttribute()projekModel projekmodel) {
		//set create
		projekModel projekbyid = p.projekbyid(projekmodel.getId());
		System.out.println(projekbyid);
		projekmodel.setRiwayatPekerjaanId(projekbyid.getRiwayatPekerjaanId());
		projekmodel.setCreatedBy(projekbyid.getCreatedBy());
		projekmodel.setCreatedOn(projekbyid.getCreatedOn());
		//update by		
		projekmodel.setModifiedBy(new Long (1));
		//update on
		projekmodel.setModifiedOn(new Date());
		p.simpanprojek(projekmodel);
		return "";
	}
	
	
	@ResponseBody
	@RequestMapping("hapusprojek/{ids}")
	public String hapusprojek(@PathVariable(name = "ids")Long ids) {
		projekModel projekbyid = p.projekbyid(ids);
		//delete by
		projekbyid.setDeletedBy(new Long(1));
		//delete on
		projekbyid.setDeletedOn(new Date());
		p.hapusprojek(ids);
		return "";
	}
	
	
	@RequestMapping("projekedit/{ids}")
	public String projekedit (Model model, @PathVariable(name = "ids") Long ids) {
		projekModel projekbyid = p.projekbyid(ids);
		model.addAttribute("projekbyid", projekbyid);		

		isitahun(model);
		List<timeperiodModel>pilih=tp.semuatimeperiod();
		model.addAttribute("pilih", pilih);
		
		return "projek/projekedit";
	}
	
	@RequestMapping("projekdelete/{ids}")
	public String projekdelete(Model model, @PathVariable(name = "ids") long ids) {
		projekModel projekbyid = p.projekbyid(ids);
		model.addAttribute("projekbyid", projekbyid);
		
		return "projek/projekdelete";

	}
	
	
	
	
	
}
