package com.mipro.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


import com.mipro.model.timeperiodModel;
import com.mipro.service.timeperiodService;

@Controller
public class timeperiodController {
	
	@Autowired
	private timeperiodService ts;
	
	@RequestMapping("timeperiod")
	public String timeperiod() {
		
		return"timeperiod/timeperiod";
	}
	
	@RequestMapping("timeperiodlist")
	public String timeperiodlist(Model model) {
		List<timeperiodModel>semuatimeperiod = ts.semuatimeperiod();
		model.addAttribute("semuatimeperiod", semuatimeperiod);
		return "timeperiod/timeperiodlist";
	}
	
	@RequestMapping("timeperiodadd")  
	public String timeperiodadd() {
		
		return "timeperiod/timeperiodadd";
	}
	
	@ResponseBody
	@RequestMapping("simpantimeperiod")
	public String simpantimeperiod(@ModelAttribute() timeperiodModel timeperiodModel) {
		//create by
		long user = 5;
		timeperiodModel.setCreatedBy(user);
		//create on
		timeperiodModel.setCreatedOn(new Date());
		ts.simpantimeperiod(timeperiodModel);
		return "";
	}
	
	@ResponseBody
	@RequestMapping("ubahtimeperiod")
	public String ubahtimeperiod(@ModelAttribute()timeperiodModel timeperiodModel) {
		//set create
		timeperiodModel timeperiodbyid = ts.timeperiodbyid(timeperiodModel.getId());
		timeperiodModel.setCreatedBy(timeperiodbyid.getCreatedBy());
		timeperiodModel.setCreatedOn(timeperiodbyid.getCreatedOn());
		//update by
		Long user = (long)8;
		timeperiodModel.setModifiedBy(new Long(user));
		//update on
		timeperiodModel.setModifiedOn(new Date());
		ts.simpantimeperiod(timeperiodModel);
		return "";
	}
	
	@ResponseBody
	@RequestMapping("hapustimeperiod/{ids}")
	public String hapustimeperiod(@PathVariable(name = "ids")Long ids) {
		timeperiodModel timeperiodbyid = ts.timeperiodbyid(ids);
		//delete by
		Long user = (long)9;
		timeperiodbyid.setDeletedBy(new Long(user));
		//delete on
		timeperiodbyid.setDeletedOn(new Date());
		ts.hapustimeperiod(ids);
		return "";
	}
	
	@RequestMapping("timeperiodedit/{ids}")
	public String timeperiodedit (Model model, 
			@PathVariable(name = "ids") Long ids) {
		timeperiodModel timeperiodbyid = ts.timeperiodbyid(ids);
		model.addAttribute("timeperiodbyid", timeperiodbyid);
		return "timeperiod/timeperiodedit";
	}
	
	@RequestMapping("timeperioddelete/{ids}")
	public String timeperioddelete(Model model, @PathVariable(name = "ids") Long ids) {
		timeperiodModel timeperiodbyid = ts.timeperiodbyid(ids);
		model.addAttribute("timeperiodbyid", timeperiodbyid);
		return "timeperiod/timeperioddelete";

	}
	
	
	
	
	
	

}
