package com.mipro.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mipro.dto.detildto;
import com.mipro.dto.inuddetail;
import com.mipro.dto.vwundangan;
import com.mipro.model.biodataModel;
import com.mipro.model.roModel;
import com.mipro.model.scheduleTypeModel;
import com.mipro.model.troModel;
import com.mipro.model.undanganDetailModel;
import com.mipro.model.undanganModel;
import com.mipro.service.pelamarService;
import com.mipro.service.roService;
import com.mipro.service.scheduleTypeService;
import com.mipro.service.troService;
import com.mipro.service.undanganDetailService;
import com.mipro.service.undanganService;


@Controller
public class undanganController {

	@Autowired
	private undanganService us;
	
	@Autowired
	private undanganDetailService uds;
	
	@Autowired
	private pelamarService ps;
	
	@Autowired 
	private scheduleTypeService ss;
	
	@Autowired
	private roService ro;
	
	@Autowired
	private troService tro;
	
	@RequestMapping("undanganlist")
	public String undanganlist(Model model) {

		List<vwundangan>semuaundangan=us.viewpelamar();
		model.addAttribute("semuaundangan",semuaundangan);
		
		return "undangan/undanganlist";
	}
	
	@RequestMapping("undangan")
	public String undangan() {
		return "undangan/undangan";
	}
	
	@RequestMapping("undanganadd")
	public String undanganadd(Model model) {
		//pelamar
		List<biodataModel>listpelamar=ps.semuapelamar();
		model.addAttribute("listpelamar", listpelamar);
		
		//karyawan
		List<biodataModel>listkaryawan=ps.semuakaryawan();
		model.addAttribute("listkaryawan", listkaryawan);
		
		//undangandetail
		List<undanganDetailModel>listdetail=uds.detail();
		model.addAttribute("listdetail", listdetail);
		
		//jenisundangan
		List<scheduleTypeModel>listtipe=ss.semuascheduletipe();
		model.addAttribute("listtipe", listtipe);
		
		
		return "undangan/undanganadd";
	}
	

	
	@ResponseBody
	@RequestMapping("simpanundangan")
	public String simpanundangan(@ModelAttribute()undanganModel undanganModel,@ModelAttribute() inuddetail inud) {
		
		undanganDetailModel undanganDetailModel= new undanganDetailModel();
		
		
		//munculin creator
		undanganModel.setCreatedBy(new Long(1));
		undanganDetailModel.setCreatedBy(new Long(1));
		
		//munculin waktu update
		undanganModel.setCreatedOn(new Date());
		undanganDetailModel.setCreatedOn(new Date());	
		
		
		undanganModel.setInvitationCode("UD00000"+undanganDetailModel.getUndanganId()); 
		
		us.simpanundangan(undanganModel);
		
		//simpandetail
		undanganDetailModel.setNotes(inud.getNotes());
		undanganDetailModel.setBiodataId(inud.getBiodataId());
		undanganDetailModel.setUndanganId(undanganModel.getId());				
		uds.simpandetail(undanganDetailModel);		
		
		//simpanundangan
		
		undanganModel.setInvitationCode("UD00000"+undanganDetailModel.getUndanganId()); 
		us.simpanundangan(undanganModel);
				
		
		return "";
	}
	
	@RequestMapping("undangandetail/{ids}")
	public String undangandetail(Model model, @PathVariable(name="ids")Long ids) {
//		undanganModel undanganbyid=us.undanganbyid(ids);
//		model.addAttribute("undanganbyid", undanganbyid);
		
		detildto detil= us.detiljoin(ids);
		model.addAttribute("detil", detil);
		
		
		return"undangan/undangandetail";
	}
	
	@ResponseBody
	@RequestMapping("ubahundangan")
	public String ubahundangan(@ModelAttribute()undanganModel undanganModel, @ModelAttribute() undanganDetailModel undanganDetailModel) {
		
		undanganModel undanganbyid=us.undanganbyid(undanganModel.getId());
		undanganDetailModel detailbyid=uds.detailbyid(undanganDetailModel.getId());
		//created		
		undanganModel.setCreatedBy(undanganbyid.getCreatedBy());
		undanganModel.setCreatedOn(undanganbyid.getCreatedOn());
		
		undanganDetailModel.setCreatedBy(detailbyid.getCreatedBy());
		undanganDetailModel.setCreatedOn(detailbyid.getCreatedOn());
		
		//modifby
		undanganModel.setModifiedBy(new Long(1));
		
		undanganDetailModel.setModifiedBy(new Long(1));
		//date
		undanganModel.setModifiedOn(new Date());
		undanganDetailModel.setModifiedBy(new Long(1));
		
		us.simpanundangan(undanganModel);
		uds.simpandetail(undanganDetailModel);
		return "";
	}
	
	@ResponseBody
	@RequestMapping("hapusundangan/{ids}")
	public String hapusundangan(@PathVariable(name="ids")Long ids) {
		undanganModel undanganbyid=us.undanganbyid(ids);
		
		//deleteby		
		undanganbyid.setDeletedBy(new Long(1));
		//time
		undanganbyid.setDeletedOn(new Date());
		
		us.simpanundangan(undanganbyid);
		us.hapusundangan(ids);
		
		
		return "";
	}
	
	@RequestMapping("undanganedit/{ids}")
	public String undanganedit(Model model,
							@PathVariable(name = "ids")Long ids) {
		undanganModel undanganbyid = us.undanganbyid(ids);
		model.addAttribute("undanganbyid", undanganbyid);
		
		undanganDetailModel detailbyid=uds.detailbyid(ids);
		model.addAttribute("det", detailbyid);
		
		//pelamar
		List<biodataModel>listpelamar=ps.semuapelamar();
		model.addAttribute("listpelamar", listpelamar);
		
		//karyawan
		List<biodataModel>listkaryawan=ps.semuakaryawan();
		model.addAttribute("listkaryawan", listkaryawan);
		
		//undangandetail
		List<undanganDetailModel>listdetail=uds.detail();
		model.addAttribute("listdetail", listdetail);
		
		//jenisundangan
		List<scheduleTypeModel>listtipe=ss.semuascheduletipe();
		model.addAttribute("listtipe", listtipe);
				
		return "undangan/undanganedit";
	}
	@RequestMapping("undangandelete/{ids}")
	public String undangandelete(Model model, @PathVariable(name= "ids")Long ids) {
		undanganModel undanganbyid = us.undanganbyid(ids);
		model.addAttribute("undanganbyid", undanganbyid);
		return "undangan/undangandelete";
	}
}
