package com.mipro.dto;

import java.util.Date;

public class detildto {
	private Long id;
	private String invitationCode;
	private Date invitationDate;
	private String time;
	
	private String fullName;
	private String namaro;
	private String namatro;
	private String name;
	
	private String otherRoTro;
	private String location;
	private String notes;
	public detildto(Long id, String invitationCode, Date invitationDate, String time, String fullName, String namaro,
			String namatro, String name, String otherRoTro, String location, String notes) {
		this.id = id;
		this.invitationCode = invitationCode;
		this.invitationDate = invitationDate;
		this.time = time;
		this.fullName = fullName;
		this.namaro = namaro;
		this.namatro = namatro;
		this.name = name;
		this.otherRoTro = otherRoTro;
		this.location = location;
		this.notes = notes;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getInvitationCode() {
		return invitationCode;
	}
	public void setInvitationCode(String invitationCode) {
		this.invitationCode = invitationCode;
	}
	public Date getInvitationDate() {
		return invitationDate;
	}
	public void setInvitationDate(Date invitationDate) {
		this.invitationDate = invitationDate;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getNamaro() {
		return namaro;
	}
	public void setNamaro(String namaro) {
		this.namaro = namaro;
	}
	public String getNamatro() {
		return namatro;
	}
	public void setNamatro(String namatro) {
		this.namatro = namatro;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOtherRoTro() {
		return otherRoTro;
	}
	public void setOtherRoTro(String otherRoTro) {
		this.otherRoTro = otherRoTro;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	
}
