package com.mipro.dto;

public class inuddetail {
	
	private Long id;
	private Long biodataId;
	private String notes;
	
	
	
	public inuddetail(Long id, Long biodataId, String notes) {
		this.id = id;
		this.biodataId = biodataId;
		this.notes = notes;
	}
	
	public Long getBiodataId() {
		return biodataId;
	}
	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	


	
	
}
