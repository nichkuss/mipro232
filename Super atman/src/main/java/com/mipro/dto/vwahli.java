package com.mipro.dto;



public class vwahli {

	private Long Id;
	private String skillName;
	private String name;
	
	
	public vwahli(Long id, String skillName, String name) {
		Id = id;
		this.skillName = skillName;
		this.name = name;
	}
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public String getSkillName() {
		return skillName;
	}
	public void setSkillName(String skillName) {
		this.skillName = skillName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}