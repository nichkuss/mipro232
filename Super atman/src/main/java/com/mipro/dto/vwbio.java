package com.mipro.dto;

public class vwbio {
	
	private long Id;
	private String fullName;
	private String nickName;
	private String email;
	private String phoneNumber1;
	private String schoolName;
	
	public vwbio(long id, String fullName, String nickName, String email, String phoneNumber1, String schoolName) {
		Id = id;
		this.fullName = fullName;
		this.nickName = nickName;
		this.email = email;
		this.phoneNumber1 = phoneNumber1;
		this.schoolName = schoolName;
	}

	public long getId() {
		return Id;
	}

	public void setId(long id) {
		Id = id;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber1() {
		return phoneNumber1;
	}

	public void setPhoneNumber1(String phoneNumber1) {
		this.phoneNumber1 = phoneNumber1;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	
	
	
	
	
}
