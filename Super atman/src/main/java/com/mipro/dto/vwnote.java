package com.mipro.dto;

public class vwnote {

	private Long id;
	private String title;	
	private String name;
	private String nickName;
	public vwnote(Long id, String title, String name, String nickName) {
		this.id = id;
		this.title = title;
		this.name = name;
		this.nickName = nickName;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	
	
	
	
}