package com.mipro.dto;

public class vwprojek {

	private Long id;
	private String startYear;
	private String startMonth;
	private String projectName;
	private Integer projectDuration;
	private String client;
	private String projectPosition;
	private String description;
	private String name;
	
	public vwprojek(Long id, String startYear, String startMonth, String projectName, Integer projectDuration,
			String client, String projectPosition, String description, String name) {
		this.id = id;
		this.startYear = startYear;
		this.startMonth = startMonth;
		this.projectName = projectName;
		this.projectDuration = projectDuration;
		this.client = client;
		this.projectPosition = projectPosition;
		this.description = description;
		this.name = name;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getStartYear() {
		return startYear;
	}
	public void setStartYear(String startYear) {
		this.startYear = startYear;
	}
	public String getStartMonth() {
		return startMonth;
	}
	public void setStartMonth(String startMonth) {
		this.startMonth = startMonth;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public Integer getProjectDuration() {
		return projectDuration;
	}
	public void setProjectDuration(Integer projectDuration) {
		this.projectDuration = projectDuration;
	}
	public String getClient() {
		return client;
	}
	public void setClient(String client) {
		this.client = client;
	}
	public String getProjectPosition() {
		return projectPosition;
	}
	public void setProjectPosition(String projectPosition) {
		this.projectPosition = projectPosition;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}
