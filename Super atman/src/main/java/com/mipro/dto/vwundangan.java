package com.mipro.dto;

import java.util.Date;

public class vwundangan {
	private Long id;
	private String invitationCode;
	private String fullName;
	private String schoolName;
	private String major;
	private Date invitationDate;

	
	public vwundangan(Long id, String invitationCode, String fullName, String schoolName, String major,
			Date invitationDate) {
		this.id = id;
		this.invitationCode = invitationCode;
		this.fullName = fullName;
		this.schoolName = schoolName;
		this.major = major;
		this.invitationDate = invitationDate;
//		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getInvitationCode() {
		return invitationCode;
	}

	public void setInvitationCode(String invitationCode) {
		this.invitationCode = invitationCode;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public Date getInvitationDate() {
		return invitationDate;
	}

	public void setInvitationDate(Date invitationDate) {
		this.invitationDate = invitationDate;
	}

	public String getMajor() {
		return major;
	}

	public void setMajor(String major) {
		this.major = major;
	}

	
	
}
