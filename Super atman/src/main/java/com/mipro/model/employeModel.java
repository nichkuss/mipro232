package com.mipro.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "x_employee")

public class employeModel extends CommonEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column (name = "id", length = 11)
	private Long id;
	
	@Column (name = "biodata_id", length = 11, nullable = false)
	private Long biodataId;
	
	@Column (name = "is_idle")
	private Boolean isIdle = false;
	
	@Column (name = "is_ero")
	private Boolean isEro = false;
	
	@Column (name = "is_user_client")
	private Boolean isUserClient = false;
	
	@Column (name = "ero_email", length = 100)
	private String eroEmail;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getBiodataId() {
		return biodataId;
	}

	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}

	public Boolean getIsIdle() {
		return isIdle;
	}

	public void setIsIdle(Boolean isIdle) {
		this.isIdle = isIdle;
	}

	public Boolean getIsEro() {
		return isEro;
	}

	public void setIsEro(Boolean isEro) {
		this.isEro = isEro;
	}

	public Boolean getIsUserClient() {
		return isUserClient;
	}

	public void setIsUserClient(Boolean isUserClient) {
		this.isUserClient = isUserClient;
	}

	public String getEroEmail() {
		return eroEmail;
	}

	public void setEroEmail(String eroEmail) {
		this.eroEmail = eroEmail;
	}
	
	
}
