package com.mipro.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name = "x_skill_level")
public class m_skillLevel {
	//kolom
		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		@Column(name = "id", nullable = false, length = 11)
	    private long Id;
		
		
		@Column(name = "created_by", nullable = false, length = 11)
	    private Long createdBy;

	    @Column(name = "created_on", nullable = false)
	    private Date createdOn;

	    @Column(name = "modified_by", nullable = true, length = 11)
	    private Long modifiedBy;

	    @Column(name = "modified_on", nullable = true)
	    private Date modifiedOn;

	    @Column(name = "deleted_by", nullable = true, length = 11)
	    private Long deletedBy;

	    @Column(name = "deleted_on", nullable = true)
	    private Date deletedOn;

	    @Column(name = "is_delete", nullable = false)
	    private Boolean isDelete;

	    @Column(name = "name", nullable = false)
	    private String name;

		public long getId() {
			return Id;
		}

		public void setId(long id) {
			Id = id;
		}

		public Long getCreatedBy() {
			return createdBy;
		}

		public void setCreatedBy(Long createdBy) {
			this.createdBy = createdBy;
		}

		public Date getCreatedOn() {
			return createdOn;
		}

		public void setCreatedOn(Date createdOn) {
			this.createdOn = createdOn;
		}

		public Long getModifiedBy() {
			return modifiedBy;
		}

		public void setModifiedBy(Long modifiedBy) {
			this.modifiedBy = modifiedBy;
		}

		public Date getModifiedOn() {
			return modifiedOn;
		}

		public void setModifiedOn(Date modifiedOn) {
			this.modifiedOn = modifiedOn;
		}

		public Long getDeletedBy() {
			return deletedBy;
		}

		public void setDeletedBy(Long deletedBy) {
			this.deletedBy = deletedBy;
		}

		public Date getDeletedOn() {
			return deletedOn;
		}

		public void setDeletedOn(Date deletedOn) {
			this.deletedOn = deletedOn;
		}

		public Boolean getIsDelete() {
			return isDelete;
		}

		public void setIsDelete(Boolean isDelete) {
			this.isDelete = isDelete;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
	    
		

}