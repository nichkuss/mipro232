package com.mipro.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "x_undangan_detail")
public class undanganDetailModel {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)

	@Column(name = "id", nullable = false, length = 11)
	private Long id;

	@Column(name = "created_by", nullable = false, length = 11)
	private Long createdBy;

	@Column(name = "created_on", nullable = false)
	private Date createdOn;

	@Column(name = "modified_by", length = 11)
	private Long modifiedBy;

	@Column(name = "modified_on")
	private Date modifiedOn;
	
	@Column(name = "deleted_by", length = 11)
	private Long deletedBy;

	@Column(name = "deleted_on")
	private Date deletedOn;

	@Column(name = "is_delete")
	private Boolean isDelete;

	@Column(name = "undangan_id", nullable = false, length = 11)
	private Long undanganId;

//	    @ManyToOne
//	    @JoinColumn(name = "undangan_id", foreignKey = @ForeignKey(name = "fk_undangan_id_detail"), insertable = false, updatable = false)
//
//	    public M_Undangan m_undangan;
//
//	    public M_UndanganDetail() {
//
//	    }

	@Column(name = "biodata_id", nullable = false, length = 11)
	private Long biodataId;

//	    @ManyToOne
//	    @JoinColumn(name = "biodata_id", foreignKey = @ForeignKey(name = "fk_biodata_id_detail"), insertable = false, updatable = false)
//
//	    public M_Biodata m_biodata;

	// public M_ViewPelamar m_viewPelamar;

	@Column(name = "notes", length = 1000)
	private String notes;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Long getUndanganId() {
		return undanganId;
	}

	public void setUndanganId(Long undanganId) {
		this.undanganId = undanganId;
	}

	public Long getBiodataId() {
		return biodataId;
	}

	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	
}
