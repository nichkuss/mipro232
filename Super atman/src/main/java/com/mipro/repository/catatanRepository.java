package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.mipro.dto.vwnote;
import com.mipro.model.biodataModel;
import com.mipro.model.catatanModel;

public interface catatanRepository extends JpaRepository<catatanModel, Long> {

	@Query("select c from catatanModel c where c.id=?1")
	public catatanModel catatanbyid(Long ids);

	// isdel
	@Modifying
	@Query("update catatanModel set isDelete = true where id=?1")
	int deleteflag(Long ids);

	//custom
	@Query("select k from catatanModel k where k.isDelete=false order by k.id")
	public List<catatanModel> semuacatatan();
	
	//biodatabyid
	@Query("select b from biodataModel b where b.id=?1")
	public biodataModel biodatabyid(Long ids);

	//join
	@Query("select new com.mipro.dto.vwnote( c.id,  c.title, t.name, b.nickName) from catatanModel c INNER join catatanTypeModel t on c.noteTypeId=t.id "
			+ "INNER join biodataModel b on b.addrbookId=c.createdBy "
			+ "INNER join biodataModel p on p.id=c.biodataId where c.isDelete=false AND c.biodataId=?1 order by c.id")
	public List<vwnote> semuanotej(Long ids);

	

	
}
