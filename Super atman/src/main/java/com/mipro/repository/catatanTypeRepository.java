package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mipro.model.catatanTypeModel;

public interface catatanTypeRepository extends JpaRepository<catatanTypeModel, Long> {
	
	@Query("select t from catatanTypeModel t where t.isDelete=false")
	List<catatanTypeModel> semuatipe();
}
