package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mipro.dto.vwbio;
import com.mipro.model.biodataModel;

public interface pelamarRepository extends JpaRepository<biodataModel, Long> {
	//list pelamar
	@Query("select p from biodataModel p where p.id IN (1,2,3,4)")
	List<biodataModel>semuapelamar();
	//list karyawan
	@Query("select p from biodataModel p where p.id NOT IN (1,2,3,4)")
	List<biodataModel>semuakaryawan();

	@Query("select s from biodataModel s where s.id=?1")  // k  terakhir = singakatan
	biodataModel pelamarbyid(long ids);

	@Query("select new com.mipro.dto.vwbio( b.id,  b.fullName, b.nickName, b.email, b.phoneNumber1, p.schoolName) from biodataModel b INNER join pendidikanModel p on b.id=p.biodataid")
	List<vwbio> semuapelamarj();
	
	
}
