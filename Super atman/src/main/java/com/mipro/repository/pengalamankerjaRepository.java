package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.mipro.dto.vwpengker;
import com.mipro.model.pengalamankerjaModel;

public interface pengalamankerjaRepository extends JpaRepository <pengalamankerjaModel, Long> {

	@Query ("select p from pengalamankerjaModel p where p.isDelete = false order by p.id")
	List<pengalamankerjaModel>semuapengalamankerja();
	
	@Query ("select p from pengalamankerjaModel p where p.id=?1")
	pengalamankerjaModel pengalamankerjabyid (Long ids);
	
	//is Delete
	@Modifying
	@Query("update pengalamankerjaModel set isDelete = true where id = ?1")
	int deleteflag (Long ids);
	
	//Join Table
	@Query("select new com.mipro.dto.vwpengker(p.id, p.biodataId, p.companyName, p.joinYear, p.joinMonth, p.resignYear, p.resignMonth, p.lastPosition, p.notes)"
			+ " from pengalamankerjaModel p INNER JOIN biodataModel r on p.biodataId = r.id where p.isDelete = false and p.biodataId=?1 order by p.id")
	List<vwpengker>semuapengalamankerjaJ(Long ids);
		
	@Query("select p from pengalamankerjaModel p where p.biodataId=?1")
	Long biodataid(Long ids);
	

}
