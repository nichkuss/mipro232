package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.mipro.dto.vwprojek;
import com.mipro.model.projekModel;

public interface projekRepository extends JpaRepository<projekModel, Long> {

	@Query("select new com.mipro.dto.vwprojek(p.id, p.startYear, p.startMonth, p.projectName, p.projectDuration, p.client, p.projectPosition, p.description, t.name) "
			+ "from projekModel p INNER join pengalamankerjaModel pk on p.riwayatPekerjaanId=pk.id "
			+ "INNER join timeperiodModel t on p.timePeriodId=t.id where p.isDelete=false ")
	List<vwprojek> semuaprojek();

	@Query ("select p from projekModel p where p.id=?1")
	projekModel projekbyid(Long ids);

	@Modifying
	@Query("update projekModel set isDelete = true where id = ?1")
	int deleteflag(Long ids);

	
}
