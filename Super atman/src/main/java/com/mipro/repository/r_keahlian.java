package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.mipro.dto.vwahli;
import com.mipro.model.m_keahlian;


public interface r_keahlian extends JpaRepository<m_keahlian, Long> {
	
//	@Query("select k from m_keahlian k where k.isDelete=false") //si kotaModel disingkat menjadi "k" 
//	List<m_keahlian>semuakeahlian();
	
	@Query("select k from m_keahlian k where k.Id=?1")
	m_keahlian keahlianbyid(Long ids);
	
	//isdel
	@Modifying
	@Query("update m_keahlian set isDelete = true where Id=?1")
	int deleteflag(Long ids);
	
	
	
//	join
	@Query("select new com.mipro.dto.vwahli( k.Id,k.skillName ,s.name) from m_keahlian k "
			+ "INNER join m_skillLevel s on k.skillLevelId=s.Id "
			+ "INNER join biodataModel b on b.id=k.biodataId where k.isDelete=false AND k.biodataId=?1")
	List<vwahli>semuakeahlianj(Long ids);
	
}
