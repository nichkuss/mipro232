package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


import com.mipro.model.m_skillLevel;

public interface r_skillLevel extends JpaRepository<m_skillLevel, Long> {
	@Query("select s from m_skillLevel s where s.isDelete=false")
	List<m_skillLevel> semuaskill();

}
