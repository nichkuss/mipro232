package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mipro.model.roModel;

public interface roRepository extends JpaRepository<roModel, Long> {
	
	@Query("select ro from roModel ro")
	List<roModel> semuaro();

}
