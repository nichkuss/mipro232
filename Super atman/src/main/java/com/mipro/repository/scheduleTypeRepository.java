package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mipro.model.scheduleTypeModel;

public interface scheduleTypeRepository extends JpaRepository<scheduleTypeModel, Long> {

	@Query("select s from scheduleTypeModel s where s.isDelete=false")
	List<scheduleTypeModel> semuascheduletipe();

}
