package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mipro.model.troModel;

public interface troRepository extends JpaRepository<troModel, Long> {
	
	@Query("select t from troModel t")
	List<troModel> semuatro();

}
