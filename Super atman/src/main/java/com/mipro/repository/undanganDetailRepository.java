package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mipro.model.undanganDetailModel;

public interface undanganDetailRepository extends JpaRepository<undanganDetailModel, Long> {

	@Query("select d from undanganDetailModel d ")
	List<undanganDetailModel> detail();

	//by id
	@Query("select u from undanganDetailModel u where u.id=?1")
	undanganDetailModel detailbyid(Long ids);

}
