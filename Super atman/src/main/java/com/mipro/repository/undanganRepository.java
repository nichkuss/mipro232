package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.mipro.dto.detildto;
import com.mipro.dto.vwundangan;
import com.mipro.model.undanganModel;

public interface undanganRepository extends JpaRepository<undanganModel, Long> {

	//custom
	@Query("select u from undanganModel u where u.isDelete=false")
	List<undanganModel> semuaundangan();

	//by id
	@Query("select u from undanganModel u where u.id=?1")
	undanganModel undanganbyid(Long ids);

	//isdelete
	@Modifying
	@Query("update undanganModel set isDelete = true where id=?1")
	void deleteflag(Long ids);

	//join
	@Query("select new com.mipro.dto.vwundangan( u.id, u.invitationCode, b.fullName, p.schoolName, p.major, u.invitationDate) from undanganModel u "
			+ "INNER join undanganDetailModel ud on ud.undanganId=u.id INNER join biodataModel b on ud.biodataId=b.id"
			+ " INNER join pendidikanModel p on p.biodataid=b.id  where u.isDelete=false")
	List<vwundangan> viewpelamar();
	
//	@Query("select new com.mipro.dto.vwundangan( u.id, u.invitationCode, b.fullName, p.schoolName, p.major, u.invitationDate) from undanganModel u "
//			+ "INNER join undanganDetailModel ud on ud.undanganId=u.id INNER join biodataModel b on ud.biodataId=b.id"
//			+ " INNER join pendidikanModel p on p.biodataid=b.id  where u.isDelete=false and u.id=?1")
//	vwundangan viewpelamarJ(Long ids);

	//detil
	@Query("select new com.mipro.dto.detildto(u.id, u.invitationCode, u.invitationDate, u.time, b.fullName, c.fullName as namaro, d.fullName as namatro,  t.name,  u.otherRoTro, u.location, ud.notes) from undanganModel u "
			+ "INNER join undanganDetailModel ud on ud.undanganId=u.id INNER join scheduleTypeModel t on u.scheduleTypeId=t.id "
			+ "INNER join biodataModel b on ud.biodataId=b.id  INNER join biodataModel c on u.ro=c.id INNER join biodataModel d on u.tro=d.id where u.isDelete=false and u.id=?1")
	
	detildto detiljoin(Long ids);

}
