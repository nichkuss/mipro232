package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.dto.vwnote;
import com.mipro.model.biodataModel;
import com.mipro.model.catatanModel;
import com.mipro.repository.catatanRepository;

@Service
@Transactional
public class catatanService {

	@Autowired
	private catatanRepository cr;

	// custom
	public List<catatanModel> semuacatatan() {
		return cr.semuacatatan();
	}

	// save by jpa
	public catatanModel simpancatatan(catatanModel catatanModel) {
		return cr.save(catatanModel);
	}

	//catatanbyid
	public catatanModel catatanbyid(Long ids) {
		return cr.catatanbyid(ids);
	}
	
	//biodatabyid
		public biodataModel biodatabyid(Long ids) {
			return cr.biodatabyid(ids);
		}

	// isdelete
	public void hapuscatatan(Long ids) {
		cr.deleteflag(ids);		
	}
	
	//join
	public List<vwnote>semuanotej(Long ids){
		return cr.semuanotej(ids);
	}
	
}
