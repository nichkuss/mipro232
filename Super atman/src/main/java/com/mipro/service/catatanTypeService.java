package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.model.catatanTypeModel;
import com.mipro.repository.catatanTypeRepository;

@Service
@Transactional
public class catatanTypeService {
	@Autowired
	private catatanTypeRepository cts;
	
	public List<catatanTypeModel>semuatipe(){
		return cts.semuatipe();
	}
}
