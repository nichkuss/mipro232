package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.dto.vwbio;
import com.mipro.model.biodataModel;
import com.mipro.repository.pelamarRepository;

@Service
@Transactional
public class pelamarService {

	@Autowired
	private pelamarRepository pr;

	public List<biodataModel> semuapelamar() {
		return pr.semuapelamar();
	}
	
	public List<biodataModel> semuakaryawan() {
		return pr.semuakaryawan();
	}
	
	public List<vwbio> semuapelamarj(){
		return pr.semuapelamarj();
	}
	
	public biodataModel pelamarbyid(long ids) {
		return pr.pelamarbyid(ids);
	}
	
}
