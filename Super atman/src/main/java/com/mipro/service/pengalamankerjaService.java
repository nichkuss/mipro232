package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.dto.vwpengker;
import com.mipro.model.pengalamankerjaModel;
import com.mipro.repository.pengalamankerjaRepository;

@Service
@Transactional
public class pengalamankerjaService {
	
	@Autowired
	private pengalamankerjaRepository pr;
	
	// by custom
	public List<pengalamankerjaModel>semuapengalamankerja(){
		return pr.semuapengalamankerja();
	}
	
	//by custom join
	public List<vwpengker> semuapengalamankerjaj(Long ids){
		return pr.semuapengalamankerjaJ(ids);
	}
	
	public long biodataid(Long ids) {
		return pr.biodataid(ids);
	}
	
	public pengalamankerjaModel simpanpengalamankerja (pengalamankerjaModel pengalamankerjaModel) {
		return pr.save(pengalamankerjaModel);
	}
	
	public pengalamankerjaModel pengalamankerjabyid (Long ids) {
		return pr.pengalamankerjabyid(ids);
	}
	
	public void hapuspengalamankerja(Long ids) { 
//		pr.deleteById(ids);
		pr.deleteflag(ids);
	}

}
