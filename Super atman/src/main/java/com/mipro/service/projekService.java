package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.dto.vwprojek;
import com.mipro.model.projekModel;
import com.mipro.repository.projekRepository;

@Service
@Transactional
public class projekService {

	@Autowired projekRepository pj;
	
	//join
	public List<vwprojek>semuaprojek(){
		return pj.semuaprojek();
	}
	
	//save
	public projekModel simpanprojek (projekModel projekmodel) {
		return pj.save(projekmodel);
	}
	
	//by id
	public projekModel projekbyid (Long ids) {
		return pj.projekbyid(ids);
	}
	
	//delete
	public void hapusprojek(Long ids) { 
		pj.deleteflag(ids);
	}
}
