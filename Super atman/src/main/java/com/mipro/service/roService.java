package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.model.roModel;
import com.mipro.repository.roRepository;

@Service
@Transactional
public class roService {
	@Autowired
	private roRepository rr;
	
	public List<roModel> semuaro(){
		return rr.semuaro();
	}
}
