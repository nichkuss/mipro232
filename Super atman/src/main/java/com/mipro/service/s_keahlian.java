package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.dto.vwahli;
import com.mipro.model.m_keahlian;
import com.mipro.repository.r_keahlian;

@Service
@Transactional
public class s_keahlian {
	@Autowired
	private r_keahlian k;
	
//	//by custom
//	public List<m_keahlian> semuakeahlian(){
//		return k.semuakeahlian();
//	}
	
	//save by jpa
	public m_keahlian simpankeahlian(m_keahlian keahlianModel) {
		return k.save(keahlianModel);
	}
	
	public m_keahlian keahlianbyid(Long ids) {
		return k.keahlianbyid(ids);
	}
	
	public void hapuskeahlian(Long ids) {
		k.deleteflag(ids);
	}

	public List<vwahli> semuakeahlianj(Long ids) {
		return k.semuakeahlianj(ids);
	}
	
}
