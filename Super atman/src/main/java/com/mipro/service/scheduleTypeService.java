package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.model.scheduleTypeModel;
import com.mipro.repository.scheduleTypeRepository;

@Service
@Transactional
public class scheduleTypeService {

	@Autowired
	private scheduleTypeRepository sr;
	
	public List<scheduleTypeModel> semuascheduletipe(){
		return sr.semuascheduletipe();
	}
}
