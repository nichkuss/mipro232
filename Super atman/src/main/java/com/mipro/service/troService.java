package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.model.troModel;
import com.mipro.repository.troRepository;

@Service
@Transactional
public class troService {
	@Autowired
	private troRepository rr;
	
	public List<troModel> semuatro(){
		return rr.semuatro();
	}
}
