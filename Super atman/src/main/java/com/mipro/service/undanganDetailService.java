package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.model.undanganDetailModel;

import com.mipro.repository.undanganDetailRepository;

@Service
@Transactional
public class undanganDetailService {
	@Autowired
	private undanganDetailRepository udr;

	public List<undanganDetailModel> detail() {
		return udr.detail();
	}

	// save by jpa
	public undanganDetailModel simpandetail(undanganDetailModel undanganDetailModel) {
		return udr.save(undanganDetailModel);
	}
	
	public undanganDetailModel detailbyid(Long ids) {
		return udr.detailbyid(ids);
	}

}
