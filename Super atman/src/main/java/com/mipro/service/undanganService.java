package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.dto.detildto;
import com.mipro.dto.vwundangan;
import com.mipro.model.undanganModel;
import com.mipro.repository.undanganRepository;

@Service
@Transactional
public class undanganService {
	@Autowired
	private undanganRepository u;

	// custom
	public List<undanganModel> semuaundangan() {
		return u.semuaundangan();
	}
	
	//join
	public List<vwundangan>viewpelamar(){
		return u.viewpelamar();
	}

	// save by jpa
	public undanganModel simpanundangan(undanganModel undanganModel) {
		return u.save(undanganModel);
	}

	public undanganModel undanganbyid(Long ids) {
		return u.undanganbyid(ids);
	}

	// isdelete
	public void hapusundangan(Long ids) {
		u.deleteflag(ids);
	}
	
	public detildto detiljoin(Long ids){
		return u.detiljoin(ids);
	}
	
	
}
