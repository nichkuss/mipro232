catatanlist();
function catatanlist() {
	$.ajax({
		url : '/catatanlist',
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#catatanlist').html(result);	
		}
	});
}

function addform() {
	var ids = $('#idbiodata').val();
	$.ajax({
		url : '/catatanadd/'+ids,
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#noncatatanlist').html(result);
			$('#cmodal').modal();
			$('#judul').html("Tambah Catatan")
			
		}
	});
}

function closemodal(){
	$('#cmodal').modal();
	$('.modal-backdrop').hide();
	$('.modal-backdrop').remove();
}

function refreshpage(){
	$.ajax({
		url : '/catatan',
		type : 'get',
		dataType : 'html',
		success : function(result) {
			closemodal();
			$('#fragment').html(result);
		}
	});
	return false;
}