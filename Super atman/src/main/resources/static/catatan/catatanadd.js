



var ck1;
var ck2;

function cek1(){
	var judul = document.getElementById("title").value;
	var errorMsg = "Judul Catatan tidak boleh kosong";
	var warna = errorMsg.fontcolor("#cc0000");
	if(judul==""){
		$('#eror').html(warna);
		$('#eror').show();

		document.getElementById("label").style.color = "#cc0000";
		document.getElementById("title").style.borderColor = "#cc0000";
		
		ck1=0;
	} else {
		$('#eror').hide();

		document.getElementById("label").style.color = "";
		document.getElementById("title").style.borderColor = "";
		
		ck1=1;
		
	}
}
function cek2(){
	var tipe = document.getElementById("noteTypeId").value;
	var errorMsg = "Jenis Catatan tidak boleh kosong";
	var warna = errorMsg.fontcolor("#cc0000");
	if(tipe==""){
		$('#eror2').html(warna);
		$('#eror2').show();
		
		document.getElementById("label2").style.color = "#cc0000";
		document.getElementById("noteTypeId").style.borderColor = "#cc0000";
		
		ck2=0;
	} else {
		$('#eror2').hide();
		
		document.getElementById("label2").style.color = "";
		document.getElementById("noteTypeId").style.borderColor = "";
		
		ck2=1;
		
	}
}

function btnsave(){
	cek1();
	cek2();
	if(ck1 == 1 && ck2==1){
		document.getElementById("btn_simpan").type="submit";
		$('#datanewcatatan').submit(function() {
			var rfs = $('#biodataId').val();
			formdata=$('#datanewcatatan').serialize();
				$.ajax({
				url : '/simpancatatan',
				type : 'POST',
				data : formdata,
				success : function(result) {
					
					refresh2(rfs);
					tutupmodal()
					
				}
			});
			return false;
		});
	}
	else{
		cek1();
		cek2();
	}
}

//function valid(){	
//		
//	var judul = document.getElementById("title").value;
//	var tipe = document.getElementById("noteTypeId").value;
//	var errorMsg = "Judul Catatan tidak boleh kosong";
//	var warna = errorMsg.fontcolor("#cc0000");
//	
//	if(judul == "" && tipe == ""){
//		$('#eror').html(warna);
//		$('#eror').show();
//		$('#eror2').html(warna);
//		$('#eror2').show();
//		document.getElementById("btn_simpan").disabled = true;
//		document.getElementById("label").style.color = "#cc0000";
//		document.getElementById("title").style.borderColor = "#cc0000";
//		document.getElementById("label2").style.color = "#cc0000";
//		document.getElementById("noteTypeId").style.borderColor = "#cc0000";	
//
//	}
//	else if(judul != "" && tipe == ""){
//		$('#eror').hide();
//		$('#eror2').html(warna);
//		$('#eror2').show();
//		document.getElementById("btn_simpan").disabled = true;
//		document.getElementById("label").style.color = "";
//		document.getElementById("title").style.borderColor = "#000080";
//		document.getElementById("label2").style.color = "#cc0000";
//		document.getElementById("noteTypeId").style.borderColor = "#cc0000";	
//	}
//	
//	else if(judul == "" && tipe != ""){
//		$('#eror').html(warna);
//		$('#eror').show();
//		$('#eror2').hide();
//		document.getElementById("btn_simpan").disabled = true;
//		document.getElementById("label").style.color = "#cc0000";
//		document.getElementById("title").style.borderColor = "#cc0000";
//		document.getElementById("label2").style.color = "";
//		document.getElementById("noteTypeId").style.borderColor = "#000080";	
//	}
//	
//	else{
//		$('#eror').hide();
//		$('#eror2').hide();
//		document.getElementById("btn_simpan").disabled = false;
//		document.getElementById("label").style.color = "";
//		document.getElementById("title").style.borderColor = "#000080";
//		document.getElementById("label2").style.color = "";
//		document.getElementById("noteTypeId").style.borderColor = "#000080";
//		
//		
//	}
//}
