var ck1;
var ck2;

function cek1(){
	var judul = document.getElementById("title").value;
	var errorMsg = "Judul Catatan tidak boleh kosong";
	var warna = errorMsg.fontcolor("#cc0000");
	if(judul==""){
		$('#eror').html(warna);
		$('#eror').show();

		document.getElementById("label").style.color = "#cc0000";
		document.getElementById("title").style.borderColor = "#cc0000";
		
		ck1=0;
	} else {
		$('#eror').hide();

		document.getElementById("label").style.color = "";
		document.getElementById("title").style.borderColor = "";
		
		ck1=1;
		
	}
}
function cek2(){
	var tipe = document.getElementById("noteTypeId").value;
	var errorMsg = "Jenis Catatan tidak boleh kosong";
	var warna = errorMsg.fontcolor("#cc0000");
	if(tipe==""){
		$('#eror2').html(warna);
		$('#eror2').show();
		
		document.getElementById("label2").style.color = "#cc0000";
		document.getElementById("noteTypeId").style.borderColor = "#cc0000";
		
		ck2=0;
	} else {
		$('#eror2').hide();
		
		document.getElementById("label2").style.color = "";
		document.getElementById("noteTypeId").style.borderColor = "";
		
		ck2=1;
		
	}
}

function btnsave(){
	
	if(ck1 == 1 && ck2==1){
		document.getElementById("btn_simpan").type="submit";

$('#dataeditcatatan').submit(function() {
	var rfs = $('#biodataId').val();
	formdata=$('#dataeditcatatan').serialize();
		$.ajax({
			url : '/ubahcatatan',
			type : 'POST',
			data : formdata,
	success : function(result) {
		refresh2(rfs);
		tutupmodal()
		}
	});
	return false;
});

	}
	else{
		cek1();
		cek2();
	}
}