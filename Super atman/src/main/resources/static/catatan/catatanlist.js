//tambah
function addform() {
	var ids = $('#idbiodata').val();	
	$.ajax({
		url : '/catatanadd/'+ids,
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#noncatatanlist').html(result);
			$('#cmodal').modal();
			$('#judul').html("Tambah Catatan")
			
		}
	});
}

function editcatatan(t) {
	var alamat=t.getAttribute("href");
	$.ajax({
		url : alamat,
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#noncatatanlist').html(result);
			$('#cmodal').modal();
			$('#judul').html("Edit Catatan")
		}
	});
}

function deletecatatan(t) {
	var alamat=t.getAttribute("href");
	$.ajax({
		url : alamat,
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#delcatatanlist').html(result);
			$('#delcmodal').modal();	
			$('#judul2').html("Hapus Catatan ?")
		}
	});
}




function tutupmodal(){
	  $('#cmodal').modal('hide');
	  $('#delcmodal').modal('hide');
	  $('#pelamarmodal').modal('show');
}
