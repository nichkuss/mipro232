keahlianlist();
function keahlianlist() {
	$.ajax({
		url : '/keahlianlist',
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#keahlianlist').html(result);	
		}
	});
}

function addform() {
	$.ajax({
		url : '/keahlianadd',
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#nonkeahlianlist').html(result);
			$('#kmodal').modal();
			$('#judul').html("Tambah Keahlian")
			
		}
	});
}

function closemodal(){
	$('#kmodal').modal();
	$('.modal-backdrop').hide();
	$('.modal-backdrop').remove();
}

function refreshpage(){
	$.ajax({
		url : '/keahlian',
		type : 'get',
		dataType : 'html',
		success : function(result) {
			closemodal();
			$('#fragment').html(result);
		}
	});
	return false;
}