
$('#dataeditkeahlian').submit(function() {
	var rfs = $('#biodataId').val();
	formdata=$('#dataeditkeahlian').serialize();
	$.ajax({
		url : '/ubahkeahlian',
		type : 'POST',
		data : formdata,
		success : function(result) {
			refresh1(rfs);
			tutupmodal()
		}
	});
	return false;
});

function cek1(){
	var skill = document.getElementById("skillLevelId").value;
	
	var errorMsg = "Level Keahlian tidak boleh kosong";
	var warna = errorMsg.fontcolor("#cc0000");
	
	if(skill == ""){
		$('#eror').html(warna);
		$('#eror').show();
		document.getElementById("label").style.color = "#cc0000";
		document.getElementById("skillLevelId").style.borderColor = "#cc0000";
		document.getElementById("btn_simpan").disabled = true;

	}else{
		$('#eror').hide();
		document.getElementById("label").style.color = "";
		document.getElementById("skillLevelId").style.borderColor = "#000080";
		document.getElementById("btn_simpan").disabled = false;
	}
}