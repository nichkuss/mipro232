
function editkeahlian(t) {
	var alamat=t.getAttribute("href");
	$.ajax({
		url : alamat,
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#nonkeahlianlist').html(result);
			$('#kmodal').modal();
			$('#judul').html("Edit Keahlian")
			
		}
	});
}

function deletekeahlian(t) {
	var alamat=t.getAttribute("href");
	$.ajax({
		url : alamat,
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#delkeahlianlist').html(result);
			$('#delkmodal').modal();	
			$('#judul2').html("Hapus Keahlian ?")
		}
	});
}


function addform() {
	var ids = $('#idbiodata').val();
	$.ajax({
		url : '/keahlianadd/'+ids,
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#nonkeahlianlist').html(result);
			$('#kmodal').modal();
			$('#judul').html("Tambah Keahlian")
			
		}
	});
}

function tutupmodal(){
	  $('#kmodal').modal('hide');
	  $('#delkmodal').modal('hide');
	  $('#pelamarmodal').modal('show');
}

