//function btsave(){
//	var company_name = document.getElementById("companyName").value;
//	
//	var errorMsg = "Nama perusahaan cannot be empty";
//	var warna = errorMsg.fontcolor("#cc0000");
//	
//	if(company_name == ""){
//		$('#company_name_error').html(warna);
//		$('#company_name_error').show();
//		document.getElementById("company").style.color = "#cc0000";
//		document.getElementById("companyName").style.borderColor = "#cc0000";
//		document.getElementById("btn_simpan").disabled = true;
//
//	}else{
//		$('#company_name_error').hide();
//		document.getElementById("company").style.color = "";
//		document.getElementById("companyName").style.borderColor = "";
//		document.getElementById("btn_simpan").disabled = false;
//	}
//}

//==================== Year =========================

$('#resignYear').change(function()
{
	var join_year = $('#joinYear').val();
	var resign_year = $('#resignYear').val();
	var errorMsg = "Join year cannot be greater than Resign year";
	var warna = errorMsg.fontcolor("#cc0000");
	var errorResignYear;
	
	if(join_year > resign_year)
	{
		$('#resign_year_error').html(warna);
		$('#resign_year_error').show();
		document.getElementById("keluar").style.color = "#cc0000";
		document.getElementById("resignYear").style.borderColor = "#cc0000";
		document.getElementById("btn_simpan").disabled = true;
		errorResignYear = true;
	}
	else
	{
		$('#resign_year_error').hide();
		document.getElementById("keluar").style.color = "";
		document.getElementById("resignYear").style.borderColor = "";
		document.getElementById("btn_simpan").disabled = false;
	}
});


//==================== Month =========================
$('#resignMonth').change(function(){
	
	var join_month = $('#joinMonth').val();
	var resign_month = $('#resignMonth').val();
	var join_year = $('#joinYear').val();
	var resign_year = $('#resignYear').val();
	var errorMsg = "Join Month cannot be greater than Resign Month";
	var warna = errorMsg.fontcolor("#cc0000");
	var errorResignYear;
	
	if(join_year == resign_year && join_month > resign_month){
		
		$('#resign_month_error').html(warna);
		$('#resign_month_error').show();
		document.getElementById("keluar").style.color = "#cc0000";
		document.getElementById("resignMonth").style.borderColor = "#cc0000";
		document.getElementById("btn_simpan").disabled = true;
		errorResignYear = true;	
	}else{
		$('#resign_month_error').hide();
		document.getElementById("keluar").style.color = "";
		document.getElementById("resignMonth").style.borderColor = "";
		document.getElementById("btn_simpan").disabled = false;
	}
	
});

//=========================================================================

function btsave(){
	var company_name = document.getElementById("companyName").value;
	var join_month = document.getElementById("joinMonth").value;
	var join_year = document.getElementById("joinYear").value;
	var last_position = document.getElementById("lastPosition").value;
	
	var errorMsg1 = "Nama perusahaan cannot be empty";
	var warna1 = errorMsg1.fontcolor("#cc0000");
	var errorMsg2 = "Join Month cannot be empty";
	var warna2 = errorMsg2.fontcolor("#cc0000");
	var errorMsg3 = "Join Year cannot be empty";
	var warna3 = errorMsg3.fontcolor("#cc0000");
	var errorMsg4 = "Posisi Terakhir cannot be empty";
	var warna4 = errorMsg4.fontcolor("#cc0000");
	
	if (join_month == "" && join_year == ""){
		document.getElementById("resignMonth").disabled=false;
		document.getElementById("resignYear").disabled=false;
	}else if (join_month != "" && join_year != ""){
		document.getElementById("resignMonth").disabled=false;
		document.getElementById("resignYear").disabled=false;
	}
	
	if(company_name == ""){
		$('#company_name_error').html(warna1);
		$('#company_name_error').show();
		document.getElementById("company").style.color = "#cc0000";
		document.getElementById("companyName").style.borderColor = "#cc0000";
		document.getElementById("btn_simpan").disabled = true;
	}else if (company_name != ""){
		$('#company_name_error').hide();
		document.getElementById("company").style.color = "";
		document.getElementById("companyName").style.borderColor = "";
		document.getElementById("btn_simpan").disabled = false;
		if(join_month == ""){
			$('#join_month_error').html(warna2);
			$('#join_month_error').show();
			document.getElementById("masuk").style.color = "#cc0000";
			document.getElementById("joinMonth").style.borderColor = "#cc0000";
			document.getElementById("btn_simpan").disabled = true;
		}else if(join_month != ""){
			$('#join_month_error').hide();
			document.getElementById("masuk").style.color = "";
			document.getElementById("joinMonth").style.borderColor = "";
			document.getElementById("btn_simpan").disabled = false;
			if(join_year == ""){
				$('#join_year_error').html(warna3);
				$('#join_year_error').show();
				document.getElementById("masuk").style.color = "#cc0000";
				document.getElementById("joinYear").style.borderColor = "#cc0000";
				document.getElementById("btn_simpan").disabled = true;
			}else if(join_year != ""){
				$('#join_year_error').hide();
				document.getElementById("masuk").style.color = "";
				document.getElementById("joinYear").style.borderColor = "";
				document.getElementById("btn_simpan").disabled = false;
				if(last_position == ""){
					$('#last_position_error').html(warna4);
					$('#last_position_error').show();
					document.getElementById("last").style.color = "#cc0000";
					document.getElementById("lastPosition").style.borderColor = "#cc0000";
					document.getElementById("btn_simpan").disabled = true;
				}else if(last_position != ""){
					$('#last_position_error').hide();
					document.getElementById("last").style.color = "";
					document.getElementById("lastPosition").style.borderColor = "";
					document.getElementById("btn_simpan").disabled = false;
				}
			}
		}
	}
}



//function selectfunction1(){
//	var join_month = document.getElementById("joinMonth").value;
//	var join_year = document.getElementById("joinYear").value;
//	
//	var errorMsg = "Join Month cannot be empty";
//	var warna = errorMsg.fontcolor("#cc0000");
//	
//	if(join_month == ""){
//		$('#join_month_error').html(warna);
//		$('#join_month_error').show();
//		document.getElementById("masuk").style.color = "#cc0000";
//		document.getElementById("joinMonth").style.borderColor = "#cc0000";
//		document.getElementById("btn_simpan").disabled = true;
//	}
//
//	else if (join_month == "" && join_year == ""){
//		document.getElementById("resignMonth").disabled=true;
//		document.getElementById("resignYear").disabled=true;
//	}else{
//		$('#join_month_error').hide();
//		document.getElementById("masuk").style.color = "";
//		document.getElementById("joinMonth").style.borderColor = "";
//		document.getElementById("btn_simpan").disabled = false;
//		document.getElementById("resignMonth").disabled=false;
//		document.getElementById("resignMonth").value="";
//		document.getElementById("resignYear").disabled=false;
//		document.getElementById("resignYear").value="";
//	}
//}
//
//function selectfunction2(){
//	var join_month = document.getElementById("joinMonth").value;
//	var join_year = document.getElementById("joinYear").value;
//	
//	var errorMsg = "Join Year cannot be empty";
//	var warna = errorMsg.fontcolor("#cc0000");
//	
//	
//	if(join_year == ""){
//		$('#join_year_error').html(warna);
//		$('#join_year_error').show();
//		document.getElementById("masuk").style.color = "#cc0000";
//		document.getElementById("joinYear").style.borderColor = "#cc0000";
//		document.getElementById("btn_simpan").disabled = true;
//	}
//
//	else if (join_month == "" && join_year == ""){
//		document.getElementById("resignMonth").disabled=true;
//		document.getElementById("resignYear").disabled=true;
//	}else{
//		$('#join_year_error').hide();
//		document.getElementById("masuk").style.color = "";
//		document.getElementById("joinYear").style.borderColor = "";
//		document.getElementById("btn_simpan").disabled = false;
//		document.getElementById("resignMonth").disabled=false;
//		document.getElementById("resignMonth").value="";
//		document.getElementById("resignYear").disabled=false;
//		document.getElementById("resignYear").value="";
//	}
//}
//
//function cek1(){
//	var last_position = document.getElementById("lastPosition").value;
//	
//	var errorMsg = "Posisi Terakhir cannot be empty";
//	var warna = errorMsg.fontcolor("#cc0000");
//	var val1;
//	
//	if(last_position == ""){
//		$('#last_position_error').html(warna);
//		$('#last_position_error').show();
//		document.getElementById("last").style.color = "#cc0000";
//		document.getElementById("lastPosition").style.borderColor = "#cc0000";
//		document.getElementById("btn_simpan").disabled = true;
//		val1 = true;
//	}else{
//		$('#last_position_error').hide();
//		document.getElementById("last").style.color = "";
//		document.getElementById("lastPosition").style.borderColor = "";
//		document.getElementById("btn_simpan").disabled = false;
//	}
//}

//==================== Batal =========================

function tutupmodal2(){
	  $('#pengalamankerjaModal').modal('hide');
	  $('#delpengalamankerjaModal').modal('hide');
}

//==================== Simpan =========================

		$('#dataeditpengalamankerja').submit(function() {
			formdata = $('#dataeditpengalamankerja').serialize();
			var rfsh = $('#biodataId').val();
				$.ajax({
						url : '/ubahpengalamankerja',
						type : 'POST',
						data : formdata,
						success : function(result) {
							refresh2(rfsh);
							alert("Ubah Pengalaman Kerja Berhasil")
							tutupmodal();
						}
				});
				return false; // agar tidak pindah halaman
	});
		

		



