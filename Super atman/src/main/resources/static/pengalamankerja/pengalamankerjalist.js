function addform() {
	var ids = $('#idbiodata').val();
	$.ajax({
		url : '/pengalamankerjaadd/'+ ids,
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#pengalamankerjaModal').modal();
			$('#nonpengalamankerjalist').html(result);
			
		}
	
	});
}

function tutupmodal(){
	  $('#pengalamankerjaModal').modal('hide');
	  $('#delpengalamankerjaModal').modal('hide');
}

function editpengalamankerja(t) {
	var alamat = t.getAttribute("href");
	$.ajax({
		url : alamat,
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#nonpengalamankerjalist').html(result);
			$('#pengalamankerjaModal').modal();
			$('#judul').html("Edit Pengalaman Kerja")	

		}

	});
}
function deletepengalamankerja(t) {
	var alamat = t.getAttribute("href");
	$.ajax({
		url : alamat,
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#delpengalamankerjalist').html(result);
			$('#delpengalamankerjaModal').modal();
			$('#judul2').html("Hapus Pengalaman Kerja ?")
			

		}

	});
}


//---------------------------------------------------------------------//


//proyek

function tambahproject(t) {
	var alamat = t.getAttribute("href");
	$.ajax({
		url : alamat,
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#nonpengalamankerjalist').html(result);
			$('#pengalamankerjaModal').modal();
			$('#judul').html("Tambah Riwayat Projek")
			

		}

	});
}

//projeklist();
//function projeklist() {
//	$.ajax({
//		url : '/projeklist',
//		type : 'get',
//		dataType : 'html',
//		success : function(result) {
//			$('#projeklist').html(result);	
//		}
//	});
//}



isitbpengkerja();
function isitbpengkerja() {
	
	
	$.ajax({
		url : '/pengalamankerjalistdata/1',
		type : 'get',
		dataType : 'json',
		success : function(result) {
			var str = "";
			 $.each(result, function (i, item) {
					//bikin tabel
					
					str += "<tr class='trnumber' style='border-top-color: navy; border-top-style: solid; border-bottom-color: navy; border-bottom-style: solid;'> " +
							"<td>"+item.companyName + " ("+ item.joinYear+"-"+item.joinMonth+ " s/d "+ item.resignYear+"-"+item.resignMonth+") "+"</td>"
							"</tr>";
					
					//coba
					
					
					
					str += "<td>" + item.notes+ "</td>";
					   
					str += "<td>"+
							"<div class='dropdown'>"+
								"<button style='background-color:navy' class='btn btn-secondary dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'> "+
								"<b style='color:white;'>"+"more"+"</b>"
								
							"</div>"+ 
							"</td>";
					   
					
		        });
			//taruh tabel ke div 			 
			    
				$("#tbpenglamankerja").empty();
				$("#tbpenglamankerja").append(str);
		
		}
	});

	

}

function isitestdetail() {
	
	//bikin tabel
	var str = "";
	str += "<tr class='trnumber' style='border-top-color: navy; border-top-style: solid; border-bottom-color: navy; border-bottom-style: solid;'> " +
			"<td class='number'></td>" +
			"<td style='text-align: center'>"+v2+"</td>" +
			"<td style='text-align: center'>" +
			"<button type='button' onclick='deletetestdetail(this)' style='width:30px;height:30px; border: white; background-color: white'><img alt='del' src='images/waste-bin.png' style='width:25px;height: 25px'></button></td>" +
			"<td name='testTypeId' class='tdId'>"+v4+"</td> </tr>";
	//taruh tabel ke div 
//	$("#tesdetail").empty();
		$("#tesdetail").append(str);
		$("td.number").each(function(i,v) {
			$(v).text(i + 1);
		});
		$(".tdId").hide();

}

function editprojek(t) {
	var alamat = t.getAttribute("href");
	$.ajax({
		url : alamat,
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#nonpengalamankerjalist').html(result);
			$('#pengalamankerjaModal').modal();
			$('#judul').html("Edit Riwayat Proyek")	

		}

	});
}

function deleteprojek(t) {
	var alamat = t.getAttribute("href");
	$.ajax({
		url : alamat,
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#delpengalamankerjalist').html(result);
			$('#delpengalamankerjaModal').modal();
			$('#judul2').html("Hapus Riwayat Proyek")

		}

	});
}
