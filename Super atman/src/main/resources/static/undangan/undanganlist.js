function detailundangan (t) {
	var alamat=t.getAttribute("href");
	$.ajax({
		url : alamat,
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#nonundanganlist').html(result);
			$('#kmodal').modal();
			$('#judul').html("UD00000")
		}
	});
}

function editundangan(t) {
	var alamat=t.getAttribute("href");
	$.ajax({
		url : alamat,
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#nonundanganlist').html(result);
			$('#kmodal').modal();
			$('#judul').html("Edit undangan")
		}
	});
}

function deleteundangan(t) {
	var alamat=t.getAttribute("href");
	$.ajax({
		url : alamat,
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#delundanganlist').html(result);
			$('#delkmodal').modal();	
			$('#judul2').html("Hapus undangan ?")
		}
	});
}


//paging kk

oTable = $('#tbundangan').DataTable({
	"lengthMenu": [[-1, 1, 2, 3], ["All", 1, 2, 3]]
});   //pay attention to capital D, which is mandatory to retrieve "api" datatables' object, as @Lionel said

//$("#btncari").click(function() {
//	$('#tableSearch').val();
//	$('#tbundangan').DataTable().search().draw();
//})

$('#tableSearch').keyup(function(){
      oTable.search($(this).val()).draw() ;
})

//$('#btncari').click(function(){
//      oTable.search($("#tableSearch").val()).draw() ;
//})

$("#pgsize li").click(function() {
	var table = $('#tbundangan').DataTable();
	table.page.len( $(this).text() ).draw();
});

$("#asc").click(function() {
	var table = $('#tbundangan').DataTable();
	table.order( [ 1, 'asc' ]).draw();
});

$("#desc").click(function() {
	var table = $('#tbundangan').DataTable();
	table.order( [ 1, 'desc' ] ).draw();
});

$("#maju").click(function() {
	var table = $('#tbundangan').DataTable();
	table.page( 'next' ).draw( 'page' );
})

$("#mundur").click(function() {
	var table = $('#tbundangan').DataTable();
	table.page( 'previous' ).draw( 'page' );
})

$("#clear").click(function() {
	$('#tbundangan').DataTable().search('').draw();
	$('#tableSearch').val('');
});