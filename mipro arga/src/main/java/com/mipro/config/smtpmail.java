package com.mipro.config;


import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;


@Component
public class smtpmail {
	
	@Autowired
	JavaMailSender javaMailSender;
	
	public void send(String to, String subject, String body) throws MailException {

		SimpleMailMessage message = new SimpleMailMessage();
        message.setSubject(subject);
        message.setTo(to);
        message.setText(body);

        javaMailSender.send(message);
    }
	
	public void sendTs(String to, String subject, String body) throws MailException {
		
		SimpleMailMessage message = new SimpleMailMessage();

//        MimeMessage message = javaMailSender.createMimeMessage();
//        MimeMessageHelper helper;
//        helper = new MimeMessageHelper(message, true);
		message.setSubject(subject);
		message.setTo(to);
		message.setText(body);

        javaMailSender.send(message);
    }
	
	public void sendTsCc(String to, String subject, String body, String cc) throws MailException {
		
		SimpleMailMessage message = new SimpleMailMessage();

//        MimeMessage message = javaMailSender.createMimeMessage();
//        MimeMessageHelper helper;
//        helper = new MimeMessageHelper(message, true);
		
		message.setSubject(subject);
		message.setTo(to);
		message.setCc(cc);
		message.setText(body);

        javaMailSender.send(message);
    }
	

}
