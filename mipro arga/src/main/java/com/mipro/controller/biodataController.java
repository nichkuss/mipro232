package com.mipro.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class biodataController {
	
	@RequestMapping("biodata")
	public String biodata() {
		
		return"biodata/biodata";
	}

}
