package com.mipro.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class organisasiController {
	
	@RequestMapping("organisasi")
	public String organisasi() {
		
		return"organisasi/organisasi";
	}

}
