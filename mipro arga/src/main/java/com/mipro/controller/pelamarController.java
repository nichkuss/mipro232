package com.mipro.controller;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mipro.config.smtpmail;
import com.mipro.dto.vwpelamar;
import com.mipro.model.pelamarModel;
import com.mipro.service.pelamarService;

@Controller
public class pelamarController {
	
	@Autowired
	private pelamarService ps;
	
	@Autowired
	smtpmail smtpMailSender;
	

    

	@RequestMapping("pelamar")
	public String pelamar() {
		return "pelamar/pelamar";
	}
	
	@RequestMapping("pelamarlist")
	public String pelamarlist(Model model) {
//		List<pelamarModel>semuapelamar=ps.semuapelamar();
//		model.addAttribute("semuapelamar", semuapelamar);
		
		List<vwpelamar>semuapelamarj = ps.semuapelamarj();
		model.addAttribute("semuapelamar", semuapelamarj);
		
		return "pelamar/pelamarlist";
	}


	@RequestMapping("pelamardetail/{ids}")
	public String pelamardetail(Model model, @PathVariable(name="ids")long ids) {
		pelamarModel pelamarbyid=ps.pelamarbyid(ids);
		model.addAttribute("pelamarbyid", pelamarbyid);
		return"pelamar/pelamardetail";
	}
	
	@RequestMapping("kirimtoken/{ids}")
	public String kirimtoken(Model model, @PathVariable(name="ids")Long ids) {
		pelamarModel pelamarbyid=ps.pelamarbyid(ids);
		model.addAttribute("pelamarbyid", pelamarbyid);
		model.addAttribute("pelamarbyid", ids);
		
		return "pelamar/kirimtoken";
	}
	
	@ResponseBody
	@RequestMapping("simpantoken")
	public String simpantoken(Model model, @ModelAttribute()pelamarModel pelamarModel) {
		Long user = (long)20;

		pelamarModel pelamarbyid = ps.pelamarbyid(pelamarModel.getId());
		String alamat_tujuan = pelamarbyid.getEmail();
		String c[] = pelamarModel.getExpiredToken().toString().split(" ");
		
		String bulan="";
		switch(c[1]) {

	    case "Jan": bulan = "01" ; break;
	    case "Feb": bulan = "02" ; break;
	    case "Mar": bulan = "03" ; break;
	    case "Apr": bulan = "04" ; break;
	    case "May": bulan = "05" ; break;
	    case "Jun": bulan = "06" ; break;
	    case "Jul": bulan = "07" ; break;
	    case "Aug": bulan = "08" ; break;
	    case "Sep": bulan = "09" ; break;
	    case "Oct": bulan = "10" ; break;
	    case "Nov": bulan = "11" ; break;
	    case "Dec": bulan = "12" ; break;
	    }

		    String tgl = c[5]+"-"+bulan+"-"+c[2];
		    
		    
		
		pelamarModel.setEmail(pelamarbyid.getEmail());
		pelamarModel.setFullName(pelamarbyid.getFullName());
		pelamarModel.setNickName(pelamarbyid.getNickName());
		pelamarModel.setDob(pelamarbyid.getDob());
		pelamarModel.setGender(pelamarbyid.getGender());
		pelamarModel.setPhoneNumber1(pelamarbyid.getPhoneNumber1());
		pelamarModel.setAddrbookId(pelamarbyid.getAddrbookId());
		
		//set create
		pelamarModel.setCreatedBy(pelamarbyid.getCreatedBy());
		pelamarModel.setCreatedOn(pelamarbyid.getCreatedOn());
		
		//buat token
		 String uuid = UUID.randomUUID().toString().replace("-", "");
			uuid=uuid.substring(0,10);
			pelamarModel.setToken(uuid);
		
		//update by
		pelamarModel.setModifiedBy(new Long(user));
		//update on
		pelamarModel.setModifiedOn(new Date());
		
		
		  try {
			smtpMailSender.send(alamat_tujuan,"Token Xsis","token : " + uuid + "\nTanggal : " + tgl);
			ps.simpantoken(pelamarModel);
			
		
		} catch (MailException e) {
			String kirim = "kirim";
			model.addAttribute("tanda", kirim);
			// TODO Auto-generated catch block
//			e.printStackTrace();
			System.out.println(e);
			
		}
		  return "pelamar/pelamarlist";  
	}
	
//	@ResponseBody
//	@RequestMapping("generateui")
//	public String generateui() {
//		
//	    String uuid = UUID.randomUUID().toString().replace("-", "");
//		uuid=uuid.substring(0,10);
//		Long idst=1l;
//		Date et = new Date();
//		ps.kirimflag(et, idst);
//		return uuid;
//	}
//	@ResponseBody
//	@RequestMapping("kirim")
//		public String kirim()throws ParseException, MessagingException {
//	        smtpMailSender.send("rizky.putra@xsis.co.id","test","isinyaaa");
//		return "";
//	}
	
	
}

