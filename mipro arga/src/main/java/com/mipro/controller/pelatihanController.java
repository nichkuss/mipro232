package com.mipro.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mipro.dto.vwpelatihan;
import com.mipro.model.pelatihanModel;
import com.mipro.model.timeperiodModel;
import com.mipro.service.pelamarService;
import com.mipro.service.pelatihanService;
import com.mipro.service.timeperiodService;

@Controller
public class pelatihanController {
	
	@Autowired
	private pelatihanService ps;
	
	@Autowired
	private timeperiodService ts;
	
	@Autowired
	private pelamarService pel;
	
	@RequestMapping("pelatihan")
	public String pelatihan() {
		
		return"pelatihan/pelatihan";
	}
	
	@RequestMapping("pelatihanlist/{ids}")
	public String pelatihanlist(Model model, @PathVariable(name="ids")Long ids) {
		List<vwpelatihan>semuapelatihan = ps.semuapelatihanj(ids);
		model.addAttribute("pelatihanbyid", ids);
		model.addAttribute("semuapelatihan", semuapelatihan);
		return "pelatihan/pelatihanlist";
	}
	
	@RequestMapping("pelatihanadd/{ids}")
	public String pelatihanadd(Model model, @PathVariable(name="ids")Long ids) {
		List<timeperiodModel>timeperiod = ts.semuatimeperiod();
		isitahun(model);
		model.addAttribute("timeperiod", timeperiod);
		model.addAttribute("ids", ids);
		return "pelatihan/pelatihanadd";
	}
	
	@ResponseBody
	@RequestMapping("simpanpelatihan")
	public String simpanpelatihan(@ModelAttribute() pelatihanModel pelatihanModel) {
		//create by
		Long user = (long) 10;
		pelatihanModel.setCreatedBy(new Long(user));
		//create on
		pelatihanModel.setCreatedOn(new Date());
		ps.simpanpelatihan(pelatihanModel);
		return "";
	}
	
	@ResponseBody
	@RequestMapping("ubahpelatihan")
	public String ubahpelatihan(@ModelAttribute()pelatihanModel pelatihanModel) {
		Long user = (long)20;
		//set create
		pelatihanModel pelatihanbyid = ps.pelatihanbyid(pelatihanModel.getId());
		pelatihanModel.setBiodataId(pelatihanbyid.getBiodataId());
		pelatihanModel.setCreatedBy(pelatihanbyid.getCreatedBy());
		pelatihanModel.setCreatedOn(pelatihanbyid.getCreatedOn());
		
		//update by
		pelatihanModel.setModifiedBy(new Long(20));
		//update on
		pelatihanModel.setModifiedOn(new Date());
		ps.simpanpelatihan(pelatihanModel);
		return "";
	}
	
	@ResponseBody
	@RequestMapping("hapuspelatihan/{ids}")
	public String hapuspelatihan(@PathVariable(name = "ids")Long ids, @ModelAttribute() pelatihanModel pelatihanModel) {
		pelatihanModel pelatihanbyid = ps.pelatihanbyid(ids);
		//delete by
		Long user = (long)30;
		pelatihanbyid.setDeletedBy(new Long(user));
		//delete on
		pelatihanbyid.setDeletedOn(new Date());
		ps.simpanpelatihan(pelatihanbyid);
		ps.hapuspelatihan(ids);
		return "";
	}
	
	@RequestMapping("pelatihanedit/{ids}")
	public String pelatihanedit (Model model, @PathVariable(name = "ids") Long ids) {
		pelatihanModel pelatihanbyid = ps.pelatihanbyid(ids);
		model.addAttribute("pelatihanbyid", pelatihanbyid);
		
		List<timeperiodModel>timeperiod = ts.semuatimeperiod();
		model.addAttribute("timeperiod", timeperiod);
		isitahun(model);
		
		return "pelatihan/pelatihanedit";
	}
	
	@RequestMapping("pelatihandelete/{ids}")
	public String pelatihandelete(Model model, @PathVariable(name = "ids") Long ids) {
		pelatihanModel pelatihanbyid = ps.pelatihanbyid(ids);
		model.addAttribute("pelatihanbyid", pelatihanbyid);
		return "pelatihan/pelatihandelete";

	}
	
	public void isitahun(Model model) {
		List<Integer>tahun= new ArrayList();
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		for (int i = 0; i <= 30; i++) {
			tahun.add(year - i);
		}
		model.addAttribute("tahun", tahun);
	}

}
