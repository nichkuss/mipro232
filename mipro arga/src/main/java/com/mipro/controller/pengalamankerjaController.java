package com.mipro.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mipro.dto.vwpengker;
import com.mipro.model.pengalamankerjaModel;
import com.mipro.service.pengalamankerjaService;

@Controller
public class pengalamankerjaController {
	
	@Autowired
	private pengalamankerjaService ps;
	
	@RequestMapping("/pengalamankerja")
	public String pengalamankerja() {
		
		return"pengalamankerja/pengalamankerja";
	}
	
	@RequestMapping("pengalamankerjalist/{ids}")
	public String pengalamankerjalist(Model model, @PathVariable(name="ids")Long ids) {
		List<vwpengker>semuapengalamankerja = ps.semuapengalamankerjaj(ids);
		model.addAttribute("pengalamankerjabyid", ids);
		model.addAttribute("semuapengalamankerja", semuapengalamankerja);
		return "pengalamankerja/pengalamankerjalist";
	}
	
	@RequestMapping("pengalamankerjaadd/{ids}")  
	public String pengalamankerjaadd(Model model, @PathVariable(name="ids")Long ids) {
		isitahun(model);
		model.addAttribute("ids", ids);
		return "pengalamankerja/pengalamankerjaadd";
	}
	
	@ResponseBody
	@RequestMapping("simpanpengalamankerja")
	public String simpanpengalamankerja(@ModelAttribute() pengalamankerjaModel pengalamankerjaModel) {
		//System.out.println(pengalamankerjaModel.getIsItRelated());
		if (pengalamankerjaModel.getIsItRelated() == null) {
			pengalamankerjaModel.setIsItRelated(false);
		}else {
			pengalamankerjaModel.setIsItRelated(true);
		}
		//create by
		Long user = (long)123;
		pengalamankerjaModel.setCreatedBy(new Long (user));
		//create on
		pengalamankerjaModel.setCreatedOn(new Date());
		ps.simpanpengalamankerja(pengalamankerjaModel);
		return "";
	}
	
	@ResponseBody
	@RequestMapping("ubahpengalamankerja")
	public String ubahpengalamankerja(@ModelAttribute()pengalamankerjaModel pengalamankerjaModel) {
		//set create
		pengalamankerjaModel pengalamankerjabyid = ps.pengalamankerjabyid(pengalamankerjaModel.getId());
		pengalamankerjaModel.setBiodataId(pengalamankerjabyid.getBiodataId());
		pengalamankerjaModel.setCreatedBy(pengalamankerjabyid.getCreatedBy());
		pengalamankerjaModel.setCreatedOn(pengalamankerjabyid.getCreatedOn());
		//update by
		Long user = (long)32;
		pengalamankerjaModel.setModifiedBy(new Long (20));
		//update on
		pengalamankerjaModel.setModifiedOn(new Date());
		ps.simpanpengalamankerja(pengalamankerjaModel);
		return "";
	}
	
	@ResponseBody
	@RequestMapping("hapuspengalamankerja/{ids}")
	public String hapuspengalamankerja(@PathVariable(name = "ids")long ids) {
		pengalamankerjaModel pengalamankerjabyid = ps.pengalamankerjabyid(ids);
		//delete by
		long user = 4;
		pengalamankerjabyid.setDeletedBy(user);
		//delete on
		pengalamankerjabyid.setDeletedOn(new Date());
		ps.hapuspengalamankerja(ids);
		return "";
	}
	
	@RequestMapping("pengalamankerjaedit/{ids}")
	public String pengalamankerjaedit (Model model, 
			@PathVariable(name = "ids") Long ids) {
		pengalamankerjaModel pengalamankerjabyid = ps.pengalamankerjabyid(ids);
		model.addAttribute("pengalamankerjabyid", pengalamankerjabyid);
		
//		List<pengalamankerjaModel>timeperiod = ts.semuatimeperiod();
//		model.addAttribute("timeperiod", timeperiod);
		isitahun(model);
		
		return "pengalamankerja/pengalamankerjaedit";
	}
	
	@RequestMapping("pengalamankerjadelete/{ids}")
	public String pengalamankerjadelete(Model model, @PathVariable(name = "ids") long ids) {
		pengalamankerjaModel pengalamankerjabyid = ps.pengalamankerjabyid(ids);
		model.addAttribute("pengalamankerjabyid", pengalamankerjabyid);
		return "pengalamankerja/pengalamankerjadelete";

	}
	
	public void isitahun(Model model) {
		List<Integer>tahun= new ArrayList();
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		for (int i = 0; i <= 30; i++) {
			tahun.add(year - i);
		}
		model.addAttribute("tahun", tahun);
	}

}
