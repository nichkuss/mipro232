package com.mipro.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mipro.dto.vwatc;
import com.mipro.dto.vwkeahlian;
import com.mipro.dto.vwpelatihan;
import com.mipro.dto.vwpendidikanprofil;
import com.mipro.dto.vwpengker;
import com.mipro.dto.vwsertifikasiprofil;
import com.mipro.model.pelamarModel;
import com.mipro.service.attachmentService;
import com.mipro.service.keahlianService;
import com.mipro.service.pelamarService;
import com.mipro.service.pelatihanService;
import com.mipro.service.pendidikanService;
import com.mipro.service.pengalamankerjaService;
import com.mipro.service.sertifikasiService;

@Controller
public class profilController {
	
	@Autowired
	private sertifikasiService ss;
	
	@Autowired
	private pelatihanService ps;
	
	@Autowired
	private pendidikanService pend;
	
	@Autowired
	private pengalamankerjaService pker;
	
	@Autowired
	private keahlianService ks;
	
	@Autowired
	private attachmentService as;
	
	
	@RequestMapping("profil")
	public String profil() {
		
		return"profil/profil";
	}
	
	@RequestMapping("profillist/{ids}")
	public String profillist(Model model, @PathVariable(name="ids")Long ids) {
		List<vwsertifikasiprofil>semuasertifikasi = ss.semuasertifikasij(ids);
		model.addAttribute("sertifikasibyid", ids);
		model.addAttribute("semuasertifikasi", semuasertifikasi);
		
		List<vwpelatihan>semuapelatihan = ps.semuapelatihanj(ids);
		model.addAttribute("pelatihanbyid", ids);
		model.addAttribute("semuapelatihan", semuapelatihan);
		
		
		List<vwpendidikanprofil>semuapendidikan = pend.semuapendidikanj(ids);
		String thnlhr="";
		Date date1 = null;
		Date date2 = new Date();
		for (vwpendidikanprofil s:semuapendidikan) {
			System.out.println("thn db :"+s.getDob());
			thnlhr=s.getDob().toString();
		}
		try {
			date1=new SimpleDateFormat("yyyy-MM-dd").parse(thnlhr);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("thn lhr :"+ date1);
		System.out.println("thn now :"+ date2);
		
		Calendar cal=Calendar.getInstance();
	    cal.setTime(date1);
		
		Calendar birthDay = cal;
		Calendar today = new GregorianCalendar(); 
		today.setTime(new Date()); 
		int yearsInBetween = today.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR); 
		int monthsDiff = today.get(Calendar.MONTH) - birthDay.get(Calendar.MONTH);
		long ageInMonths = yearsInBetween*12 + monthsDiff; 
		long age = yearsInBetween;
		long usia = ageInMonths / 12;
		System.out.println("Tahun : " + yearsInBetween);
		System.out.println("Bulan : " + monthsDiff);
		System.out.println("Usia : " + usia);
		System.out.println("Number of months since James gosling born : " + ageInMonths); 
		System.out.println("Sir James Gosling's age : " + age);

	
		
		model.addAttribute("umur", usia);
		model.addAttribute("pendidikanbyid", ids);
		model.addAttribute("semuapendidikan", semuapendidikan);
		
		
		List<vwpengker>semuapengalamankerja = pker.semuapengalamankerjaj(ids);
		model.addAttribute("pengalamankerjabyid", ids);
		model.addAttribute("semuapengalamankerja", semuapengalamankerja);
		
		List<vwkeahlian>tingkatlanjut = ks.tingkatlanjut(ids);
		List<vwkeahlian>menengah = ks.menengah(ids);
		List<vwkeahlian>pemula = ks.pemula(ids);
		model.addAttribute("keahlianbyid", ids);
		model.addAttribute("tingkatlanjut", tingkatlanjut);
		model.addAttribute("menengah", menengah);
		model.addAttribute("pemula", pemula);
		
		List<vwatc>fotoprofil = as.foto2(ids);
		model.addAttribute("fotobyid", ids);
		model.addAttribute("fotoprofil", fotoprofil);
		
		
		return"profil/profillist";
	}
	

}
