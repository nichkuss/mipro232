package com.mipro.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mipro.service.timesheetService;

@Controller
@RequestMapping(path="/resapi", produces="application/json")
@CrossOrigin(origins = "*")
public class resController {
	
	@Autowired
	private timesheetService ts;
	
	@GetMapping("/timesheetlist/{id}/{yr}/{mnt}")
	public ResponseEntity<?> timesheetlist(
			@PathVariable(name="id")Long id,
			@PathVariable(name="yr")Integer yr,
			@PathVariable(name="mnt")Integer mnt) {
		return new ResponseEntity<>(ts.tsjoin(id, yr, mnt), HttpStatus.OK);
	}
	
	
	

}
