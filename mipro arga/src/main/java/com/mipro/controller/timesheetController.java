package com.mipro.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mipro.config.smtpmail;
import com.mipro.dto.vwpendidikanprofil;
import com.mipro.dto.vwtimesheet;
import com.mipro.model.pelamarModel;
import com.mipro.service.timesheetService;

@Controller
public class timesheetController {
	
	@Autowired
	private timesheetService tr;
	
	@Autowired
	smtpmail smtpMailSender;
	
	
	@RequestMapping("timesheet")
	public String timesheet(Model model) {
		isitahun(model);
		
		return"timesheet/timesheet";
	}
	
	//===Join===
	@RequestMapping("timesheetlist/{id}/{yr}/{mnt}")
	public String timesheetlist(Model model,
								@PathVariable(name="id")Long id,
								@PathVariable(name="yr")Integer yr,
								@PathVariable(name="mnt")Integer mnt) {
		List<vwtimesheet>semuatimesheet=tr.tsjoin(id, yr, mnt);
		model.addAttribute("semuatimesheet", semuatimesheet);
		return "timesheet/timesheetlist";
	}
	
	//===Kirim===
	@RequestMapping("kirimtimesheet")
	public String kirimtimesheet() {
		return"timesheet/kirimtimesheet";
	}
	
	//===Update & Send Email Tanpa Cc===
	@ResponseBody
	@RequestMapping("simpantimesheet/{id}/{yr}/{mnt}")
	public String simpantimesheet(Model model,
								@PathVariable(name="id")Long id,
								@PathVariable(name="yr")Integer yr,
								@PathVariable(name="mnt")Integer mnt) {
		List<vwtimesheet>semuatimesheet=tr.tsjoin(id, yr, mnt);
		vwtimesheet krm = tr.tsjoinem(id, yr, mnt);
		String email = krm.getUserEmail();
		
		String tgl="";
		
		for (vwtimesheet s:semuatimesheet) {
			System.out.println("thn db :"+s.getTimesheetDate());
			tgl=s.getTimesheetDate().toString();
		}
		String c[]= tgl.split("-");
		
		String bulan="";
		switch(c[1]) {

	    case "01": bulan = "Jan" ; break;
	    case "02": bulan = "Feb" ; break;
	    case "03": bulan = "Mar" ; break;
	    case "04": bulan = "Apr" ; break;
	    case "05": bulan = "Mei" ; break;
	    case "06": bulan = "Jun" ; break;
	    case "07": bulan = "Jul" ; break;
	    case "08": bulan = "Aug" ; break;
	    case "09": bulan = "Sep" ; break;
	    case "10": bulan = "Okt" ; break;
	    case "11": bulan = "Nov" ; break;
	    case "12": bulan = "Des" ; break;
	    }
		
		String Tanggal = c[0]+"-"+bulan+"-"+c[2];
		System.out.println("tgl jadi : " + Tanggal);
		

		
		
		 try {
			smtpMailSender.sendTs(email,"Status Timesheet","Timesheet tanggal : " + Tanggal + " sudah di submit" );
//			String tanda = "Hallo";
//			model.addAttribute("tanda", tanda);
			tr.submit(id, mnt , yr);
			
		} catch (MailException e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			System.out.println(e);
	
		}
		return "timesheet/timesheet";
	}
	
	//===Update & Send Email Dengan Cc===
	@ResponseBody
	@RequestMapping("simpantimesheetCc/{id}/{yr}/{mnt}/{cc}")
	public String simpantimesheetCc(
									@PathVariable(name="id")Long id,
									@PathVariable(name="yr")Integer yr,
									@PathVariable(name="mnt")Integer mnt,
									@PathVariable(name="cc")String cc) {
		vwtimesheet krm = tr.tsjoinem(id, yr, mnt);
		String email = krm.getUserEmail();
		

			
		 try {
			smtpMailSender.sendTsCc(email,"Status Timesheet","Status berubah",cc);
			tr.submit(id, mnt , yr);
		} catch (MailException e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			System.out.println(e);
		}
		return "";
	}
	
	
	public void isitahun(Model model) {
		List<Integer>tahun= new ArrayList();
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		for (int i = 0; i <= 10; i++) {
			tahun.add(year - i);
		}
		model.addAttribute("tahun", tahun);
	}

}
