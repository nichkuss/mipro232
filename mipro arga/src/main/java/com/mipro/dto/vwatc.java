package com.mipro.dto;

public class vwatc {
	
	private Long id;
	private Long biodataId;
	private String fileName;
	private String filePath;
	private Boolean isPhoto;
	public vwatc(Long id, Long biodataId, String fileName, String filePath, Boolean isPhoto) {
		super();
		this.id = id;
		this.biodataId = biodataId;
		this.fileName = fileName;
		this.filePath = filePath;
		this.isPhoto = isPhoto;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getBiodataId() {
		return biodataId;
	}
	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public Boolean getIsPhoto() {
		return isPhoto;
	}
	public void setIsPhoto(Boolean isPhoto) {
		this.isPhoto = isPhoto;
	}
	
	

}
