package com.mipro.dto;

public class vwkeahlian {
	
	private Long id;
	private Long biodataId;
	private Long skillLevelId;
	private String skillName;
	public vwkeahlian(Long id, Long biodataId, Long skillLevelId, String skillName) {
		super();
		this.id = id;
		this.biodataId = biodataId;
		this.skillLevelId = skillLevelId;
		this.skillName = skillName;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getBiodataId() {
		return biodataId;
	}
	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}
	public Long getSkillLevelId() {
		return skillLevelId;
	}
	public void setSkillLevelId(Long skillLevelId) {
		this.skillLevelId = skillLevelId;
	}
	public String getSkillName() {
		return skillName;
	}
	public void setSkillName(String skillName) {
		this.skillName = skillName;
	}
	
}
