package com.mipro.dto;

public class vwpelatihan {
	
	private Long id;
	private Long biodataId;
	private String trainingName;
	private String organizer;
	private String trainingYear;
	private String trainingMonth;
	private int trainingDuration;
	private String name;
	public vwpelatihan(Long id, Long biodataId, String trainingName, String organizer, String trainingYear,
			String trainingMonth, int trainingDuration, String name) {
		super();
		this.id = id;
		this.biodataId = biodataId;
		this.trainingName = trainingName;
		this.organizer = organizer;
		this.trainingYear = trainingYear;
		this.trainingMonth = trainingMonth;
		this.trainingDuration = trainingDuration;
		this.name = name;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getBiodataId() {
		return biodataId;
	}
	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}
	public String getTrainingName() {
		return trainingName;
	}
	public void setTrainingName(String trainingName) {
		this.trainingName = trainingName;
	}
	public String getOrganizer() {
		return organizer;
	}
	public void setOrganizer(String organizer) {
		this.organizer = organizer;
	}
	public String getTrainingYear() {
		return trainingYear;
	}
	public void setTrainingYear(String trainingYear) {
		this.trainingYear = trainingYear;
	}
	public String getTrainingMonth() {
		return trainingMonth;
	}
	public void setTrainingMonth(String trainingMonth) {
		this.trainingMonth = trainingMonth;
	}
	public int getTrainingDuration() {
		return trainingDuration;
	}
	public void setTrainingDuration(int trainingDuration) {
		this.trainingDuration = trainingDuration;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	
	
	
	
}
