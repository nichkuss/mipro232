package com.mipro.dto;

import java.util.Date;

public class vwpendidikanprofil {
	
	private Long id;
	private Long biodataId;
	private String schoolName;
	private String country;
	private String entryYear;
	private String graduationYear;
	private String major;
	private String gpa;
	private String fullName;
	private Boolean gender;
	private Date dob;
	public vwpendidikanprofil(Long id, Long biodataId, String schoolName, String country, String entryYear,
			String graduationYear, String major, String gpa, String fullName, Boolean gender, Date dob) {
		super();
		this.id = id;
		this.biodataId = biodataId;
		this.schoolName = schoolName;
		this.country = country;
		this.entryYear = entryYear;
		this.graduationYear = graduationYear;
		this.major = major;
		this.gpa = gpa;
		this.fullName = fullName;
		this.gender = gender;
		this.dob = dob;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getBiodataId() {
		return biodataId;
	}
	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getEntryYear() {
		return entryYear;
	}
	public void setEntryYear(String entryYear) {
		this.entryYear = entryYear;
	}
	public String getGraduationYear() {
		return graduationYear;
	}
	public void setGraduationYear(String graduationYear) {
		this.graduationYear = graduationYear;
	}
	public String getMajor() {
		return major;
	}
	public void setMajor(String major) {
		this.major = major;
	}
	public String getGpa() {
		return gpa;
	}
	public void setGpa(String gpa) {
		this.gpa = gpa;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public Boolean getGender() {
		return gender;
	}
	public void setGender(Boolean gender) {
		this.gender = gender;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	
	
	

}
