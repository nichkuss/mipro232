package com.mipro.dto;

public class vwpengker {
	
	private Long id;
	private Long biodataId;
	private String companyName;
	private String joinYear;
	private String joinMonth;
	private String resignYear;
	private String resignMonth;
	private String lastPosition;
	private String notes;
	public vwpengker(Long id, Long biodataId, String companyName, String joinYear, String joinMonth, String resignYear,
			String resignMonth, String lastPosition, String notes) {
		super();
		this.id = id;
		this.biodataId = biodataId;
		this.companyName = companyName;
		this.joinYear = joinYear;
		this.joinMonth = joinMonth;
		this.resignYear = resignYear;
		this.resignMonth = resignMonth;
		this.lastPosition = lastPosition;
		this.notes = notes;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getBiodataId() {
		return biodataId;
	}
	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getJoinYear() {
		return joinYear;
	}
	public void setJoinYear(String joinYear) {
		this.joinYear = joinYear;
	}
	public String getJoinMonth() {
		return joinMonth;
	}
	public void setJoinMonth(String joinMonth) {
		this.joinMonth = joinMonth;
	}
	public String getResignYear() {
		return resignYear;
	}
	public void setResignYear(String resignYear) {
		this.resignYear = resignYear;
	}
	public String getResignMonth() {
		return resignMonth;
	}
	public void setResignMonth(String resignMonth) {
		this.resignMonth = resignMonth;
	}
	public String getLastPosition() {
		return lastPosition;
	}
	public void setLastPosition(String lastPosition) {
		this.lastPosition = lastPosition;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	
	

}
