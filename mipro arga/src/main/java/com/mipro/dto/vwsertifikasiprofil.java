package com.mipro.dto;

public class vwsertifikasiprofil {
	
	private Long id;
	private Long biodataId;
	private String certificateName;
	private String publisher;
	private String validStartYear;
	private String untilYear;
	
	public vwsertifikasiprofil(Long id, Long biodataId, String certificateName, String publisher, String validStartYear,
			String untilYear) {
		super();
		this.id = id;
		this.biodataId = biodataId;
		this.certificateName = certificateName;
		this.publisher = publisher;
		this.validStartYear = validStartYear;
		this.untilYear = untilYear;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getBiodataId() {
		return biodataId;
	}

	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}

	public String getCertificateName() {
		return certificateName;
	}

	public void setCertificateName(String certificateName) {
		this.certificateName = certificateName;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getValidStartYear() {
		return validStartYear;
	}

	public void setValidStartYear(String validStartYear) {
		this.validStartYear = validStartYear;
	}

	public String getUntilYear() {
		return untilYear;
	}

	public void setUntilYear(String untilYear) {
		this.untilYear = untilYear;
	}
	
	

}
