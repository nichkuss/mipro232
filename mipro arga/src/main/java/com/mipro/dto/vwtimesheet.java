package com.mipro.dto;

import java.util.Date;

public class vwtimesheet {
	
	private String status;
	private Date timesheetDate;
	private String name;
	private String userEmail;
	public vwtimesheet(String status, Date timesheetDate, String name) {
		super();
		this.status = status;
		this.timesheetDate = timesheetDate;
		this.name = name;
	}
	
	public vwtimesheet(String userEmail) {
		super();
		this.userEmail = userEmail;
	}

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getTimesheetDate() {
		return timesheetDate;
	}
	public void setTimesheetDate(Date timesheetDate) {
		this.timesheetDate = timesheetDate;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	
	
	
	

}
