package com.mipro.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "x_biodata_attachment")
public class biodataattachmentModel extends CommonEntity{
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, length = 11)
    private Long Id;
	
	@Column(name = "biodata_id",length=11, nullable=false)
	private Long biodataId;
	
	@Column(name = "file_name",length=100)
	private String fileName;
	
	@Column(name = "file_path",length=1000)
	private String filePath;
	
	@Column(name = "notes",length=1000)
	private String notes;
	
	@Column(name = "is_photo")
	private Boolean isPhoto;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public Long getBiodataId() {
		return biodataId;
	}

	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Boolean getIsPhoto() {
		return isPhoto;
	}

	public void setIsPhoto(Boolean isPhoto) {
		this.isPhoto = isPhoto;
	}
	
	

}
