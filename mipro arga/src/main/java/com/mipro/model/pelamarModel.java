package com.mipro.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Id;

@Entity
@Table(name = "x_biodata")
public class pelamarModel extends CommonEntity  {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, length = 11)
    private Long Id;

    @Column(name = "fullname", nullable = false, length = 255)
    private String fullName;

    @Column(name = "nick_name", nullable = false, length = 100)
    private String nickName;
    
    @Column(name = "dob", nullable = false)
    private Date dob;

    @Column(name = "email", nullable = false, length = 100)
    private String email;

    @Column(name = "phone_number1", nullable = false, length = 50)
    private String phoneNumber1;

    @Column(name = "token", length = 10)
    private String token;
    
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "expired_token")
    private Date expiredToken;
    
    @Column(name = "gender", nullable = false)
    private Boolean gender;
    
    @Column(name = "addrbook_id")
    private Long addrbookId;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber1() {
		return phoneNumber1;
	}

	public void setPhoneNumber1(String phoneNumber1) {
		this.phoneNumber1 = phoneNumber1;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Date getExpiredToken() {
		return expiredToken;
	}

	public void setExpiredToken(Date expiredToken) {
		this.expiredToken = expiredToken;
	}

	public Boolean getGender() {
		return gender;
	}

	public void setGender(Boolean gender) {
		this.gender = gender;
	}

	public Long getAddrbookId() {
		return addrbookId;
	}

	public void setAddrbookId(Long addrbookId) {
		this.addrbookId = addrbookId;
	}

    
	

}
