package com.mipro.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "x_riwayat_pelatihan")
public class pelatihanModel extends CommonEntity{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column (name = "id", length = 11)
	private Long id;
	
	@Column(name = "biodata_id",length=11, nullable=false)
	private Long biodataId;
	
	@Column(name = "training_name", length=100, nullable=true)
	private String trainingName;
	
	@Column(name = "organizer", length=50, nullable=true)
	private String organizer;
	
	@Column(name = "training_Year", length=10, nullable=true)
	private String trainingYear;
	
	@Column(name = "training_month", length=10, nullable=true)
	private String trainingMonth;
	
	@Column(name = "training_duration", nullable=true)
	private int trainingDuration;
	
	@Column(name = "time_period_id", length=11, nullable=true)
	private Long timePeriodId;
	
	@Column(name = "city", length=50, nullable=true)
	private String city;
	
	@Column(name = "country", length=50, nullable=true)
	private String country;
	
	@Column(name = "notes", length=1000, nullable=true)
	private String notes;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getBiodataId() {
		return biodataId;
	}

	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}

	public String getTrainingName() {
		return trainingName;
	}

	public void setTrainingName(String trainingName) {
		this.trainingName = trainingName;
	}

	public String getOrganizer() {
		return organizer;
	}

	public void setOrganizer(String organizer) {
		this.organizer = organizer;
	}

	public String getTrainingYear() {
		return trainingYear;
	}

	public void setTrainingYear(String trainingYear) {
		this.trainingYear = trainingYear;
	}

	public String getTrainingMonth() {
		return trainingMonth;
	}

	public void setTrainingMonth(String trainingMonth) {
		this.trainingMonth = trainingMonth;
	}

	public int getTrainingDuration() {
		return trainingDuration;
	}

	public void setTrainingDuration(int trainingDuration) {
		this.trainingDuration = trainingDuration;
	}

	public Long getTimePeriodId() {
		return timePeriodId;
	}

	public void setTimePeriodId(Long timePeriodId) {
		this.timePeriodId = timePeriodId;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	

	
	
	
	

}
