package com.mipro.model;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "x_placement")
public class placementModel extends CommonEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", length=11, nullable=false)
	private Long id;
	
	@Column(name = "client_id", length=11, nullable=false)
	private Long clientId;
	
	@Column(name = "employee_id", length=11, nullable=false)
	private Long employeeId;
	
	@ManyToOne
	@JoinColumn(name="employee_id", insertable=false, updatable=false)
	private employeeModel employeeModel;
	

	@Column(name = "is_placement_active", nullable=false)
	private Boolean isPlacementActive;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getClientId() {
		return clientId;
	}

	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}

	public Long getEmployeeId() {
		return employeeId;
	}
	
	public employeeModel getEmployeeModel() {
		return employeeModel;
	}

	public void setEmployeeModel(employeeModel employeeModel) {
		this.employeeModel = employeeModel;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public Boolean getIsPlacementActive() {
		return isPlacementActive;
	}

	public void setIsPlacementActive(Boolean isPlacementActive) {
		this.isPlacementActive = isPlacementActive;
	}
	
	
	
	

}
