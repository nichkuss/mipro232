package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mipro.dto.vwatc;
import com.mipro.model.biodataattachmentModel;

public interface attachmentRepository extends JpaRepository <biodataattachmentModel, Long> {
	
	@Query("select b from biodataattachmentModel b")
	List<biodataattachmentModel>semuaatc();
	
	@Query("select b from biodataattachmentModel b where b.id=?1")
	biodataattachmentModel atcbyid(Long ids);
	
	@Query("select new com.mipro.dto.vwatc(b.id, b.biodataId, b.fileName, b.filePath, b.isPhoto)"
			+ "from biodataattachmentModel b "
			+ "INNER JOIN pelamarModel c on b.biodataId=c.id where b.isDelete=false and b.biodataId=?1 and b.isPhoto=true")
	List<vwatc>foto(Long ids);

}
