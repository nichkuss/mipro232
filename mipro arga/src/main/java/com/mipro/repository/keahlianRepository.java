package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mipro.dto.vwkeahlian;
import com.mipro.model.keahlianModel;

public interface keahlianRepository extends JpaRepository<keahlianModel, Long> {
	
	@Query("select k from keahlianModel k")
	List<keahlianModel>semuakeahlian();
	
	@Query("select k from keahlianModel k where k.id=?1")
	keahlianModel keahlianbyid(Long ids);
	
	//Join
	@Query("select new com.mipro.dto.vwkeahlian(k.id, k.biodataId, k.skillLevelId, k.skillName) "
			+ "from keahlianModel k "
			+ "INNER JOIN pelamarModel p on k.biodataId=p.id where k.isDelete = false and k.skillLevelId = 3 and k.biodataId=?1")
	List<vwkeahlian>tingkatlanjut(Long ids);
	
	@Query("select new com.mipro.dto.vwkeahlian(k.id, k.biodataId, k.skillLevelId, k.skillName)"
			+ "from keahlianModel k "
			+ "INNER JOIN pelamarModel p on k.biodataId=p.id where k.isDelete = false and k.skillLevelId = 2 and k.biodataId=?1")
	List<vwkeahlian>menengah(Long ids);
	
	@Query("select new com.mipro.dto.vwkeahlian(k.id, k.biodataId, k.skillLevelId, k.skillName)"
			+ "from keahlianModel k "
			+ "INNER JOIN pelamarModel p on k.biodataId=p.id where k.isDelete = false and k.skillLevelId = 1 and k.biodataId=?1")
	List<vwkeahlian>pemula(Long ids);

}
