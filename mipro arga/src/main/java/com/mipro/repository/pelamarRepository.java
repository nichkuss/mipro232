package com.mipro.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.mipro.dto.vwpelamar;
import com.mipro.model.pelamarModel;

public interface pelamarRepository extends JpaRepository<pelamarModel, Long> {
	
	@Query("select p from pelamarModel p order by p.id ") 
	List<pelamarModel>semuapelamar();

	@Query("select r from pelamarModel r where r.id=?1")  // k  terakhir = singakatan
	pelamarModel pelamarbyid(Long ids);
	
	@Modifying
	@Query("update pelamarModel set expiredToken =?1 where Id=?2")
	int kirimflag(Date et, Long idst);
	
	//Join
	@Query("select new com.mipro.dto.vwpelamar(p.id, p.fullName, p.nickName, p.email, p.phoneNumber1, s.schoolName, p.gender)"
			+ "from pelamarModel p "
			+ "INNER JOIN pendidikanModel s on s.biodataId = p.id WHERE p.isDelete = false")
	List<vwpelamar>semuapelamarJ();

	


}
