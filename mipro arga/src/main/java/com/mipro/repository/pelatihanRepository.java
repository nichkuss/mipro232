package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.mipro.dto.vwpelatihan;
import com.mipro.model.pelatihanModel;

public interface pelatihanRepository extends  JpaRepository <pelatihanModel, Long> {
	
	@Query ("select p from pelatihanModel p where p.isDelete = false order by p.id")
	List<pelatihanModel>semuapelatihan();
	
	@Query ("select p from pelatihanModel p where p.id=?1")
	pelatihanModel pelatihanbyid (Long ids);
	
	//is Delete
	@Modifying
	@Query("update pelatihanModel set isDelete = true where id = ?1")
	int deleteflag (Long ids);
	
//	//Join Table
//	@Query("select new com.mipro.dto.vwpelatihan(p.id, p.biodataId, p.trainingName, p.organizer, p.trainingYear, p.trainingMonth, p.trainingDuration, t.name)"
//			+ " from pelatihanModel p INNER JOIN timeperiodModel t on p.timePeriodId = t.id"
//			+ " INNER JOIN pelamarModel r on p.biodataId = r.id where p.isDelete = false and p.biodataId=?1 order by p.id")
//	List<vwpelatihan>semuapelatihanJ(Long ids);
// select p.biodata_id , string_agg(p.training_name,', ') as trn,string_agg( p.organizer,', ') as org

	
	//Join Table
	@Query("select new com.mipro.dto.vwpelatihan(p.id, p.biodataId, p.trainingName, p.organizer, p.trainingYear, p.trainingMonth, p.trainingDuration, t.name)"
			+ " from pelatihanModel p INNER JOIN timeperiodModel t on p.timePeriodId = t.id"
			+ " INNER JOIN pelamarModel r on p.biodataId = r.id where p.isDelete = false and p.biodataId=?1 order by p.id")
	List<vwpelatihan>semuapelatihanJ(Long ids);
	
	@Query("select p from pelatihanModel p where p.biodataId=?1")
	Long biodataid(Long ids);


}
