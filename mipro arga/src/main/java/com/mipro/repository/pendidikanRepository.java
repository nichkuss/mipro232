package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mipro.dto.vwpendidikanprofil;
import com.mipro.model.pendidikanModel;

public interface pendidikanRepository extends JpaRepository<pendidikanModel, Long> {
	
	@Query("select p from pendidikanModel p order by p.id ") 
	List<pendidikanModel>semuapendidikan();

	@Query("select r from pendidikanModel r where r.id=?1")  // k  terakhir = singakatan
	pendidikanModel pendidikanbyid(long ids);
	
	// Join Table
	@Query("select new com.mipro.dto.vwpendidikanprofil(p.id, p.biodataId, p.schoolName, p.country, p.entryYear, p.graduationYear, p.major, p.gpa, "
			+ "r.fullName, r.gender, r.dob) "
			+ "from pendidikanModel p "
			+ "INNER JOIN pelamarModel r on p.biodataId=r.id where p.isDelete = false and p.biodataId=?1")
	List<vwpendidikanprofil>semuapendidikanprofilJ(Long ids);
	
	@Query("select p from pendidikanModel p where p.biodataId=?1")
	Long biodataId(Long ids);

}
