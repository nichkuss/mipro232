package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mipro.dto.vwsertifikasiprofil;
import com.mipro.model.sertifikasiModel;

public interface sertifikasiRepository extends JpaRepository<sertifikasiModel, Long> {
	
	@Query("select s from sertifikasiModel s order by s.id ") 
	List<sertifikasiModel>semuasertifikasi();

	@Query("select t from sertifikasiModel t where t.id=?1")  // k  terakhir = singakatan
	sertifikasiModel sertifikasibyid(Long ids);
	
	//Join Table
	@Query("SELECT new com.mipro.dto.vwsertifikasiprofil(s.id, s.biodataId, s.certificateName, s.publisher, s.validStartYear, s.untilYear)"
			+ "FROM sertifikasiModel s "
			+ "INNER JOIN pelamarModel p ON s.biodataId=p.id WHERE s.isDelete = false and s.biodataId=?1")
	List <vwsertifikasiprofil>semuasertifikasiprofilJ(Long ids);
	
	@Query("select s from sertifikasiModel s where s.biodataId=?1")
	Long biodataId(Long ids);

}
