package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.mipro.model.timeperiodModel;

public interface timeperiodRepository extends  JpaRepository <timeperiodModel, Long> {
	
	@Query ("select t from timeperiodModel t where t.isDelete = false order by t.id")
	List<timeperiodModel>semuatimeperiod();
	
	@Query ("select t from timeperiodModel t where t.id=?1")
	timeperiodModel timeperiodbyid (Long ids);
	
	@Modifying
	@Query("update timeperiodModel set isDelete = true where id = ?1")
	int deleteflag (Long ids);

}
