package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.mipro.dto.vwtimesheet;
import com.mipro.model.timesheetModel;

public interface timesheetRepository extends  JpaRepository <timesheetModel, Long> {
	
	@Query("select new com.mipro.dto.vwtimesheet(t.status, t.timesheetDate, c.name)"
			+ "from timesheetModel t "
			+ "INNER JOIN placementModel p on t.placementId=p.id "
			+ "INNER JOIN clientModel c on p.clientId=c.id "
			+ "INNER JOIN employeeModel e on p.employeeId=e.id "
			+ "INNER JOIN pelamarModel f on e.biodataId=f.id "
			+ "INNER JOIN addrbookModel a on f.addrbookId=a.id "
			+ "where t.isDelete=false and a.id=?1 and t.userApproval = null and "
			+ "(EXTRACT(year from t.timesheetDate)=?2 AND EXTRACT(MONTH from t.timesheetDate)=?3)")
	List<vwtimesheet>tsjoin(long id, int year,int month);
	
	@Query("select distinct new com.mipro.dto.vwtimesheet(c.userEmail)"
			+ "from timesheetModel t "
			+ "INNER JOIN placementModel p on t.placementId=p.id "
			+ "INNER JOIN clientModel c on p.clientId=c.id "
			+ "INNER JOIN employeeModel e on p.employeeId=e.id "
			+ "INNER JOIN pelamarModel f on e.biodataId=f.id "
			+ "INNER JOIN addrbookModel a on f.addrbookId=a.id "
			+ "where t.isDelete=false and a.id=?1 and t.userApproval = null and "
			+ "(EXTRACT(year from t.timesheetDate)=?2 AND EXTRACT(MONTH from t.timesheetDate)=?3)")
	vwtimesheet tsjoinem(long id, int year,int month);
	
//	@Query("select a from timesheetModel a where a.isDelete=false "
//			+ "and (EXTRACT(year from a.timesheetDate)=?1 AND EXTRACT(MONTH from a.timesheetDate)=?2) ")
//	List<timesheetModel>semuatimesheetassesment(int year,int month);
	
	@Modifying
	@Query(value = "UPDATE x_timesheet t "
			+ " SET modified_by=1, "
			+ " modified_on=now(), "
			+ " user_approval='Submitted', "
			+ " submitted_on=now()	"
			+ " FROM x_placement p "	
					+ " JOIN x_client c ON c.id = p.client_id "
					+ " JOIN x_employee e ON e.id = p.employee_id"
					+ " JOIN x_biodata b ON e.biodata_id = b.id "
					+ " JOIN x_addrbook a ON a.id = b.addrbook_id "
			+ " WHERE p.id = t.placement_id AND t.is_delete = false AND p.id=?1"
			+ " AND (extract(month FROM t.timesheet_date) = ?2 "
			+ " AND extract(year FROM t.timesheet_date) = ?3) "
			+ " AND t.user_approval IS NULL AND t.ero_status IS NULL ", 
		    nativeQuery = true)
	int submit(Long id, Integer Bulan, Integer tahun);
	
	@Query(value = "select DISTINCT c.name "
			+ "from x_timesheet t "
			+ "INNER JOIN x_placement p on t.placement_id=p.id "
			+ "INNER JOIN x_client c on p.client_id=c.id "
			+ "INNER JOIN x_employee e on p.employee_id=e.id "
			+ "INNER JOIN x_biodata f on e.biodata_id=f.id "
			+ "INNER JOIN x_addrbook a on f.addrbook_id=a.id "
			+ "where t.is_delete=false and p.id=?1 and t.user_approval = null and "
			+ "(EXTRACT(year from t.timesheet_date)=?2 AND EXTRACT(MONTH from t.timesheet_date)=?3)", nativeQuery = true)
	vwtimesheet tsdisc(long id, int year,int month);

}
