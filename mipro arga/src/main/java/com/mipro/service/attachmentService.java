package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.dto.vwatc;
import com.mipro.model.biodataattachmentModel;
import com.mipro.repository.attachmentRepository;

@Service
@Transactional
public class attachmentService {
	
	@Autowired
	private attachmentRepository ar;
	
	//by custom join
	public List<vwatc>foto2(Long ids){
		return ar.foto(ids);
	}
	
	public biodataattachmentModel atcbyid (Long ids) {
		return ar.atcbyid(ids);
	}

}
