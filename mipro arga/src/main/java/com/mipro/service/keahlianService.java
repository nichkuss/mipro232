package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.dto.vwkeahlian;
import com.mipro.model.keahlianModel;
import com.mipro.repository.keahlianRepository;

@Service
@Transactional
public class keahlianService {
	
	@Autowired
	private keahlianRepository kr;
	
	//by custom
	public List<keahlianModel>semuakeahlian(){
		return kr.semuakeahlian();
	}
	
	//by custom join
	public List<vwkeahlian>tingkatlanjut(Long ids){
		return kr.tingkatlanjut(ids);
	}
	public List<vwkeahlian>menengah(Long ids){
		return kr.menengah(ids);
	}
	public List<vwkeahlian>pemula(Long ids){
		return kr.pemula(ids);
	}
	
	public keahlianModel keahlianbyid (Long ids) {
		return kr.keahlianbyid(ids);
	}

}
