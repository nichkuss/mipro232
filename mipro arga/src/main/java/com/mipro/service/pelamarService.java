package com.mipro.service;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.dto.vwpelamar;
import com.mipro.model.pelamarModel;
import com.mipro.repository.pelamarRepository;

@Service
@Transactional
public class pelamarService {
	
	@Autowired
	private pelamarRepository pr;

	public List<pelamarModel> semuapelamar() {
		return pr.semuapelamar();
	}
	
	public pelamarModel pelamarbyid(Long ids) {
		return pr.pelamarbyid(ids);
	}
	
	public pelamarModel simpantoken (pelamarModel pelamarModel) {
		return pr.save(pelamarModel);
	}
	
	public void kirimflag(Date et, Long idst) {
		pr.kirimflag(et, idst);
	}
	
	//Custom Join
	public List<vwpelamar>semuapelamarj(){
		return pr.semuapelamarJ();
	}


}
