package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.dto.vwpelatihan;
import com.mipro.model.pelatihanModel;
import com.mipro.repository.pelatihanRepository;


@Service
@Transactional
public class pelatihanService {
	
	@Autowired
	private pelatihanRepository pr;
	
	// by custom
	public List<pelatihanModel>semuaPelatihan(){
		return pr.semuapelatihan();
	}
	
	//by custom join
	public List<vwpelatihan> semuapelatihanj(Long ids){
		return pr.semuapelatihanJ(ids);
	}
	
	public long biodataid(Long ids) {
		return pr.biodataid(ids);
	}

	
	public pelatihanModel simpanpelatihan (pelatihanModel pelatihanModel) {
		return pr.save(pelatihanModel);
	}
	
	public pelatihanModel pelatihanbyid (Long ids) {
		return pr.pelatihanbyid(ids);
	}
	
	public void hapuspelatihan(Long ids) {
//		pr.deleteById(ids);
		pr.deleteflag(ids);
	}

}
