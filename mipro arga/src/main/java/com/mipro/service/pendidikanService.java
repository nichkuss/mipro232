package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.dto.vwpendidikanprofil;
import com.mipro.model.pendidikanModel;
import com.mipro.repository.pendidikanRepository;

@Service
@Transactional
public class pendidikanService {
	
	@Autowired
	private pendidikanRepository pr;
	//by default
	public List<pendidikanModel>semuapendidikan(){
		return pr.semuapendidikan();
	}
	//Custom Join
	public List<vwpendidikanprofil>semuapendidikanj(Long ids){
		return pr.semuapendidikanprofilJ(ids);
	}
	
	public long biodataid(Long ids) {
		return pr.biodataId(ids);
	}
	
	public pendidikanModel pendidikanbyid(long ids) {
		return pr.pendidikanbyid(ids);
	}

}
