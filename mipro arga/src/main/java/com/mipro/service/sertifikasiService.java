package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.dto.vwsertifikasiprofil;
import com.mipro.model.sertifikasiModel;
import com.mipro.repository.sertifikasiRepository;

@Service
@Transactional
public class sertifikasiService {
	
	@Autowired
	private sertifikasiRepository sr;
	// by default
	public List<sertifikasiModel>semuasertifikasi(){
		return sr.semuasertifikasi();
	}
	
	//Custom Join
	public List<vwsertifikasiprofil>semuasertifikasij(Long ids){
		return sr.semuasertifikasiprofilJ(ids);
	}
	
	public long biodataid(Long ids) {
		return sr.biodataId(ids);
	}
	
	public sertifikasiModel sertifikasibyid(Long ids) {
		return sr.sertifikasibyid(ids);
	}

}
