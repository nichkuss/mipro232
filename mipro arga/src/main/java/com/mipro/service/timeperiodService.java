package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.model.timeperiodModel;
import com.mipro.repository.timeperiodRepository;

@Service
@Transactional
public class timeperiodService {
	
	@Autowired
	private timeperiodRepository tr;
	
	public List<timeperiodModel>semuatimeperiod(){
		return tr.semuatimeperiod();
	}
	
	public timeperiodModel simpantimeperiod (timeperiodModel timeperiodModel) {
		return tr.save(timeperiodModel);
	}
	
	public timeperiodModel timeperiodbyid (Long ids) {
		return tr.timeperiodbyid(ids);
	}
	
	public void hapustimeperiod(Long ids) {
//		tr.deleteById(ids);
		tr.deleteflag(ids);
	}

}
