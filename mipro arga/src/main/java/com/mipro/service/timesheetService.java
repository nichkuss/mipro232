package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.dto.vwtimesheet;
import com.mipro.model.timesheetModel;
import com.mipro.repository.timesheetRepository;

@Service
@Transactional
public class timesheetService {
	
	@Autowired
	private timesheetRepository tr;
	
	public List<vwtimesheet>tsjoin(long id,int year,int month){
		return tr.tsjoin(id, year, month);
	}
	
//	public List<timesheetModel>semuatimesheetassesment(int year,int month){
//		return tr.semuatimesheetassesment(year, month);
//	}
	
	public int submit(Long id, Integer Bulan, Integer tahun) {
		return tr.submit(id, Bulan, tahun);
	}
	
	public vwtimesheet tsdisc(long id, int year,int month){
		return tr.tsdisc(id, year, month);
	}
	
	public vwtimesheet tsjoinem(long id, int year,int month) {
		return tr.tsjoinem(id, year, month);
	}

	

}
