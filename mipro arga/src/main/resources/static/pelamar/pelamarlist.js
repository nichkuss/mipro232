function detpelamar(t) {
	var alamat = t.getAttribute("href");
	$.ajax({
		url : alamat,
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#pelamarmodal').modal({backdrop: 'static', keyboard: false}) 
			$('#pelamarisi').html(result);
			$('#judul').html("Detil Pelamar")
		}
	});
}

function krmform(t) {
	var alamat = t.getAttribute("href");
	$.ajax({
		url : alamat,
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#nonkirimtokenlist').html(result);
			$('#kirimtokenModal').modal();
			$('#judul2').html("Kirim Token")
			
			
		}
	
	});
}
