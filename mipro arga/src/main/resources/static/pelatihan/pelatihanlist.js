function addform() {
	var ids = $('#idbiodata').val();
	$.ajax({
		url : '/pelatihanadd/'+ ids,
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#pelatihanModal').modal();
			$('#nonpelatihanlist').html(result);
			$('#judulpeladd').html("Tambah Pelatihan");
			
		}
	
	});
}

function tutupmodal(){
	  $('#pelatihanModal').modal('hide');
	  $('#pelatihaneditModal').modal('hide');
	  $('#delpelatihanModal').modal('hide');
	  $('#pelamarModal').modal('show');
}


function editpelatihan(t) {
	var alamat = t.getAttribute("href");
	$.ajax({
		url : alamat,
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#pelatihaneditModal').modal();
			$('#nonpelatihaneditlist').html(result);
			$('#judulpeledit').html("Edit Pelatihan");
		}

	});
}
function deletepelatihan(t) {
	var alamat = t.getAttribute("href");
	$.ajax({
		url : alamat,
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#delpelatihanlist').html(result);
			$('#delpelatihanModal').modal();
			$('#judul2').html("Hapus Pelatihan ?")
			

		}

	});
}