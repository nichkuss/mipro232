function addform() {
	var ids = $('#idbiodata').val();
	$.ajax({
		url : '/pengalamankerjaadd/'+ ids,
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#pengalamankerjaModal').modal();
			$('#nonpengalamankerjalist').html(result);
			$('#judulpengadd').html("Tambah Pengalaman Kerja");
			
		}
	
	});
}

function tutupmodal(){
	  $('#pengalamankerjaModal').modal('hide');
	  $('#pengalamankerjaeditModal').modal('hide');
	  $('#delpengalamankerjaModal').modal('hide');
}

function editpengalamankerja(t) {
	var alamat = t.getAttribute("href");
	$.ajax({
		url : alamat,
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#nonpengalamankerjaeditlist').html(result);
			$('#pengalamankerjaeditModal').modal();
			$('#judulpengedit').html("Edit Pengalaman Kerja");
		}

	});
}
function deletepengalamankerja(t) {
	var alamat = t.getAttribute("href");
	$.ajax({
		url : alamat,
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#delpengalamankerjalist').html(result);
			$('#delpengalamankerjaModal').modal();
			$('#judul2').html("Hapus Pengalaman Kerja ?")
			

		}

	});
}