package com.mipro.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mipro.dto.bid;
import com.mipro.dto.jkeluarga;
import com.mipro.dto.keluarga;
import com.mipro.model.educationlevelModel;
import com.mipro.model.familyModel;
import com.mipro.model.familyrelationModel;
import com.mipro.model.familytreetypeModel;
import com.mipro.service.educationlevelService;
import com.mipro.service.familyService;
import com.mipro.service.familyrelationService;
import com.mipro.service.familytreetypeService;


@Controller
public class familyController {

	@Autowired
	private familyService fs;
	@Autowired
	private familyrelationService frs;
	@Autowired
	private familytreetypeService fts;
	@Autowired
	private educationlevelService els;
	
	
	@RequestMapping("familylist/{ids}")
	public String familylist(Model model,
			@PathVariable(name="ids")Long ids) {
		List<keluarga> semuafamily = fs.semuafamily(ids);
//		List<familyModel> semuafamily = fs.semuafamilyj();
//		familyModel familybyid=fs.familybyid(ids);
		model.addAttribute("familybyid", ids);
		model.addAttribute("semuafamily", semuafamily);
		return "family/familylist";
	}
	
	@RequestMapping("family")
	public String family() {
		return "family/family";
	}
	
	@RequestMapping("familyadd/{ids}")
	public String familyadd(Model model,
			@PathVariable(name = "ids")Long ids) {
//		List<familyrelationModel> semuarelation = frs.semuafamilyrelation();
//		List<familyrelationModel> semuarelationj = frs.relationtreetype(ids);
		List<familytreetypeModel> semuatreetype = fts.semuafamilytreetype();
		List<educationlevelModel> semuaeducation = els.semuaeducationlevel();
//		model.addAttribute("semuarelation", semuarelation);
		model.addAttribute("semuatreetype", semuatreetype);
		model.addAttribute("semuaeducation", semuaeducation);
		model.addAttribute("ids", ids);
		return "family/familyadd";
	}
	
	@ResponseBody
	@RequestMapping("simpanfamily")
	public String simpanfamily(@ModelAttribute()familyModel familyModel) {
		Long loguser = (long) 101;
		familyModel.setCreatedBy(new Long(loguser));
		familyModel.setCreatedOn(new Date());
		fs.simpanfamily(familyModel);
		return "";
	}
	
	@ResponseBody
	@RequestMapping("ubahfamily")
	public String ubahfamily(@ModelAttribute()familyModel familyModel) {
		Long loguser = (long) 101;
		familyModel familybyid=fs.familybyid(familyModel.getId());
		
		familyModel.setBiodataId(familybyid.getBiodataId());
		familyModel.setCreatedBy(familybyid.getCreatedBy());
		familyModel.setCreatedOn(familybyid.getCreatedOn());
		familyModel.setModifiedBy(new Long(101));
		familyModel.setModifiedOn(new Date());
		fs.simpanfamily(familyModel);
		return "";
	}
	
	@ResponseBody
	@RequestMapping("hapusfamily/{ids}")
	public String hapusfamily(@PathVariable(name = "ids")Long ids,@ModelAttribute() familyModel familyModel) {
		
		familyModel familybyid=fs.familybyid(ids);
		Long loguser = (long) 101;
		familybyid.setDeletedBy(new Long(loguser));
		familybyid.setDeletedOn(new Date());
		fs.simpanfamily(familybyid);
		fs.hapusfamily(ids);
		return "";
	}
	
	@RequestMapping("familyedit/{ids}")
	public String familyedit(Model model,
			@PathVariable(name="ids")Long ids) {
		List<familyrelationModel> relationtreetype = frs.relationtreetype(ids);
		
		List<familyrelationModel> semuarelation = frs.semuafamilyrelation();
		List<familytreetypeModel> semuatreetype = fts.semuafamilytreetype();
		List<educationlevelModel> semuaeducation = els.semuaeducationlevel();
		familyModel familybyid=fs.familybyid(ids);

		model.addAttribute("relationtreetype", relationtreetype);

		model.addAttribute("semuarelation", semuarelation);
		model.addAttribute("semuatreetype", semuatreetype);
		model.addAttribute("semuaeducation", semuaeducation);
		model.addAttribute("bid", ids);
		model.addAttribute("familybyid", familybyid);
		return "family/familyedit";
	}
	
	@RequestMapping("familydelete/{ids}")
	public String familydelete(Model model,
			@PathVariable(name="ids")Long ids) {
		
		familyModel familybyid=fs.familybyid(ids);
		model.addAttribute("familybyid", familybyid);
		return "family/familydelete";
	}
	@InitBinder
	public void initBinder(WebDataBinder binder) {
	    CustomDateEditor editor = new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true);
	    binder.registerCustomEditor(Date.class, editor);
	}
	
//	@RequestMapping("relation/{ids}")
//	public 	List<familyrelationModel> relationtreetype(@PathVariable(name = "ids")Long ids, Model model){
//return frs.relationtreetype(ids);
//	}
	@RequestMapping("relation/{ids}")
	public String relationtreetype(@PathVariable(name = "ids")Long ids, Model model){
		List<familyrelationModel> relationtreetype = frs.relationtreetype(ids);
//	List<jkeluarga> relationtreetype = frs.relationtreetypeJ(ids);
		
		model.addAttribute("relationtreetype", relationtreetype);
		return "family/relation";
	
	}
	@RequestMapping("relation2/{ids}/{id2}")
	public String relationtreetype2(@PathVariable(name = "ids")Long ids, Model model, @PathVariable(name = "id2")Long id2){
		List<familyrelationModel> relationtreetype = frs.relationtreetype(ids);
//	List<jkeluarga> relationtreetype = frs.relationtreetypeJ(ids);
		familyModel familybyid=fs.familybyid(id2);
		model.addAttribute("familybyid", familybyid);
		model.addAttribute("relationtreetype", relationtreetype);
		return "family/relation2";
		
	}
}
