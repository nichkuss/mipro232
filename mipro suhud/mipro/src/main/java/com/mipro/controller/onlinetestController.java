package com.mipro.controller;



import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;

import javax.persistence.Convert;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mipro.config.smtpmail;
import com.mipro.dto.dettype;
import com.mipro.dto.transaksi;
import com.mipro.dto.adb;
import com.mipro.model.addrbookModel;
import com.mipro.model.onlinetestModel;
import com.mipro.model.testdetailModel;
import com.mipro.model.testtypeModel;
import com.mipro.model.pelamarModel;
import com.mipro.service.addrbookService;
import com.mipro.service.onlinetestService;
import com.mipro.service.pelamarService;
import com.mipro.service.testdetailService;
import com.mipro.service.testtypeService;

@Controller
public class onlinetestController {
	
	@Autowired
	private onlinetestService os;
	
	@Autowired
	private testtypeService ts;
	
	@Autowired
	private testdetailService tds;
	
	@Autowired
	private addrbookService as;

	@Autowired
	private pelamarService ps;
	
	@Autowired
	smtpmail smtpMailSender;
	
//	@RequestMapping("onlinetestlist")
//	public String onlinetestlist(Model model) {
//		List<onlinetestModel>semuaonlinetest=ss.semuaonlinetest();
//		model.addAttribute("semuaonlinetest", semuaonlinetest);
//		return "onlinetest/onlinetestlist";
//	}
	
	@RequestMapping("onlinetestlist/{ids}")
	public String onlinetestlist(Model model,
			@PathVariable(name="ids")Long ids) {
//		List<onlinetestModel> semuaonlinetestlist = os.semuaonlinetest(ids);
		model.addAttribute("semuaonlinetestbyid", ids);
//		model.addAttribute("semuaonlinetestlist", semuaonlinetestlist);
		return "onlinetest/onlinetestlist";
	}
	@RequestMapping("onlinetestlist2/{ids}")
	public String onlinetestlist2(Model model,
			@PathVariable(name="ids")Long ids) {
		List<onlinetestModel> semuaonlinetest = os.semuaonlinetest(ids);
		model.addAttribute("semuaonlinetestbyid", ids);
		model.addAttribute("semuaonlinetest", semuaonlinetest);
		return "onlinetest/onlinetestlist2";
	}
	
	@RequestMapping("onlinetestdetail/{ids}")
	public String onlinetestdetail(Model model,@PathVariable(name="ids")Long ids) {
		List<dettype>semuatestdetail = tds.semuatestdetailJ(ids);
		model.addAttribute("semuatestdetail", semuatestdetail);
		model.addAttribute("biodataId", ids);
		return "onlinetest/onlinetestdetail";
	}

	@RequestMapping("onlinetestdetail2/{ids}")
	public String onlinetestdetail2(Model model, @PathVariable(name="ids")Long ids) {
		List<dettype>semuatestdetail = tds.semuatestdetailJ(ids);
		model.addAttribute("id", ids);
		model.addAttribute("semuatestdetail2", semuatestdetail);
		return "onlinetest/onlinetestdetail2";
	}

	@RequestMapping("adb/{ids}")
	public String adb(Model model,
					@PathVariable(name = "ids")Long ids) {
		List<adb>semuaadb = os.addrbio(ids);
		model.addAttribute("semuaadb", semuaadb);
//		System.out.println(semuaadb+" HAI");
		return "onlinetest/adb";
	}


	@RequestMapping("tambahtest")
	public String tambahtest(Model model) {
		List<testtypeModel>semuatesttype = ts.semuatesstype();
		model.addAttribute("semuatesttype", semuatesttype);
		return "onlinetest/tambahtest";
	}
	
	@ResponseBody
	@RequestMapping("simpanonlinetest/{detailtestId}/{detailtestorder}")
	public String simpanonlinetest(@ModelAttribute()testdetailModel testdetailModel,
									@PathVariable String[] detailtestId,
									@PathVariable String[] detailtestorder,
									@ModelAttribute()onlinetestModel onlinetestModel,
									@ModelAttribute()pelamarModel pelamarModel,
									@ModelAttribute()addrbookModel addrbookModel) {
		Long loguser = (long) 101;
//		onlinetestModel onlinetestModel = new onlinetestModel();
//		
//		onlinetestModel.setBiodataId(new Long(1));
		onlinetestModel.setCreatedBy(new Long(loguser));
		onlinetestModel.setCreatedOn(new Date());
		onlinetestModel.setStatus("Proses");
		os.saveiltesmd(onlinetestModel);
		
		//get max period by id
		System.out.println(os.getperiodbyid(onlinetestModel.getBiodataId()));
		
		
		onlinetestModel.setPeriod(os.getperiodbyid(onlinetestModel.getBiodataId()));
		onlinetestModel.setPeriodCode("PRD"+onlinetestModel.getId());
		os.saveiltesmd(onlinetestModel);
		Long ids = onlinetestModel.getId();
		
		pelamarModel pelamarbyid = ps.pelamarbyid(onlinetestModel.getBiodataId());
		String alamattujuan = pelamarbyid.getEmail();
		addrbookModel addrbookbyid = as.addrbookbyid(pelamarbyid.getAddrbookId());
		String uname = addrbookbyid.getAbuid();
		String upwd = addrbookbyid.getAbpwd();
		addrbookbyid.setDeletedBy(new Long(loguser));
		addrbookbyid.setDeletedOn(new Date());
		as.simpanaddrbook(addrbookbyid);
		as.aktif(pelamarbyid.getAddrbookId());
		
		//Email
		 try {
				smtpMailSender.send(alamattujuan,"Username dan Password","Username : " + uname +"<br>"  
						+ "\n Password : " + upwd);
				Integer a=1;
				System.out.println(a);
			} catch (MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Integer a2=0;
				System.out.println(a2);
			}
		
		for (int i = 0; i < detailtestId.length; i++) {
			testdetailModel = new testdetailModel();
			testdetailModel.setCreatedBy(new Long(loguser));
			testdetailModel.setCreatedOn(new Date());
			testdetailModel.setOnlineTestId(ids);
			testdetailModel.setTestTypeId(new Long(detailtestId[i]));
			testdetailModel.setTestOrder(new Integer(detailtestorder[i]));
			testdetailModel.setIsDelete(false);
			tds.simpantestdetail(testdetailModel);
		}
		
		
//		as.nonaktif(ids);

		
		return"";
	}
	
	@ResponseBody
	@RequestMapping("hapustestdetail/{ids}")
	public String hapustestdetail(@PathVariable(name = "ids")Long ids,@ModelAttribute() testdetailModel testdetailModel) {
		
		testdetailModel testdetailbyid=tds.testdetailbyid(ids);
		Long loguser = (long) 101;
		testdetailbyid.setDeletedBy(new Long(loguser));
		testdetailbyid.setDeletedOn(new Date());
		tds.simpantestdetail(testdetailbyid);
		tds.hapustestdetail(ids);
		return "";
	}
	@ResponseBody
	@RequestMapping("hapusonlinetest/{ids}")
	public String hapusonlinetest(@PathVariable(name = "ids")Long ids,@ModelAttribute() onlinetestModel onlinetestModel) {
		
		onlinetestModel onlinetestbyid=os.onlinetestbyid(ids);
		Long loguser = (long) 101;
		onlinetestbyid.setDeletedBy(new Long(loguser));
		onlinetestbyid.setDeletedOn(new Date());
		os.saveiltesmd(onlinetestbyid);
		os.hapusonlinetest(ids);
		return "";
	}
	
	@RequestMapping("testdetaildelete/{ids}")
	public String testdetaildelete(Model model,
			@PathVariable(name="ids")Long ids) {		
		testdetailModel testdetailbyid=tds.testdetailbyid(ids);
		model.addAttribute("testdetailbyid", testdetailbyid);
		return "onlinetest/testdetaildelete";
	}
	
	@RequestMapping("onlinetestdelete/{ids}")
	public String onlinetestdelete(Model model,
			@PathVariable(name="ids")Long ids) {		
		onlinetestModel onlinetestbyid=os.onlinetestbyid(ids);
		model.addAttribute("onlinetestbyid", onlinetestbyid);
		return "onlinetest/onlinetestdelete";
	}
	
	// Punya AddrBook
	@ResponseBody
	@RequestMapping("nonaktif/{ids}")
	public String nonaktif(@PathVariable(name = "ids")Long ids,@ModelAttribute() addrbookModel addrbookModel) {
		
		addrbookModel addrbookbyid=as.addrbookbyid(ids);
		Long loguser = (long) 101;
		addrbookbyid.setDeletedBy(new Long(loguser));
		addrbookbyid.setDeletedOn(new Date());
		as.simpanaddrbook(addrbookbyid);
		as.nonaktif(ids);
		return "";
	}

	@ResponseBody
	@RequestMapping("aktif/{ids}")
	public String aktif(@PathVariable(name = "ids")Long ids,@ModelAttribute() addrbookModel addrbookModel) {
		
		addrbookModel addrbookbyid=as.addrbookbyid(ids);
		Long loguser = (long) 101;
		addrbookbyid.setDeletedBy(new Long(loguser));
		addrbookbyid.setDeletedOn(new Date());
		as.simpanaddrbook(addrbookbyid);
		as.aktif(ids);
		return "";
	}
	
}