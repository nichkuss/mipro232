package com.mipro.controller;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mipro.dto.bid;
import com.mipro.model.sertifikasiModel;
import com.mipro.service.pelamarService;
import com.mipro.service.sertifikasiService;

@Controller
public class sertifikasiController {
	
	@Autowired
	private sertifikasiService ss;
	
//	@Autowired
//	private pelamarService ps;
	
	@RequestMapping("sertifikasilist/{ids}")
	public String sertifikasilist(Model model,
			@PathVariable(name="ids")Long ids) {
//		List<sertifikasiModel>semuasertifikasi=ss.semuasertifikasi();
		List<bid> semuasertifikasi = ss.semuasertifikasiJ(ids);
//		sertifikasiModel sertifikasibyid=ss.sertifikasibyid(ids);
		model.addAttribute("sertifikasibyid", ids);
		model.addAttribute("semuasertifikasi", semuasertifikasi);
		return "sertifikasi/sertifikasilist";
	}
	
	@RequestMapping("sertifikasi")
	public String sertifikasi() {
		return "sertifikasi/sertifikasi";
	}
	
	@RequestMapping("sertifikasiadd/{ids}")
	public String sertifikasiadd(Model model,
			@PathVariable(name = "ids")Long ids) {
		isitahun(model);
		model.addAttribute("ids", ids);
		return "sertifikasi/sertifikasiadd";
	}
	
	@ResponseBody
	@RequestMapping("simpansertifikasi")
	public String simpansertifikasi(@ModelAttribute()sertifikasiModel sertifikasiModel) {
		Long loguser = (long) 101;
		sertifikasiModel.setCreatedBy(new Long(loguser));
		sertifikasiModel.setCreatedOn(new Date());
		ss.simpansertifikasi(sertifikasiModel);
		return "";
	}
	
	@ResponseBody
	@RequestMapping("ubahsertifikasi")
	public String ubahsertifikasi(@ModelAttribute()sertifikasiModel sertifikasiModel) {
		Long loguser = (long) 101;
		sertifikasiModel sertifikasibyid=ss.sertifikasibyid(sertifikasiModel.getId());
		
		sertifikasiModel.setBiodataId(sertifikasibyid.getBiodataId());
		sertifikasiModel.setCreatedBy(sertifikasibyid.getCreatedBy());
		sertifikasiModel.setCreatedOn(sertifikasibyid.getCreatedOn());
		sertifikasiModel.setModifiedBy(new Long(101));
		sertifikasiModel.setModifiedOn(new Date());
		ss.simpansertifikasi(sertifikasiModel);
		return "";
	}
	
	@ResponseBody
	@RequestMapping("hapussertifikasi/{ids}")
	public String hapussertifikasi(@PathVariable(name = "ids")Long ids,@ModelAttribute() sertifikasiModel sertifikasiModel) {
		
		sertifikasiModel sertifikasibyid=ss.sertifikasibyid(ids);
		Long loguser = (long) 101;
		sertifikasibyid.setDeletedBy(new Long(loguser));
		sertifikasibyid.setDeletedOn(new Date());
		ss.simpansertifikasi(sertifikasibyid);
		ss.hapussertifikasi(ids);
		return "";
	}
	
	@RequestMapping("sertifikasiedit/{ids}")
	public String sertifikasiedit(Model model,
			@PathVariable(name="ids")Long ids) {
		isitahun(model);
		sertifikasiModel sertifikasibyid=ss.sertifikasibyid(ids);
		model.addAttribute("sertifikasibyid", sertifikasibyid);
		return "sertifikasi/sertifikasiedit";
	}
	
	@RequestMapping("sertifikasidelete/{ids}")
	public String sertifikasidelete(Model model,
			@PathVariable(name="ids")Long ids) {
		isitahun(model);
		
		sertifikasiModel sertifikasibyid=ss.sertifikasibyid(ids);
		model.addAttribute("sertifikasibyid", sertifikasibyid);
		return "sertifikasi/sertifikasidelete";
	}
	
	public void isitahun(Model model) {
		Calendar cal = Calendar.getInstance();
		int thn = cal.get(Calendar.YEAR);
		List<Integer> tahunk = new ArrayList();
		model.addAttribute("tahun", tahunk);
		thn = thn - 30;
		for (int i = 0; i <= 30; i++) {
			tahunk.add(thn + i);
		}

		model.addAttribute("tahun", tahunk);
	}

}