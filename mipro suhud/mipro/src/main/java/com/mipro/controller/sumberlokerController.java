package com.mipro.controller;



import java.text.SimpleDateFormat;
import java.util.Date;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mipro.model.sumberlokerModel;
import com.mipro.service.sumberlokerService;

@Controller
public class sumberlokerController {
	
	@Autowired
	private sumberlokerService ss;
	
//	@RequestMapping("sumberlokerlist")
//	public String sumberlokerlist(Model model) {
//		List<sumberlokerModel>semuasumberloker=ss.semuasumberloker();
//		model.addAttribute("semuasumberloker", semuasumberloker);
//		return "sumberloker/sumberlokerlist";
//	}
	
	@RequestMapping("sumberloker")
	public String sumberloker() {
		return "sumberloker/sumberloker";
	}

	
	@ResponseBody
	@RequestMapping("ubahsumberloker")
	public String ubahsumberloker(@ModelAttribute()sumberlokerModel sumberlokerModel) {
		long loguser = 101;
		sumberlokerModel sumberlokerbyid=ss.sumberlokerbyid(sumberlokerModel.getId());
		sumberlokerModel.setCreatedBy(sumberlokerbyid.getCreatedBy());
		sumberlokerModel.setCreatedOn(sumberlokerbyid.getCreatedOn());
		
		sumberlokerModel.setModifiedBy(loguser);
		sumberlokerModel.setModifiedOn(new Date());
		ss.simpansumberloker(sumberlokerModel);
		return "";
	}
	
	@ResponseBody
	@RequestMapping("simpansumberloker")
	public String simpansumberloker(@ModelAttribute()sumberlokerModel sumberlokerModel) {
		Long loguser = (long) 101;
		sumberlokerModel.setCreatedBy(new Long(loguser));
		sumberlokerModel.setCreatedOn(new Date());
		ss.simpansumberloker(sumberlokerModel);
		return "";
	}
	
	@RequestMapping("sumberlokeredit/{ids}")
	public String sumberlokeredit(Model model,
			@PathVariable(name="ids")long ids) {
		sumberlokerModel sumberlokerbyid = ss.sumberlokerbyid(ids);
//		if (sumberlokerbyid==null) {
//			sumberlokerbyid = new sumberlokerModel();
//		} 		
		model.addAttribute("sumberlokerbyid", sumberlokerbyid);
		if (sumberlokerbyid==null) {
			return "sumberloker/sumberlokeradd";
		}else {
			return "sumberloker/sumberlokeredit";	
		}
		
	}
	
	@RequestMapping("sumberlokeradd")
	public String sumberlokeradd(){
		return "sumberloker/sumberlokeradd";
	}
	
//	@RequestMapping("sumberlokeredit")
//	public String sumberlokeredit(@ModelAttribute()sumberlokerModel sumberlokerModel, Model model) {
//		sumberlokerModel sumberlokerbyid=ss.sumberlokerbyid(sumberlokerModel.getId());
//		model.addAttribute("sumberlokerbyid", sumberlokerbyid);
//		return "sumberloker/sumberlokeredit";
//	}
	@InitBinder
	public void initBinder(WebDataBinder binder) {
	    CustomDateEditor editor = new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true);
	    binder.registerCustomEditor(Date.class, editor);
	}

}