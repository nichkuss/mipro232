package com.mipro.dto;

public class adb {

	private Long id;
	private Long biodataId;
	private String abpwd;
	private String abuid;
	private Boolean isDelete;
	public adb(Long id, Long biodataId, String abpwd, String abuid, Boolean isDelete) {
		this.id = id;
		this.biodataId = biodataId;
		this.abpwd = abpwd;
		this.abuid = abuid;
		this.isDelete = isDelete;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getBiodataId() {
		return biodataId;
	}
	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}
	public String getAbpwd() {
		return abpwd;
	}
	public void setAbpwd(String abpwd) {
		this.abpwd = abpwd;
	}
	public String getAbuid() {
		return abuid;
	}
	public void setAbuid(String abuid) {
		this.abuid = abuid;
	}
	public Boolean getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}	
	
}
