package com.mipro.dto;

public class bid {
	private Long id;
	private Long biodataId;
	private String certificateName;
	private String publisher;
	private String validStartYear;
	private String validStartMonth;
	private String untilYear;
	private String untilMonth;
	public bid(Long id, Long biodataId, String certificateName, String publisher, String validStartYear,
			String validStartMonth, String untilYear, String untilMonth) {
		this.id = id;
		this.biodataId = biodataId;
		this.certificateName = certificateName;
		this.publisher = publisher;
		this.validStartYear = validStartYear;
		this.validStartMonth = validStartMonth;
		this.untilYear = untilYear;
		this.untilMonth = untilMonth;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getBiodataId() {
		return biodataId;
	}
	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}
	public String getCertificateName() {
		return certificateName;
	}
	public void setCertificateName(String certificateName) {
		this.certificateName = certificateName;
	}
	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	public String getValidStartYear() {
		return validStartYear;
	}
	public void setValidStartYear(String validStartYear) {
		this.validStartYear = validStartYear;
	}
	public String getValidStartMonth() {
		return validStartMonth;
	}
	public void setValidStartMonth(String validStartMonth) {
		this.validStartMonth = validStartMonth;
	}
	public String getUntilYear() {
		return untilYear;
	}
	public void setUntilYear(String untilYear) {
		this.untilYear = untilYear;
	}
	public String getUntilMonth() {
		return untilMonth;
	}
	public void setUntilMonth(String untilMonth) {
		this.untilMonth = untilMonth;
	}
	
}
