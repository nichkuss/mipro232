package com.mipro.dto;

public class dettype {
	private Long id;
	private Long testTypeId;
	private String name;
	private Integer testOrder;
	private Boolean isDelete;
	public dettype(Long id, Long testTypeId, String name, Integer testOrder, Boolean isDelete) {
		this.id = id;
		this.testTypeId = testTypeId;
		this.name = name;
		this.testOrder = testOrder;
		this.isDelete = isDelete;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getTestTypeId() {
		return testTypeId;
	}
	public void setTestTypeId(Long testTypeId) {
		this.testTypeId = testTypeId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getTestOrder() {
		return testOrder;
	}
	public void setTestOrder(Integer testOrder) {
		this.testOrder = testOrder;
	}
	public Boolean getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}
	
}
