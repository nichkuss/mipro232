package com.mipro.dto;

public class jkeluarga {

	private Long id;
	private Long familyTreeTypeId;
	private String name;
	public jkeluarga(Long id, Long familyTreeTypeId, String name) {
		this.id = id;
		this.familyTreeTypeId = familyTreeTypeId;
		this.name = name;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getFamilyTreeTypeId() {
		return familyTreeTypeId;
	}
	public void setFamilyTreeTypeId(Long familyTreeTypeId) {
		this.familyTreeTypeId = familyTreeTypeId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

}
