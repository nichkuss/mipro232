package com.mipro.dto;

import java.util.Date;

public class keluarga {
	private Long id;
	private Long biodataId;
	private Long familyTreeTypeId;
	private Long familyRelationId;
	private Long educationLevelId;
	private String name;
	private Boolean gender;
	private Date dob;
	private String job;
	private String note;
	private String treetypename;
	private String relationname;
	private String educationname;
	public keluarga(Long id, Long biodataId, Long familyTreeTypeId, Long familyRelationId, Long educationLevelId,
			String name, Boolean gender, Date dob, String job, String note, String treetypename, String relationname,
			String educationname) {
		this.id = id;
		this.biodataId = biodataId;
		this.familyTreeTypeId = familyTreeTypeId;
		this.familyRelationId = familyRelationId;
		this.educationLevelId = educationLevelId;
		this.name = name;
		this.gender = gender;
		this.dob = dob;
		this.job = job;
		this.note = note;
		this.treetypename = treetypename;
		this.relationname = relationname;
		this.educationname = educationname;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getBiodataId() {
		return biodataId;
	}
	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}
	public Long getFamilyTreeTypeId() {
		return familyTreeTypeId;
	}
	public void setFamilyTreeTypeId(Long familyTreeTypeId) {
		this.familyTreeTypeId = familyTreeTypeId;
	}
	public Long getFamilyRelationId() {
		return familyRelationId;
	}
	public void setFamilyRelationId(Long familyRelationId) {
		this.familyRelationId = familyRelationId;
	}
	public Long getEducationLevelId() {
		return educationLevelId;
	}
	public void setEducationLevelId(Long educationLevelId) {
		this.educationLevelId = educationLevelId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Boolean getGender() {
		return gender;
	}
	public void setGender(Boolean gender) {
		this.gender = gender;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public String getJob() {
		return job;
	}
	public void setJob(String job) {
		this.job = job;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getTreetypename() {
		return treetypename;
	}
	public void setTreetypename(String treetypename) {
		this.treetypename = treetypename;
	}
	public String getRelationname() {
		return relationname;
	}
	public void setRelationname(String relationname) {
		this.relationname = relationname;
	}
	public String getEducationname() {
		return educationname;
	}
	public void setEducationname(String educationname) {
		this.educationname = educationname;
	}
		
	
}
