package com.mipro.dto;

import java.util.Date;

public class transaksi {
	
	//Address Book
	private Long idAddr;
	private String abpwd;
	private String abuid;
	
	//Biodata
	private Long idBiodata;
	private Long addrbookId;
	
	//Test Type
	private Long idTestType;
	private String name;
	private String description;
	
	//Online Test
	private Long idOnlineTest;
	private Long biodataId;
	private String periodCode;
	private Integer period;
	private Date testDate;
	private Date expiredTest;
	private String userAccess;
	private String status;
	
	//Test Detail
	private Long onlineTestId;
	private Long testTypeId;
	private Integer testOrder;
	public transaksi(Long idAddr, String abpwd, String abuid, Long idBiodata, Long addrbookId, Long idTestType,
			String name, String description, Long idOnlineTest, Long biodataId, String periodCode, Integer period,
			Date testDate, Date expiredTest, String userAccess, String status, Long onlineTestId, Long testTypeId,
			Integer testOrder) {
		this.idAddr = idAddr;
		this.abpwd = abpwd;
		this.abuid = abuid;
		this.idBiodata = idBiodata;
		this.addrbookId = addrbookId;
		this.idTestType = idTestType;
		this.name = name;
		this.description = description;
		this.idOnlineTest = idOnlineTest;
		this.biodataId = biodataId;
		this.periodCode = periodCode;
		this.period = period;
		this.testDate = testDate;
		this.expiredTest = expiredTest;
		this.userAccess = userAccess;
		this.status = status;
		this.onlineTestId = onlineTestId;
		this.testTypeId = testTypeId;
		this.testOrder = testOrder;
	}
	public Long getIdAddr() {
		return idAddr;
	}
	public void setIdAddr(Long idAddr) {
		this.idAddr = idAddr;
	}
	public String getAbpwd() {
		return abpwd;
	}
	public void setAbpwd(String abpwd) {
		this.abpwd = abpwd;
	}
	public String getAbuid() {
		return abuid;
	}
	public void setAbuid(String abuid) {
		this.abuid = abuid;
	}
	public Long getIdBiodata() {
		return idBiodata;
	}
	public void setIdBiodata(Long idBiodata) {
		this.idBiodata = idBiodata;
	}
	public Long getAddrbookId() {
		return addrbookId;
	}
	public void setAddrbookId(Long addrbookId) {
		this.addrbookId = addrbookId;
	}
	public Long getIdTestType() {
		return idTestType;
	}
	public void setIdTestType(Long idTestType) {
		this.idTestType = idTestType;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getIdOnlineTest() {
		return idOnlineTest;
	}
	public void setIdOnlineTest(Long idOnlineTest) {
		this.idOnlineTest = idOnlineTest;
	}
	public Long getBiodataId() {
		return biodataId;
	}
	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}
	public String getPeriodCode() {
		return periodCode;
	}
	public void setPeriodCode(String periodCode) {
		this.periodCode = periodCode;
	}
	public Integer getPeriod() {
		return period;
	}
	public void setPeriod(Integer period) {
		this.period = period;
	}
	public Date getTestDate() {
		return testDate;
	}
	public void setTestDate(Date testDate) {
		this.testDate = testDate;
	}
	public Date getExpiredTest() {
		return expiredTest;
	}
	public void setExpiredTest(Date expiredTest) {
		this.expiredTest = expiredTest;
	}
	public String getUserAccess() {
		return userAccess;
	}
	public void setUserAccess(String userAccess) {
		this.userAccess = userAccess;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Long getOnlineTestId() {
		return onlineTestId;
	}
	public void setOnlineTestId(Long onlineTestId) {
		this.onlineTestId = onlineTestId;
	}
	public Long getTestTypeId() {
		return testTypeId;
	}
	public void setTestTypeId(Long testTypeId) {
		this.testTypeId = testTypeId;
	}
	public Integer getTestOrder() {
		return testOrder;
	}
	public void setTestOrder(Integer testOrder) {
		this.testOrder = testOrder;
	}
	
	
	
	
}
