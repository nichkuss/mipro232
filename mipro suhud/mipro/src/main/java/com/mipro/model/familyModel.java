package com.mipro.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="x_keluarga")
public class familyModel {

	@Id
//	@Size(max = 11)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id", length=11)
	private Long id;
	
	@Column(name="created_by", length=11, nullable = false)
	private Long createdBy;

	@Column(name="created_on", nullable = false)
	private Date createdOn;

	
	@Column(name="modified_by", length=11)
	private Long modifiedBy;

	
	@Column(name="modified_on")
	private Date modifiedOn;

	
	@Column(name="deleted_by", length=11)
	private Long deletedBy;

	@Column(name="deleted_on")
	private Date deletedOn;
	
	@Column(name="is_delete", columnDefinition = "boolean default false", nullable = false)
	private boolean isDelete;
	
	@Column(name="biodata_id", length=11, nullable = false )
	private Long biodataId;

	@Column(name="family_tree_type_id", length=11)
	private Long familyTreeTypeId;
	
	@Column(name="family_relation_id", length=11)
	private Long familyRelationId;
	
	
	@Column(name = "name", length=100)
	private String name;
	
	@Column(name = "gender", nullable = false)
	private Boolean gender;
	
	@Column(name = "dob")
	private Date dob;
	
	@Column(name = "education_level_id", length=11)
	private Long educationLevelId;
	
	@Column(name = "job", length=100)
	private String job;
	
	@Column(name = "note", length=1000)
	private String note;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Long getBiodataId() {
		return biodataId;
	}

	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}

	public Long getFamilyTreeTypeId() {
		return familyTreeTypeId;
	}

	public void setFamilyTreeTypeId(Long familyTreeTypeId) {
		this.familyTreeTypeId = familyTreeTypeId;
	}

	public Long getFamilyRelationId() {
		return familyRelationId;
	}

	public void setFamilyRelationId(Long familyRelationId) {
		this.familyRelationId = familyRelationId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getGender() {
		return gender;
	}

	public void setGender(Boolean gender) {
		this.gender = gender;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public Long getEducationLevelId() {
		return educationLevelId;
	}

	public void setEducationLevelId(Long educationLevelId) {
		this.educationLevelId = educationLevelId;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
	
}
