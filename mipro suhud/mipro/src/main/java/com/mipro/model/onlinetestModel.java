package com.mipro.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="x_online_test")
public class onlinetestModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id", length=11)
	private Long id;
	
	@Column(name="created_by", length=11, nullable = false)
	private Long createdBy;

	@Column(name="created_on", nullable = false)
	private Date createdOn;
	
	@Column(name="modified_by", length=11)
	private Long modifiedBy;

	@Column(name="modified_on")
	private Date modifiedOn;
	
	@Column(name="deleted_by", length=11)
	private Long deletedBy;

	@Column(name="deleted_on")
	private Date deletedOn;
	
	@Column(name="is_delete", columnDefinition = "boolean default false", nullable = false)
	private boolean isDelete;
	
	@Column(name="biodata_id", length=11, nullable = false )
	private Long biodataId;

    @GeneratedValue(generator = "period_code")
    @GenericGenerator(
        name = "period_code",
    strategy = "org.hibernate.id.UUIDGenerator"
    )
	@Column(length=50)
	private String periodCode;
	
	@Column(name="period")
	private int period;
	
	@Column(name="test_date")
	private Date testDate;
	
	@Column(name="expired_test")
	private Date expiredTest;
	
	@Column(name="user_access", length=10)
	private String userAccess;

	@Column(name="status", length=50)
	private String status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Long getBiodataId() {
		return biodataId;
	}

	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}

	public String getPeriodCode() {
		return periodCode;
	}

	public void setPeriodCode(String periodCode) {
		this.periodCode = periodCode;
	}

	public int getPeriod() {
		return period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}

	public Date getTestDate() {
		return testDate;
	}

	public void setTestDate(Date testDate) {
		this.testDate = testDate;
	}

	public Date getExpiredTest() {
		return expiredTest;
	}

	public void setExpiredTest(Date expiredTest) {
		this.expiredTest = expiredTest;
	}

	public String getUserAccess() {
		return userAccess;
	}

	public void setUserAccess(String userAccess) {
		this.userAccess = userAccess;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
