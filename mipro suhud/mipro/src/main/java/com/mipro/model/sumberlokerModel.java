package com.mipro.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="x_sumber_loker")
public class sumberlokerModel {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id", length=11)
	private long id;
	
	@Column(name="created_by", length=11, nullable = false)
	private long createdBy;

	@Column(name="created_on", nullable = false)
	private Date createdOn;

	
	@Column(name="modified_by", length=11)
	private long modifiedBy;

	
	@Column(name="modified_on")
	private Date modifiedOn;

	
	@Column(name="deleted_by", length=11)
	private long deletedBy;

	@Column(name="deleted_on")
	private Date deletedOn;
	
	@Column(name="is_delete", columnDefinition = "boolean default false", nullable = false)
	private boolean isDelete;
	
	@Column(name="biodata_id", length=11, nullable = false )
	private long biodataId;
	
	@Column(name="vacancy_source", length=20)
	private String vacancySource;
	
	@Column(name="candidate_type", length=20)
	private String candidateType;
	
	@Column(name="event_name", length=100)
	private String eventName;
	
	@Column(name="career_center_name", length=100)
	private String careerCenterName;
	
	@Column(name="referrer_name", length=100)
	private String referrerName;
	
	@Column(name="referrer_phone", length=20)
	private String referrerPhone;
	
	@Column(name="referrer_email", length=100)
	private String referrerEmail;
	
	@Column(name="other_source", length=100)
	private String otherSource;
	
	@Column(name="last_income", length=20)
	private String lastIncome;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name="apply_date")
	private Date applyDate;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public long getBiodataId() {
		return biodataId;
	}

	public void setBiodataId(long biodataId) {
		this.biodataId = biodataId;
	}

	public String getVacancySource() {
		return vacancySource;
	}

	public void setVacancySource(String vacancySource) {
		this.vacancySource = vacancySource;
	}

	public String getCandidateType() {
		return candidateType;
	}

	public void setCandidateType(String candidateType) {
		this.candidateType = candidateType;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getCareerCenterName() {
		return careerCenterName;
	}

	public void setCareerCenterName(String careerCenterName) {
		this.careerCenterName = careerCenterName;
	}

	public String getReferrerName() {
		return referrerName;
	}

	public void setReferrerName(String referrerName) {
		this.referrerName = referrerName;
	}

	public String getReferrerPhone() {
		return referrerPhone;
	}

	public void setReferrerPhone(String referrerPhone) {
		this.referrerPhone = referrerPhone;
	}

	public String getReferrerEmail() {
		return referrerEmail;
	}

	public void setReferrerEmail(String referrerEmail) {
		this.referrerEmail = referrerEmail;
	}

	public String getOtherSource() {
		return otherSource;
	}

	public void setOtherSource(String otherSource) {
		this.otherSource = otherSource;
	}

	public String getLastIncome() {
		return lastIncome;
	}

	public void setLastIncome(String lastIncome) {
		this.lastIncome = lastIncome;
	}

	public Date getApplyDate() {
		return applyDate;
	}

	public void setApplyDate(Date applyDate) {
		this.applyDate = applyDate;
	}

	
}
