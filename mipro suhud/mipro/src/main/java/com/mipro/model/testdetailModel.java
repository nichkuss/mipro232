package com.mipro.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="x_online_test_detail")
public class testdetailModel {
	@Id
//	@Size(max = 11)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id", length=11)
	private Long id;
	
	@Column(name="created_by", length=11, nullable = false)
	private Long createdBy;

	@Column(name="created_on", nullable = false)
	private Date createdOn;

	
	@Column(name="modified_by", length=11)
	private Long modifiedBy;

	
	@Column(name="modified_on")
	private Date modifiedOn;

	
	@Column(name="deleted_by", length=11)
	private Long deletedBy;

	@Column(name="deleted_on")
	private Date deletedOn;
	
	@Column(name="is_delete", columnDefinition = "boolean default false", nullable = false)
	private Boolean isDelete;
	
	@Column(name="online_test_id", length=11)
	private Long onlineTestId;
	
	@Column(name="test_type_id", length=11)
	private Long testTypeId;
	
	@Column(name="test_order")
	private int testOrder;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Long getOnlineTestId() {
		return onlineTestId;
	}

	public void setOnlineTestId(Long onlineTestId) {
		this.onlineTestId = onlineTestId;
	}

	public Long getTestTypeId() {
		return testTypeId;
	}

	public void setTestTypeId(Long testTypeId) {
		this.testTypeId = testTypeId;
	}

	public int getTestOrder() {
		return testOrder;
	}

	public void setTestOrder(int testOrder) {
		this.testOrder = testOrder;
	}

}
