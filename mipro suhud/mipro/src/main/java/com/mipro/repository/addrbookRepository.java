package com.mipro.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.mipro.model.addrbookModel;
import com.mipro.model.familyModel;

public interface addrbookRepository extends JpaRepository<addrbookModel, Long> {

	@Query("select s from addrbookModel s where s.id=?1")  // k  terakhir = singakatan
	addrbookModel addrbookbyid(Long ids);
	
	@Modifying // Ubah Data
	@Query("update addrbookModel set isDelete = true where id=?1")
	int deleteflag(Long ids);
	
	@Modifying // Ubah Data
	@Query("update addrbookModel set isDelete = false where id=?1")
	int aktifflag(Long ids);
	
	
}
