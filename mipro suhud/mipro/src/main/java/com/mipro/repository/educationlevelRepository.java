package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;


import com.mipro.model.educationlevelModel;

public interface educationlevelRepository extends JpaRepository<educationlevelModel, Long> {

	@Query("select f from educationlevelModel f")
	List<educationlevelModel>semuaeducationlevel();
	
	@Query("select s from educationlevelModel s where s.id=?1")  // k  terakhir = singakatan
	educationlevelModel educationlevelbyid(Long ids);
	
	@Modifying // Ubah Data
	@Query("update educationlevelModel set isDelete = true where id=?1")
	int deleteflag(Long ids);
	
}