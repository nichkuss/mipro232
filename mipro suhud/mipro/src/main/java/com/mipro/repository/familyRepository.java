package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.mipro.dto.keluarga;
import com.mipro.model.familyModel;

public interface familyRepository extends JpaRepository<familyModel, Long> {

	@Query("select new com.mipro.dto.keluarga(a.id, a.biodataId, a.familyTreeTypeId, a.familyRelationId, a.educationLevelId, a.name, a.gender, a.dob, a.job, a.note, c.name, d.name, e.name )from familyModel a inner join pelamarModel b on a.biodataId = b.id inner join familytreetypeModel c on a.familyTreeTypeId = c.id inner join familyrelationModel d on a.familyRelationId = d.id inner join educationlevelModel e on a.educationLevelId = e.id where a.isDelete=false and a.biodataId=?1 order by a.id")
	List<keluarga>semuafamily(Long ids);
	
	@Query("select f from familyModel f")
	List<familyModel>semuafamilyj();
	
	@Query("select s from familyModel s where s.id=?1")  // k  terakhir = singakatan
	familyModel familybyid(Long ids);
	
	@Modifying // Ubah Data
	@Query("update familyModel set isDelete = true where id=?1")
	int deleteflag(Long ids);
	
}
