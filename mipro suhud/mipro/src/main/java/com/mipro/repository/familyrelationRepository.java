package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.mipro.dto.jkeluarga;
import com.mipro.model.familyrelationModel;

public interface familyrelationRepository extends JpaRepository<familyrelationModel, Long> {

	@Query("select f from familyrelationModel f where f.isDelete=false")
	List<familyrelationModel>semuafamilyrelation();
	
	@Query("select s from familyrelationModel s where s.id=?1")  // k  terakhir = singakatan
	familyrelationModel familyrelationbyid(Long ids);
	
	@Modifying // Ubah Data
	@Query("update familyrelationModel set isDelete = true where id=?1")
	int deleteflag(Long ids);
	
	@Query("select f from familyrelationModel f where f.familyTreeTypeId=?1")
	List<familyrelationModel>relationtreetype(Long ids);
	
	@Query("select new com.mipro.dto.jkeluarga(a.id, a.familyTreeTypeId, b.name) from familyrelationModel a inner join familytreetypeModel b on a.familyTreeTypeId = b.id where a.familyTreeTypeId=?1")
	List<jkeluarga>relationtreetypeJ(Long ids);
}