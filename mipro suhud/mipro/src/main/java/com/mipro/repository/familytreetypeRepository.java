package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;


import com.mipro.model.familytreetypeModel;

public interface familytreetypeRepository extends JpaRepository<familytreetypeModel, Long> {

	@Query("select f from familytreetypeModel f")
	List<familytreetypeModel>semuafamilytreetype();
	
	@Query("select s from familytreetypeModel s where s.id=?1")  // k  terakhir = singakatan
	familytreetypeModel familytreetypebyid(Long ids);
	
	@Modifying // Ubah Data
	@Query("update familytreetypeModel set isDelete = true where id=?1")
	int deleteflag(Long ids);
	
}