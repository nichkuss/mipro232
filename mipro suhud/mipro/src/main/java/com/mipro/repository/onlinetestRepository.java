package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.mipro.dto.adb;
import com.mipro.dto.transaksi;
import com.mipro.model.onlinetestModel;

public interface onlinetestRepository extends JpaRepository<onlinetestModel, Long> {
	@Query("select p from onlinetestModel p where p.biodataId=?1 and p.isDelete=false order by p.id ") 
	List<onlinetestModel>semuaonlinetest(Long ids);
	
	@Query("select new com.mipro.dto.transaksi(a.id, a.abpwd, a.abuid, b.id as idBiodata, b.addrbookId, t.id, t.name, t.description, o.id, o.biodataId, o.periodCode, o.period, o.testDate, o.expiredTest, o.userAccess, o.status, d.onlineTestId, d.testTypeId, d.testOrder) from onlinetestModel o inner join pelamarModel b on o.biodataId = b.id inner join addrbookModel a on b.addrbookId = a.id inner join testdetailModel d on o.id = d.onlineTestId inner join testtypeModel t on d.testTypeId = t.id where o.biodataId=?1 and o.isDelete=false")
	List<transaksi>semuaonlinetestJ(Long ids);

	@Query("select s from onlinetestModel s where s.id=?1")  // k  terakhir = singakatan
	onlinetestModel onlinetestbyid(long ids);
	
	@Modifying // Ubah Data
	@Query("update onlinetestModel set isDelete = true where id=?1")
	int deleteflag(Long ids);

	@Modifying // Ubah Data
	@Query("update onlinetestModel set status = 'Selesai' where id=?1")
	int selesaiflag(Long ids);
	

	@Query("select new com.mipro.dto.adb( a.id, b.addrbookId, a.abpwd, a.abuid, a.isDelete) from pelamarModel b inner join addrbookModel a on b.addrbookId = a.id where b.addrbookId=?1")
	List<adb>addrbio(Long ids);
	
	@Query("select coalesce(max(ot.period),0)+1 as prd from onlinetestModel ot where ot.biodataId=?1")  // k  terakhir = singakatan
	Integer getperiodbyid(long ids);
}
