package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.mipro.dto.bid;
import com.mipro.model.sertifikasiModel;


public interface sertifikasiRepository extends JpaRepository<sertifikasiModel, Long> {
	@Query("select s from sertifikasiModel s where isDelete=false order by s.id ") 
	List<sertifikasiModel>semuasertifikasi();
	
	@Query("select new com.mipro.dto.bid(s.id, s.biodataId,s.certificateName, s.publisher, s.validStartYear, s.validStartMonth, s.untilMonth, s.untilYear) from sertifikasiModel s INNER join pelamarModel p on s.biodataId = p.id where s.isDelete=false and s.biodataId=?1 order by s.id")
	List<bid>semuasertifikasiJ(Long ids);
	
//	@Query("select s from sertifikasiModel s where s.biodataId=?1")
//	Long biodataid(Long ids);
	
	@Query("select s from sertifikasiModel s where s.id=?1")  // k  terakhir = singakatan
	sertifikasiModel sertifikasibyid(Long ids);
	
	@Modifying // Ubah Data
	@Query("update sertifikasiModel set isDelete = true where id=?1")
	int deleteflag(Long ids);
	
	@Query("select s.certificateName from sertifikasiModel s where s.certificateName=?1")
	String ceksertifikasi(String ceksertifikasi);
	
//	@Query("select s from sertifikasiModel s where s.biodataId=?1")
//	List<sertifikasiModel> sertifikasibybiodataid(Long ids);
}
