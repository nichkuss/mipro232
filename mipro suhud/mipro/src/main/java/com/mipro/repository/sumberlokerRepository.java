package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mipro.model.sumberlokerModel;


public interface sumberlokerRepository extends JpaRepository<sumberlokerModel, Long> {
	@Query("select s from sumberlokerModel s where isDelete=false order by s.id ") 
	List<sumberlokerModel>semuasumberloker();
	
	@Query("select s from sumberlokerModel s where s.id=?1")  // k  terakhir = singakatan
	sumberlokerModel sumberlokerbyid(long ids);
	
}
