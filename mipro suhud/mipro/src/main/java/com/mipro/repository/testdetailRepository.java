package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.mipro.dto.dettype;
import com.mipro.model.testdetailModel;

public interface testdetailRepository extends JpaRepository<testdetailModel, Long> {
 
	@Query("select s from testdetailModel s where isDelete=false order by s.id ") 
	List<testdetailModel>semuatestdetail();
	
	@Query("select new com.mipro.dto.dettype(d.id, d.testTypeId, t.name, d.testOrder, d.isDelete)from testdetailModel d inner join testtypeModel t on d.testTypeId = t.id where d.isDelete=false and d.onlineTestId=?1")
	List<dettype>semuatestdetailJ(Long ids);
//	@Query("select s from testdetailModel s where s.biodataId=?1")
//	Long biodataid(Long ids);
	
	@Query("select s from testdetailModel s where s.id=?1")  // k  terakhir = singakatan
	testdetailModel testdetailbyid(Long ids);
	
	@Modifying // Ubah Data
	@Query("update testdetailModel set isDelete = true where id=?1")
	int deleteflag(Long ids);
}
