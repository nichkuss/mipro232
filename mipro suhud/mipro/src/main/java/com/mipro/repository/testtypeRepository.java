package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.mipro.model.testtypeModel;

public interface testtypeRepository extends JpaRepository<testtypeModel, Long> {

	@Query("select t from testtypeModel t where isDelete=false order by t.id ") 
	List<testtypeModel>semuatesttype();
	
	@Query("select s from sertifikasiModel s where s.id=?1")  // k  terakhir = singakatan
	testtypeModel testtypebyid(Long ids);
	
	@Modifying // Ubah Data
	@Query("update sertifikasiModel set isDelete = true where id=?1")
	int deleteflag(Long ids);
	
}
