package com.mipro.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.model.addrbookModel;
import com.mipro.repository.addrbookRepository;

@Service
@Transactional
public class addrbookService {
	
	@Autowired
	private addrbookRepository ar;
	
	public addrbookModel simpanaddrbook(addrbookModel addrbookModel) {
		return ar.save(addrbookModel);
	}

	
	public addrbookModel addrbookbyid(Long ids) {
		return ar.addrbookbyid(ids);
	}


	public void nonaktif(Long ids) {
		ar.deleteflag(ids);
	}
	public void aktif(Long ids) {
		ar.aktifflag(ids);
	}
}
