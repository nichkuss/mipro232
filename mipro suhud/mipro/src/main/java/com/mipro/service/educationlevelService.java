package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.model.educationlevelModel;
import com.mipro.repository.educationlevelRepository;

@Service
@Transactional
public class educationlevelService {
	
	@Autowired
	private educationlevelRepository fr;
	
	public List<educationlevelModel> semuaeducationlevel() {
		return fr.semuaeducationlevel();
	}

}
