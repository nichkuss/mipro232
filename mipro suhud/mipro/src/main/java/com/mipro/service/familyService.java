package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.dto.keluarga;
import com.mipro.model.familyModel;
import com.mipro.repository.familyRepository;

@Service
@Transactional
public class familyService {

	@Autowired
	private familyRepository fr;
	
	public List<keluarga> semuafamily(Long ids) {
		return fr.semuafamily(ids);
	}
	public List<familyModel> semuafamilyj() {
		return fr.semuafamilyj();
	}

	// save by jpa
	public familyModel simpanfamily(familyModel familyModel) {
		return fr.save(familyModel);
	}

	public familyModel familybyid(Long ids) {
		return fr.familybyid(ids);
	}

	public void hapusfamily(Long ids) {
		fr.deleteflag(ids);

	}
}
