package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.dto.jkeluarga;
import com.mipro.model.familyrelationModel;
import com.mipro.repository.familyrelationRepository;

@Service
@Transactional
public class familyrelationService {
	
	@Autowired
	private familyrelationRepository fr;
	
	public List<familyrelationModel> semuafamilyrelation() {
		return fr.semuafamilyrelation();
	}
	
	public List<familyrelationModel> relationtreetype(Long ids){
		return fr.relationtreetype(ids);
	}

	public List<jkeluarga> relationtreetypeJ(Long ids){
		return fr.relationtreetypeJ(ids);
	}
}
