package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.model.familytreetypeModel;
import com.mipro.repository.familytreetypeRepository;

@Service
@Transactional
public class familytreetypeService {
	
	@Autowired
	private familytreetypeRepository fr;
	
	public List<familytreetypeModel> semuafamilytreetype() {
		return fr.semuafamilytreetype();
	}

}
