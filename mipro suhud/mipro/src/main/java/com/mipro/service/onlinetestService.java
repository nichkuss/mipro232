package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.dto.adb;
import com.mipro.dto.transaksi;
import com.mipro.model.onlinetestModel;
import com.mipro.repository.onlinetestRepository;

@Service
@Transactional
public class onlinetestService {

	@Autowired
	private onlinetestRepository or;

	public List<onlinetestModel> semuaonlinetest(Long ids) {
		return or.semuaonlinetest(ids);
	}
	public List<transaksi> semuaonlinetestJ(Long ids) {
		return or.semuaonlinetestJ(ids);
	}
	
	public onlinetestModel onlinetestbyid(long ids) {
		return or.onlinetestbyid(ids);
	}
	
	public onlinetestModel saveiltesmd(onlinetestModel onlinetestModel) {
		return or.save(onlinetestModel);
	}
	
	public void hapusonlinetest(Long ids) {
		or.deleteflag(ids);

	}
	
	public List<adb> addrbio(Long ids) {
		return or.addrbio(ids);
	}
	
	public Integer getperiodbyid(Long ids) {
		return or.getperiodbyid(ids);
	}
	
	
}
