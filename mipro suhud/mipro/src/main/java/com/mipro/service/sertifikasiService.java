package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.dto.bid;
import com.mipro.model.sertifikasiModel;
import com.mipro.repository.sertifikasiRepository;

@Service
@Transactional
public class sertifikasiService {
	@Autowired
	private sertifikasiRepository sr;

	public List<sertifikasiModel> semuasertifikasi() {
		return sr.semuasertifikasi();
	}

	// save by jpa
	public sertifikasiModel simpansertifikasi(sertifikasiModel sertifikasiModel) {
		return sr.save(sertifikasiModel);
	}

	public sertifikasiModel sertifikasibyid(Long ids) {
		return sr.sertifikasibyid(ids);
	}

	public void hapussertifikasi(Long ids) {
		sr.deleteflag(ids);

	}
	public String ceksertifikasi(String ceksertifikasi) {
		return sr.ceksertifikasi(ceksertifikasi);
	}
	
//	public List<sertifikasiModel> sertifikasibybiodataId(Long ids) {
//		return sr.sertifikasibybiodataid(ids);
//	}
//	
	public List<bid> semuasertifikasiJ(Long ids){
		return sr.semuasertifikasiJ(ids);
	}
	
//	public Long biodataid(Long ids) {
//		return sr.biodataid(ids);
//	}
}
