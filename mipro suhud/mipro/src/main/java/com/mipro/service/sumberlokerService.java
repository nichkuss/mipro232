package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.model.sumberlokerModel;
import com.mipro.repository.sumberlokerRepository;

@Service
@Transactional
public class sumberlokerService {
	@Autowired
	private sumberlokerRepository sr;

	public List<sumberlokerModel> semuasumberloker() {
		return sr.semuasumberloker();
	}

	// save by jpa
	public sumberlokerModel simpansumberloker(sumberlokerModel sumberlokerModel) {
		return sr.save(sumberlokerModel);
	}

	public sumberlokerModel sumberlokerbyid(long ids) {
		return sr.sumberlokerbyid(ids);
	}


	
	
}
