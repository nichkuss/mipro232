package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.dto.dettype;
import com.mipro.model.testdetailModel;
import com.mipro.repository.testdetailRepository;

@Service
@Transactional
public class testdetailService {
	@Autowired
	private testdetailRepository tr;
	
	public List<testdetailModel> semuatestdetail() {
		return tr.semuatestdetail();
	}
	
	public List<dettype> semuatestdetailJ(Long ids) {
		return tr.semuatestdetailJ(ids);
	}

	// save by jpa
	public testdetailModel simpantestdetail(testdetailModel testdetailModel) {
		return tr.save(testdetailModel);
	}
	
	public testdetailModel testdetailbyid(Long ids) {
		return tr.testdetailbyid(ids);
	}

	public void hapustestdetail(Long ids) {
		tr.deleteflag(ids);

	}
}
