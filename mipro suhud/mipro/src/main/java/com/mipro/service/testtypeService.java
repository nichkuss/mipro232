package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.model.testtypeModel;
import com.mipro.repository.testtypeRepository;

@Service
@Transactional
public class testtypeService {
	@Autowired
	private testtypeRepository tr;
	
	public List<testtypeModel> semuatesstype() {
		return tr.semuatesttype();
	}
	// save by jpa
	public testtypeModel simpantesstype(testtypeModel testtypeModel) {
		return tr.save(testtypeModel);
	}

	public testtypeModel tesstypebyid(Long ids) {
		return tr.testtypebyid(ids);
	}

	public void hapustesstype(Long ids) {
		tr.deleteflag(ids);

	}
}
