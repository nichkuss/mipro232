familylist();

function familylist() {
	$.ajax({
		url : '/familylist',
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#familylist').html(result);

		}

	});
}

function addform() {
	$.ajax({
		url : '/familyadd',
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#familyModal').modal();
			$('#nonfamilylist').html(result);
			
		}
	
	});
}

function tutupmodal(){
		  $('#familyModal').modal('hide');
		  $('#delfamilyModal').modal('hide');
}

function closemodal() {
	$('#familyModal').hide();
	$('.modal-backdrop').hide();
	$('.modal-backdrop').remove();
}

function refreshpage(){
	$.ajax({
		url : '/family',
		type : 'get',
		dataType : 'html',
		success : function(result) {
			closemodal();
//			$('#fragment').html(result);
			$('#pelamarmodal').modal({backdrop: 'static', keyboard: false}) 
			$('#pelamarisi').html(result);

		}
	});
	return false;
}