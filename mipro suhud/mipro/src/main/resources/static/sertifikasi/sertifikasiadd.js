//function validasi(){
var thn;
var bln;
function selectfunction() {
	var x = document.getElementById("validStartMonth").value;
	var y = document.getElementById("validStartYear").value;
	var v = document.getElementById("untilMonth").value;
	var w = document.getElementById("untilYear").value;

	if (x != "" && y != "") {
		document.getElementById("untilYear").disabled = false;
		document.getElementById("untilYear").value = w;
		document.getElementById("untilMonth").disabled = false;
		document.getElementById("untilMonth").value = v;
		cekbln();
		cekthn();
	} else {
		document.getElementById("untilYear").disabled = true;
		document.getElementById("untilYear").value = "";
		document.getElementById("untilMonth").disabled = true;
		document.getElementById("untilMonth").value = "";
		$('#resign_year_error').hide();
		document.getElementById("keluar").style.color = "";
		document.getElementById("untilYear").style.borderColor = "";
		// document.getElementById("btn1").disabled = false;
		thn = 1;

		$('#resign_month_error').hide();
		document.getElementById("keluar").style.color = "";
		document.getElementById("untilMonth").style.borderColor = "";
		// document.getElementById("btn1").disabled = false;
		bln = 1;
	}

}

// function cektgl(){
// var errorMsg = "Join year cannot be greater than Resign year";
// var warna = errorMsg.fontcolor("#cc0000");
// var x = document.getElementById("validStartMonth").value;
// var y = document.getElementById("validStartYear").value;
// var v = document.getElementById("untilMonth").value;
// var w = document.getElementById("untilYear").value;
// var cek1;
// var cek2;
// alert(x+" dan "+v);
// if(y>w){
// document.getElementById("untilYear").style.borderColor="red";
// $('#resign_year_error').html(warna);
// $('#resign_year_error').show();
// cek1=0;
//
// } else if (y<=w){
// document.getElementById("untilYear").style.borderColor="";
// $('#resign_year_error').html(warna);
// $('#resign_year_error').hide();
// cek1=1
// }
//	
// if(x>v){
// document.getElementById("untilMonth").style.borderColor="red";
// $('#resign_month_error').html(warna);
// $('#resign_month_error').show();
// cek2=0
// } else if (x<v && y<=w){
// document.getElementById("untilMonth").style.borderColor="";
// $('#resign_month_error').html(warna);
// $('#resign_month_error').hide();
// cek2=1
// }
//	
// }
function cekthn() {
	// $('#untilYear').change(function()
	// {
	var join_year = $('#validStartYear').val();
	var resign_year = $('#untilYear').val();
	var errorMsg = "Until year cannot be greater than Start year";
	var warna = errorMsg.fontcolor("#cc0000");
	var errorResignYear;

	if (resign_year == "") {
		$('#resign_year_error').hide();
		document.getElementById("keluar").style.color = "";
		document.getElementById("untilYear").style.borderColor = "";
		// document.getElementById("btn1").disabled = false;
		cekbln();
		thn = 1;
	} else if (join_year > resign_year) {
		$('#resign_year_error').html(warna);
		$('#resign_year_error').show();
		document.getElementById("keluar").style.color = "#cc0000";
		document.getElementById("untilYear").style.borderColor = "#cc0000";
		// document.getElementById("btn1").disabled = true;
		errorResignYear = true;
		thn = 0;
	} else {
		$('#resign_year_error').hide();
		document.getElementById("keluar").style.color = "";
		document.getElementById("untilYear").style.borderColor = "";
		// document.getElementById("btn1").disabled = false;
		cekbln();
		thn = 1;
	}
	if (thn == 1 && bln == 1
			&& document.getElementById("certificateName").value != ""
			&& document.getElementById("publisher").value != "") {
		document.getElementById("btn1").disabled = false;
	} else {
		document.getElementById("btn1").disabled = true;
	}
	// });
}

function cekbln() {
	// $('#untilMonth').change(function(){
	var join_month = $('#validStartMonth').val();
	var resign_month = $('#untilMonth').val();
	var join_year = $('#validStartYear').val();
	var resign_year = $('#untilYear').val();
	var errorMsg = "Until Month cannot be greater than Start Month";
	var warna = errorMsg.fontcolor("#cc0000");
	var errorResignYear;

	if (resign_month == "") {
		$('#resign_month_error').hide();
		document.getElementById("keluar").style.color = "";
		document.getElementById("untilMonth").style.borderColor = "";
		// document.getElementById("btn1").disabled = false;
		bln = 1;
	}

	else if (join_month > resign_month && join_year > resign_year) {
		$('#resign_month_error').html(warna);
		$('#resign_month_error').show();
		document.getElementById("keluar").style.color = "#cc0000";
		document.getElementById("untilMonth").style.borderColor = "#cc0000";
		// document.getElementById("btn1").disabled = true;
		errorResignYear = true;
		bln = 0;
	} else if (join_year == resign_year && join_month > resign_month) {

		$('#resign_month_error').html(warna);
		$('#resign_month_error').show();
		document.getElementById("keluar").style.color = "#cc0000";
		document.getElementById("untilMonth").style.borderColor = "#cc0000";
		// document.getElementById("btn1").disabled = true;
		errorResignYear = true;
		bln = 0;
	} else {
		$('#resign_month_error').hide();
		document.getElementById("keluar").style.color = "";
		document.getElementById("untilMonth").style.borderColor = "";
		// document.getElementById("btn1").disabled = false;
		bln = 1;
	}

	if (thn == 1 && bln == 1
			&& document.getElementById("certificateName").value != ""
			&& document.getElementById("publisher").value != "") {
		document.getElementById("btn1").disabled = false;
	} else {
		document.getElementById("btn1").disabled = true;
	}
	// });
}

function cek1() {
	var a = document.getElementById("certificateName").value;
	if (a == "") {
		document.getElementById("btn1").disabled = "true";
		document.getElementById("certificateName").style.borderColor = "red";
		document.getElementById("certificateName").placeholder = "Sertifikat Tidak Boleh Kosong";
	} else {
		document.getElementById("certificateName").style.borderColor = "";
		document.getElementById("certificateName").placeholder = "";
		b = 1;
	}
	if (document.getElementById("publisher").value != "" && a != "" && bln == 1
			&& thn == 1) {
		document.getElementById("btn1").disabled = "";
	}

}

function cek2() {
	var b = document.getElementById("publisher").value;
	if (b == "") {
		document.getElementById("btn1").disabled = "true";
		document.getElementById("publisher").style.borderColor = "red";
		document.getElementById("publisher").placeholder = "Penerbit Tidak Boleh Kosong";
	} else {
		document.getElementById("publisher").style.borderColor = "";
		document.getElementById("publisher").placeholder = "";
		c = 1;
	}

	if (document.getElementById("certificateName").value != "" && b != ""
		&& bln == 1 && thn == 1) {
	document.getElementById("btn1").disabled = "";
	}

}

// var data = 0;
function tutupmodal2() {
	$('#sertifikasiModal').modal('hide');
	$('#delsertifikasiModal').modal('hide');
	
}

$('#datanewsertifikasi').submit(function() {
//	if(document.getElementById("untilYear").value==null){
//		document.getElementById("untilYear").disabled=false;
//		document.getElementById("untilYear").value="";
//	}
//	if(document.getElementById("untilMonth").value==null){
//		document.getElementById("untilMonth").disabled=false;
//		document.getElementById("untilMonth").value="";
//	}
	
	
	formdata = $('#datanewsertifikasi').serialize();
	var bid = $('#biodataId').val();
	$.ajax({
		url : '/simpansertifikasi',
		type : 'POST',
		data : formdata,
		success : function(result) {
			refreshstl(bid);
			alert("Simpan sertifikasi berhasil");
			tutupmodal2();
			

		}

	});
	return false; // agar tidak pindah halaman
});

// $(document).ready(function() {
//	
//	
// $('#datanewsertifikasi').bootstrapValidator({
// // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
//
// feedbackIcons: {
// valid: 'glyphicon glyphicon-ok',
// invalid: 'glyphicon glyphicon-remove',
// validating: 'glyphicon glyphicon-refresh'
// },
// fields: {
// certificateName: {
// validators: {
// notEmpty: {
// message: 'The username is required and cannot be empty'
// }
// }
// },
// publisher: {
// validators: {
// notEmpty: {
// message: 'The publisher is required and cannot be empty'
// }
// }
// }
// },
//		
// onSuccess: function(e, data) {
//
//		}
//
//	});    
//});

