var cn = document.getElementById("careerCenterName").value;
var en = document.getElementById("eventName").value;
var rn = document.getElementById("referrerName").value;
var rp = document.getElementById("referrerPhone").value;
var re = document.getElementById("referrerEmail").value;
var os = document.getElementById("otherSource").value;
// $(document).ready(function() {
function cekevent() {
	var en2 = document.getElementById("eventName").value;
	if (en2 == "") {
		document.getElementById("btn1").disabled = "true";
		document.getElementById("eventName").style.borderColor = "red";
		document.getElementById("eventName").placeholder = "Nama Event Tidak Boleh Kosong";
	} else {
		document.getElementById("btn1").disabled = "";
		document.getElementById("eventName").style.borderColor = "";
		document.getElementById("eventName").placeholder = "";
	}
}

function cekcareercenter() {
	var cn2 = document.getElementById("careerCenterName").value;
	if (cn2 == "") {
		document.getElementById("btn1").disabled = "true";
		document.getElementById("careerCenterName").style.borderColor = "red";
		document.getElementById("careerCenterName").placeholder = "Nama Career Center Tidak Boleh Kosong";
	} else {
		document.getElementById("btn1").disabled = "";
		document.getElementById("careerCenterName").style.borderColor = "";
		document.getElementById("careerCenterName").placeholder = "";
	}
}

function cekreferrer() {
	var rn2 = document.getElementById("referrerName").value;
	var rp2 = document.getElementById("referrerPhone").value;
	var re2 = document.getElementById("referrerEmail").value;
	if (rn2 == "") {
		document.getElementById("btn1").disabled = "true";
		document.getElementById("referrerName").style.borderColor = "red";
		document.getElementById("referrerName").placeholder = "Nama Referrer Tidak Boleh Kosong";

	} else {
//		document.getElementById("btn1").disabled = "";
		document.getElementById("referrerName").style.borderColor = "";
		document.getElementById("referrerName").placeholder = "";

	}

	if (rp2 == "") {
		document.getElementById("btn1").disabled = "true";
		document.getElementById("referrerPhone").style.borderColor = "red";
		document.getElementById("referrerPhone").placeholder = "Nomer Telephone Referrer Tidak Boleh Kosong";
	} else {
//		document.getElementById("btn1").disabled = "";
		document.getElementById("referrerPhone").style.borderColor = "";
		document.getElementById("referrerPhone").placeholder = "";
	}

	if (re2 == "") {
		document.getElementById("btn1").disabled = "true";
		document.getElementById("referrerEmail").style.borderColor = "red";
		document.getElementById("referrerEmail").placeholder = "Email Referrer Tidak Boleh Kosong";
	} else {
//		document.getElementById("btn1").disabled = "";
		document.getElementById("referrerEmail").style.borderColor = "";
		document.getElementById("referrerEmail").placeholder = "";
	}
	
	if(re2 != "" && rp2 != "" && rn2 != ""){
		document.getElementById("btn1").disabled = "";
	}
}

function cekothersource() {
	var os2 = document.getElementById("otherSource").value;
	if (os2 == "") {
		document.getElementById("btn1").disabled = "true";
		document.getElementById("otherSource").style.borderColor = "red";
		document.getElementById("otherSource").placeholder = "Nama Event Tidak Boleh Kosong";
	} else {
		document.getElementById("btn1").disabled = "";
		document.getElementById("otherSource").style.borderColor = "";
		document.getElementById("otherSource").placeholder = "";
	}
}

//----------------------------------------------------------
var x = document.getElementById("vacancySource").value;

if (x == "event") {
	document.getElementById("careerCenterName").disabled = true;
	document.getElementById("careerCenterName").value = "";
	document.getElementById("eventName").disabled = false;
	document.getElementById("eventName").value = en;
	document.getElementById("referrerName").disabled = true;
	document.getElementById("referrerName").value = "";
	document.getElementById("referrerPhone").disabled = true;
	document.getElementById("referrerPhone").value = "";
	document.getElementById("referrerEmail").disabled = true;
	document.getElementById("referrerEmail").value = "";
	document.getElementById("otherSource").disabled = true;
	document.getElementById("otherSource").value = "";

} else if (x == "career center") {
	document.getElementById("careerCenterName").disabled = false;
	document.getElementById("careerCenterName").value = cn;
	document.getElementById("eventName").disabled = true;
	document.getElementById("eventName").value = "";
	document.getElementById("referrerName").disabled = true;
	document.getElementById("referrerName").value = "";
	document.getElementById("referrerPhone").disabled = true;
	document.getElementById("referrerPhone").value = "";
	document.getElementById("referrerEmail").disabled = true;
	document.getElementById("referrerEmail").value = "";
	document.getElementById("otherSource").disabled = true;
	document.getElementById("otherSource").value = "";
} else if (x == "reference") {
	document.getElementById("careerCenterName").disabled = true;
	document.getElementById("careerCenterName").value = "";
	document.getElementById("eventName").disabled = true;
	document.getElementById("eventName").value = "";
	document.getElementById("referrerName").disabled = false;
	document.getElementById("referrerName").value = rn;
	document.getElementById("referrerPhone").disabled = false;
	document.getElementById("referrerPhone").value = rp;
	document.getElementById("referrerEmail").disabled = false;
	document.getElementById("referrerEmail").value = re;
	document.getElementById("otherSource").disabled = true;
	document.getElementById("otherSource").value = "";
} else if (x == "other") {
	document.getElementById("careerCenterName").disabled = true;
	document.getElementById("careerCenterName").value = "";
	document.getElementById("eventName").disabled = true;
	document.getElementById("eventName").value = "";
	document.getElementById("referrerName").disabled = true;
	document.getElementById("referrerName").value = "";
	document.getElementById("referrerPhone").disabled = true;
	document.getElementById("referrerPhone").value = "";
	document.getElementById("referrerEmail").disabled = true;
	document.getElementById("referrerEmail").value = "";
	document.getElementById("otherSource").disabled = false;
	document.getElementById("otherSource").value = os;
}

//-------------------------------------------------------------

$("select").change(function() {
	x = document.getElementById("vacancySource").value;
	document.getElementById("btn1").disabled = true;
	if (x == "event") {
		document.getElementById("careerCenterName").disabled = true;
		document.getElementById("careerCenterName").value = "";
		document.getElementById("eventName").disabled = false;
		document.getElementById("eventName").value = en;
		document.getElementById("referrerName").disabled = true;
		document.getElementById("referrerName").value = "";
		document.getElementById("referrerPhone").disabled = true;
		document.getElementById("referrerPhone").value = "";
		document.getElementById("referrerEmail").disabled = true;
		document.getElementById("referrerEmail").value = "";
		document.getElementById("otherSource").disabled = true;
		document.getElementById("otherSource").value = "";

		document.getElementById("btn1").disabled = "true";
		document.getElementById("referrerEmail").style.borderColor = "";
		document.getElementById("referrerEmail").placeholder = "";
		document.getElementById("careerCenterName").style.borderColor = "";
		document.getElementById("careerCenterName").placeholder = "";
		document.getElementById("otherSource").style.borderColor = "";
		document.getElementById("otherSource").placeholder = "";
		document.getElementById("referrerPhone").style.borderColor = "";
		document.getElementById("referrerPhone").placeholder = "";
		document.getElementById("referrerName").style.borderColor = "";
		document.getElementById("referrerName").placeholder = "";

		if (en != "") {
			cekevent();
		} else {
//			document.getElementById("btn1").disabled = "";
			document.getElementById("eventName").style.borderColor = "";
			document.getElementById("eventName").placeholder = "";
		}

	} else if (x == "career center") {
		document.getElementById("careerCenterName").disabled = false;
		document.getElementById("careerCenterName").value = cn;
		document.getElementById("eventName").disabled = true;
		document.getElementById("eventName").value = "";
		document.getElementById("referrerName").disabled = true;
		document.getElementById("referrerName").value = "";
		document.getElementById("referrerPhone").disabled = true;
		document.getElementById("referrerPhone").value = "";
		document.getElementById("referrerEmail").disabled = true;
		document.getElementById("referrerEmail").value = "";
		document.getElementById("otherSource").disabled = true;
		document.getElementById("otherSource").value = "";

		document.getElementById("referrerEmail").style.borderColor = "";
		document.getElementById("referrerEmail").placeholder = "";
		document.getElementById("otherSource").style.borderColor = "";
		document.getElementById("otherSource").placeholder = "";
		document.getElementById("eventName").style.borderColor = "";
		document.getElementById("eventName").placeholder = "";
		document.getElementById("referrerPhone").style.borderColor = "";
		document.getElementById("referrerPhone").placeholder = "";
		document.getElementById("referrerName").style.borderColor = "";
		document.getElementById("referrerName").placeholder = "";

		if (cn != "") {
			cekcareercenter();
		} else {
//			document.getElementById("btn1").disabled = "";
			document.getElementById("careerCenterName").style.borderColor = "";
			document.getElementById("careerCenterName").placeholder = "";
		}
	}

	else if (x == "reference") {
		document.getElementById("careerCenterName").disabled = true;
		document.getElementById("careerCenterName").value = "";
		document.getElementById("eventName").disabled = true;
		document.getElementById("eventName").value = "";
		document.getElementById("referrerName").disabled = false;
		document.getElementById("referrerName").value = rn;
		document.getElementById("referrerPhone").disabled = false;
		document.getElementById("referrerPhone").value = rp;
		document.getElementById("referrerEmail").disabled = false;
		document.getElementById("referrerEmail").value = re;
		document.getElementById("otherSource").disabled = true;
		document.getElementById("otherSource").value = "";

		document.getElementById("careerCenterName").style.borderColor = "";
		document.getElementById("careerCenterName").placeholder = "";
		document.getElementById("eventName").style.borderColor = "";
		document.getElementById("eventName").placeholder = "";
		document.getElementById("otherSource").style.borderColor = "";
		document.getElementById("otherSource").placeholder = "";

		if (rn != "" && rp != "" && re != "") {
			cekreferrer()
		} else {
//			document.getElementById("btn1").disabled = "";
			document.getElementById("referrerPhone").style.borderColor = "";
			document.getElementById("referrerPhone").placeholder = "";
			document.getElementById("referrerName").style.borderColor = "";
			document.getElementById("referrerName").placeholder = "";
			document.getElementById("referrerEmail").style.borderColor = "";
			document.getElementById("referrerEmail").placeholder = "";
		}
	} else if (x == "other") {
		document.getElementById("careerCenterName").disabled = true;
		document.getElementById("careerCenterName").value = "";
		document.getElementById("eventName").disabled = true;
		document.getElementById("eventName").value = "";
		document.getElementById("referrerName").disabled = true;
		document.getElementById("referrerName").value = "";
		document.getElementById("referrerPhone").disabled = true;
		document.getElementById("referrerPhone").value = "";
		document.getElementById("referrerEmail").disabled = true;
		document.getElementById("referrerEmail").value = "";
		document.getElementById("otherSource").disabled = false;
		document.getElementById("otherSource").value = os;

//		document.getElementById("btn1").disabled = "";
		document.getElementById("referrerEmail").style.borderColor = "";
		document.getElementById("referrerEmail").placeholder = "";
		document.getElementById("careerCenterName").style.borderColor = "";
		document.getElementById("careerCenterName").placeholder = "";
		document.getElementById("eventName").style.borderColor = "";
		document.getElementById("eventName").placeholder = "";
		document.getElementById("referrerPhone").style.borderColor = "";
		document.getElementById("referrerPhone").placeholder = "";
		document.getElementById("referrerName").style.borderColor = "";
		document.getElementById("referrerName").placeholder = "";

		if (os != "") {
			cekothersource()
		} else {
//			document.getElementById("btn1").disabled = "";
			document.getElementById("otherSource").style.borderColor = "";
			document.getElementById("otherSource").placeholder = "";
		}

	}
}); // punya .change

// }
//	
//	
$('#dataeditsumberloker').submit(function() {
	formdata = $('#dataeditsumberloker').serialize();
	$.ajax({
		url : '/ubahsumberloker',
		type : 'POST',
		data : formdata,
		success : function(result) {
			alert("Edit Berhasil")

		}

	});
	return false; // agar tidak pindah halaman
});
