package com.mipro.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class pelamarController {
	
	@RequestMapping("pelamar")
	public String pelamar() {
		
		return "pelamar/pelamar";
	}
	
	@RequestMapping("pelamardetail")
	public String pelamardetail() {
		
		return "pelamar/pelamardetail";
	}
}
