package com.mipro.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mipro.dto.joinlevelId;
import com.mipro.model.jenjangpendidikanModel;
import com.mipro.model.pendidikanModel;
import com.mipro.service.jenjangpendidikanService;
import com.mipro.service.pendidikanService;

@Controller
public class pendidikanController {

	@Autowired
	private pendidikanService ps;
	@Autowired
	private jenjangpendidikanService js;
	
	//join
	@RequestMapping("pendidikanlist/{ids}")
	public String pendidikanlist(Model model, @PathVariable(name="ids")Long ids) {
		List<joinlevelId>semuapendidikan=ps.semuapendidikanJ(ids);
		model.addAttribute("pendidikanbyid", ids);
		model.addAttribute("semuapendidikan", semuapendidikan);
		return "pendidikan/pendidikanlist";
	}
	
	@RequestMapping("pendidikan")
	public String pendidikan() {
		
		return "pendidikan/pendidikan";
	}
	
	@RequestMapping("pendidikanadd/{ids}")
	public String pendidikanadd(Model model,@PathVariable(name = "ids")Long ids) {
		List<jenjangpendidikanModel>listjenjang=js.semuajenjangpendidikan();
		model.addAttribute("listjenjang", listjenjang);
		model.addAttribute("ids", ids);
		isitahun(model);
		return "pendidikan/pendidikanadd";
	}
	
	public void isitahun(Model model) {
		Calendar cal = Calendar.getInstance();
		int thn = cal.get(Calendar.YEAR);
		List<Integer> tahunk = new ArrayList();
		model.addAttribute("tahun", tahunk);
		thn = thn - 30;
		for (int i = 0; i <= 30; i++) {
			tahunk.add(thn + i);
		}

		model.addAttribute("tahun", tahunk);
	}
	
	@ResponseBody
	@RequestMapping("simpanpendidikan")
	public String simpanpendidikan(@ModelAttribute()pendidikanModel pendidikanModel) {
		
		Long loguser = (long) 111;
		pendidikanModel.setCreatedBy(loguser);
		Timestamp wktsekarang = new Timestamp(System.currentTimeMillis());
		pendidikanModel.setCreatedOn(wktsekarang);
		ps.simpanpendidikan(pendidikanModel);
		return "";
	}
	
	@ResponseBody
	@RequestMapping("ubahpendidikan")
	public String ubahpendidikan (@ModelAttribute()pendidikanModel pendidikanModel) {
		System.out.println(pendidikanModel.getEducationLevelId());
		System.out.println(pendidikanModel.getBiodataId());
		pendidikanModel pendidikanbyid = ps.pendidikanbyid(pendidikanModel.getId());
		pendidikanModel.setBiodataId(pendidikanbyid.getBiodataId());
		pendidikanModel.setCreatedBy(pendidikanbyid.getCreatedBy());
		pendidikanModel.setCreatedOn(pendidikanbyid.getCreatedOn());
		Long loguser = (long) 111;
		pendidikanModel.setModifiedBy(loguser);
		Timestamp wktedit = new Timestamp(System.currentTimeMillis());
		pendidikanModel.setModifiedOn(wktedit);
		ps.simpanpendidikan(pendidikanModel);
		return "";
	}
	
	@ResponseBody
	@RequestMapping("hapuspendidikan/{ids}")
	public String hapuspendidikan (@PathVariable(name="ids") long ids) {
		pendidikanModel pendidikanbyid = ps.pendidikanbyid(ids);
		long loguser = 111;
		pendidikanbyid.setDeletedBy(new Long(loguser));
		Timestamp wktdelete = new Timestamp(System.currentTimeMillis());
		pendidikanbyid.setDeletedOn(wktdelete);
		ps.simpanpendidikan(pendidikanbyid);
		ps.hapuspendidikan(ids);
		return "";	
	}
	
	@RequestMapping("pendidikanedit/{ids}")
	public String pendidikanedit(Model model,
						@PathVariable(name="ids") Long ids) {
		List<jenjangpendidikanModel>listjenjang=js.semuajenjangpendidikan();
		pendidikanModel pendidikanbyid = ps.pendidikanbyid(ids);
		model.addAttribute("listjenjang", listjenjang);
		model.addAttribute("pendidikanbyid", pendidikanbyid);
		isitahun(model);
		return "pendidikan/pendidikanedit";
	}
	

	@RequestMapping("pendidikandelete/{ids}")
	public String pendidikandelete(Model model,
		@PathVariable(name="ids") Long ids) {
		pendidikanModel pendidikanbyid = ps.pendidikanbyid(ids);
		System.out.println(ids);
		model.addAttribute("pendidikanbyid", pendidikanbyid);
		return "pendidikan/pendidikandelete" ;
	}
}
