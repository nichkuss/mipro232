package com.mipro.controller;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mipro.dto.bid;
import com.mipro.dto.joinket;
import com.mipro.model.keterangantambahanModel;
import com.mipro.model.pendidikanModel;
import com.mipro.model.referensiModel;
import com.mipro.repository.referensiRepository;
import com.mipro.service.keterangantambahanService;
import com.mipro.service.pendidikanService;
import com.mipro.service.referensiService;

@Controller
public class referensiController {

	@Autowired
	private referensiService rs;
	
	@Autowired
	private keterangantambahanService kr;
	
	@RequestMapping("referensi")
	public String referensi() {
		return "referensi/referensi";
	}
	
	@RequestMapping("keterangan")
	public String keterangan() {
		return "keterangan/keterangan";
	}
	
	@RequestMapping("referensilist/{ids}")
	public String referensilist(Model model, 
			@PathVariable(name="ids")Long ids) {
//		List<referensiModel>semuareferensi=rs.semuareferensi();
		List<bid>semuareferensi = rs.semuareferensiJ(ids);
		model.addAttribute("referensibyid", ids);
		model.addAttribute("semuareferensi", semuareferensi);
//		Long ids=(long) 3;
		List<joinket>ktm = kr.semuaketeranganJ(ids);
		model.addAttribute("keterangantambahanbyid", ids);
//		keterangantambahanModel ktm = kr.keterangantambahanbyid(ids);
		model.addAttribute("ktm", ktm);
		return "referensi/referensilist";
	}
	
	@RequestMapping("keterangansemua/{ids}")
	public String keteranganlist(Model model,@PathVariable(name="ids")Long ids) {
//		List<keterangantambahanModel>semuaketerangan=kr.semuaketerangantambahan();
//		model.addAttribute("semuaketerangan", semuaketerangan);
		List<joinket>ktm = kr.semuaketeranganJ(ids);
		model.addAttribute("keterangantambahanbyid", ids);
		model.addAttribute("ktm", ktm);
		return "referensi/keterangansemua";
	}
	
	@RequestMapping("referensiadd/{ids}")
	public String referensiadd(Model model, @PathVariable(name = "ids") Long ids) {
		model.addAttribute("ids", ids);
		return "referensi/referensiadd";
	}
	
	@ResponseBody
	@RequestMapping("simpanreferensi")
	public String simpanreferensi(@ModelAttribute()referensiModel referensiModel) {
		Long loguser = (long) 111;
		referensiModel.setCreatedBy(new Long(loguser));
		Timestamp wktsekarang = new Timestamp(System.currentTimeMillis());
		referensiModel.setCreatedOn(wktsekarang);
		rs.simpanreferensi(referensiModel);
		return "";
	}
	
	@ResponseBody
	@RequestMapping("simpanketerangan")
	public String simpanketerangan(@ModelAttribute()keterangantambahanModel keterangantambahanModel) {
//		String a = "YA";
//		String b = "TIDAK";
//		if(keterangantambahanModel.getIsNegotiable()== null) {
////			keterangantambahanModel keterangantambahanbyid = kr.keterangantambahanbyid(ids);
////			keterangantambahanModel.setIsNegotiable(false);
//			model.addAttribute(attributeName, attributeValue)
//		}
		Long loguser = (long) 111;
		keterangantambahanModel.setCreatedBy(loguser);
		Timestamp wktsekarang = new Timestamp(System.currentTimeMillis());
		keterangantambahanModel.setCreatedOn(wktsekarang);
		kr.simpanketerangantambahan(keterangantambahanModel);
		return "";
	}
	
	@ResponseBody
	@RequestMapping("ubahreferensi")
	public String ubahreferensi (@ModelAttribute()referensiModel referensiModel) {
		Long loguser = (long) 111;
		referensiModel referensibyid = rs.referensibyid(referensiModel.getId());
		referensiModel.setBiodataId(referensibyid.getBiodataId());
		referensiModel.setCreatedBy(referensibyid.getCreatedBy());
		referensiModel.setCreatedOn(referensibyid.getCreatedOn());
		referensiModel.setModifiedBy(loguser);
		Timestamp wktedit = new Timestamp(System.currentTimeMillis());
		referensiModel.setModifiedOn(wktedit);
		rs.simpanreferensi(referensiModel);
		return "";
	}
	
	@ResponseBody
	@RequestMapping("ubahketerangan")
	public String ubahketerangan (@ModelAttribute()keterangantambahanModel keterangantambahanModel) {
		Long loguser = (long) 111;
		keterangantambahanModel keterangantambahanbyid = kr.keterangantambahanbyid(keterangantambahanModel.getId());
		keterangantambahanModel.setBiodataId(keterangantambahanbyid.getBiodataId());
		keterangantambahanModel.setCreatedBy(keterangantambahanbyid.getCreatedBy());
		keterangantambahanModel.setCreatedOn(keterangantambahanbyid.getCreatedOn());
		keterangantambahanModel.setModifiedBy(loguser);
		Timestamp wktedit = new Timestamp(System.currentTimeMillis());
		keterangantambahanModel.setModifiedOn(wktedit);
		kr.simpanketerangantambahan(keterangantambahanModel);
		return "";
	}
	
	@ResponseBody
	@RequestMapping("hapusreferensi/{ids}")
	public String hapusreferensi (@PathVariable(name="ids") Long ids) {
		referensiModel referensibyid = rs.referensibyid(ids);
		Long loguser = (long) 111;
		referensibyid.setDeletedBy(loguser);
		Timestamp wktdelete = new Timestamp(System.currentTimeMillis());
		referensibyid.setDeletedOn(wktdelete);
		rs.simpanreferensi(referensibyid);
		rs.hapusreferensi(ids);
		return "";
	}
	
	@RequestMapping("referensiedit/{ids}")
	public String referensiedit(Model model,
						@PathVariable(name="ids") Long ids) {
		referensiModel referensibyid = rs.referensibyid(ids);
		model.addAttribute("referensibyid", referensibyid);
		return "referensi/referensiedit";
	}
	
	@RequestMapping("keteranganedit/{ids}")
	public String keteranganedit(Model model,
						@PathVariable(name="ids") Long ids) {
		keterangantambahanModel keterangantambahanbyid = kr.keterangantambahanbyid(ids);
		model.addAttribute("keterangantambahanbyid", keterangantambahanbyid);
		return "referensi/keteranganedit";
	}

	@RequestMapping("referensidelete/{ids}")
	public String referensidelete(Model model,
						@PathVariable(name="ids") Long ids) {
		referensiModel referensibyid = rs.referensibyid(ids);
		model.addAttribute("referensibyid", referensibyid);
		return "referensi/referensidelete" ;
	}
	
}
