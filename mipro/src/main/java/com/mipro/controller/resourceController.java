package com.mipro.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mipro.dto.projectList;
import com.mipro.model.clientModel;
import com.mipro.model.resourceModel;
import com.mipro.service.clientService;
import com.mipro.service.resourceService;

@Controller
public class resourceController {
	
	@Autowired
	private resourceService rs;
	
	@Autowired
	private clientService cs;
	
	@RequestMapping("resourcelist")
	public String resourcelist(Model model) {
		List<projectList>semuaresource=rs.semuaresourceJ();
		model.addAttribute("semuaresource", semuaresource);
		return "resource/resourcelist";
	}
	
	@RequestMapping("resourcelist/{start}/{end}")
	public String resourcelist(Model model,@PathVariable(name = "start", required=false)String start,@PathVariable(name = "end",required=false)String end) {
		System.out.println(start);
		Date date1 = null;
		try {
			date1 = new SimpleDateFormat("MMMM dd, yyyy").parse(start);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
		
		Date date2 = null;
		try {
			date2 = new SimpleDateFormat("MMMM dd, yyyy").parse(end);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
		List<projectList>semuaresource=rs.semuaresourceJsrc(date1,date2);
		model.addAttribute("semuaresource", semuaresource);
		return "resource/resourcelist";
	}
	
	@RequestMapping("resource")
	public String resource() {
		return "resource/resource";
	}
	
	@RequestMapping("resourceadd")
	public String resourceadd(Model model) {
		List<clientModel>semuaclient=cs.semuaclient();
		model.addAttribute("semuaclient", semuaclient);
		return "resource/resourceadd";
	}
	
	@ResponseBody
	@RequestMapping("simpanresource")
	public String simpanresource(@ModelAttribute()resourceModel resourceModel) {
		long loguser=232;
		//created by
		resourceModel.setCreatedBy(new Long (loguser));
		//created on
		resourceModel.setCreatedOn(new Date());
		rs.simpanresource(resourceModel);
		return "";
	}
	
	@ResponseBody
	@RequestMapping("ubahresource")
	public String ubahresource(@ModelAttribute()resourceModel resourceModel) {
		long loguser=232;
		//set create
		resourceModel resourcebyid = rs.resourcebyid(resourceModel.getId());
		resourceModel.setCreatedBy(resourcebyid.getCreatedBy());
		resourceModel.setCreatedOn(resourcebyid.getCreatedOn());
		//update by
		resourceModel.setModifiedBy(new Long (loguser));
		//update on
		resourceModel.setModifiedOn(new Date());
		rs.simpanresource(resourceModel);
		return "";
	}
	
//	@ResponseBody
//	@RequestMapping("detailresource")
//	public String detailresource(@ModelAttribute()resourceModel resourceModel) {
//		resourceModel resourcebyid = rs.resourcebyid(resourceModel.getId());
//		
//		return "";
//	}
	
	@ResponseBody
	@RequestMapping("hapusresource/{ids}")
	public String hapusresource(@ModelAttribute()resourceModel resourceModel,
			@PathVariable(name = "ids")long ids) {
		resourceModel resourcebyid = rs.resourcebyid(ids);
		long loguser=232;
		resourcebyid.setDeleteBy(new Long(loguser));
		resourcebyid.setDeleteOn(new Date());
		rs.simpanresource(resourcebyid);
		rs.hapusresource(ids);
		return "";
	}
	
	@RequestMapping("resourcedetail/{ids}")
	public String detail(Model model, @PathVariable(name="ids")Long ids) {
		projectList detailresource = rs.resourceJ(ids);
		model.addAttribute("detailresource", detailresource);
		return "resource/resourcedetail";
	}
	
	@RequestMapping("resourceedit/{ids}")
	public String resourceedit(Model model,
			@PathVariable(name="ids")long ids) {
		List<clientModel>semuaclient=cs.semuaclient();
		model.addAttribute("semuaclient", semuaclient);
		resourceModel resourcebyid=rs.resourcebyid(ids);
		model.addAttribute("resourcebyid", resourcebyid);
		return "resource/resourceedit";
	}
	
	@RequestMapping("resourcedelete/{ids}")
	public String resourcedelete(Model model,
			@PathVariable(name="ids")long ids) {
		resourceModel resourcebyid=rs.resourcebyid(ids);
//		projectList projectbyid=rs.projectbyid(ids);
		model.addAttribute("resourcebyid", resourcebyid);
//		model.addAttribute("projectbyid", projectbyid);
		return "resource/resourcedelete";
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
	    CustomDateEditor editor = new CustomDateEditor(new SimpleDateFormat("MMMM dd, yyyy"), true);
	    binder.registerCustomEditor(Date.class, editor);
	}


}