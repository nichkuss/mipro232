package com.mipro.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class selectaccessController {
	
	@RequestMapping("/")
	public String selectaccess( HttpServletRequest request,Model model) {
		//pura puranya sudah login, dapet id terus di query dapatlah nama 'Mugiwara'
		String usernama="mugiwara D";
		request.getSession().setAttribute("loguser", usernama);
		model.addAttribute("user", usernama);
		return "selectaccess/selectaccess";
	}
	
	@RequestMapping("/selectaccess_pilih")
	public String selectaccess_pilih() {
		return "selectaccess/selectaccess_pilih";
	}
	
	@RequestMapping("/sa/{sa}")
	public String sa(@PathVariable(name = "sa")String sa,Model model,HttpServletRequest request) {
		request.getSession().setAttribute("logsa", sa);
	
		return "redirect:/awal";
	}
}
