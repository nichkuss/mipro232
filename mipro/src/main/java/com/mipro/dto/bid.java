package com.mipro.dto;

public class bid {

	private Long id;
	private Long biodataId;
	private String name;
	private String position;
	private String addressPhone;
	private String relation;
	public bid(Long id, Long biodataId, String name, String position, String addressPhone, String relation) {
		super();
		this.id = id;
		this.biodataId = biodataId;
		this.name = name;
		this.position = position;
		this.addressPhone = addressPhone;
		this.relation = relation;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getBiodataId() {
		return biodataId;
	}
	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getAddressPhone() {
		return addressPhone;
	}
	public void setAddressPhone(String addressPhone) {
		this.addressPhone = addressPhone;
	}
	public String getRelation() {
		return relation;
	}
	public void setRelation(String relation) {
		this.relation = relation;
	}
}






