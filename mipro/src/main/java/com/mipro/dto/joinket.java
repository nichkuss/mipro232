package com.mipro.dto;

public class joinket {

	private Long id;
	private Long biodataId;
	private String emergencyContactName;
	private String emergencyContactPhone;
	private String expectedSalary;
	private Boolean isNegotiable;
	private String startWorking;
	private Boolean isApplyOtherPlace;
	private Boolean isReadyToOutoftown;
	private String applyPlace;
	private String selectionPhase;
	private Boolean isEverBadlySick;
	private String diseaseName;
	private String diseaseTime;
	private Boolean isEverPsychotest;
	private String psychotestNeeds;
	private String psychotestTime;
	private String requirementesRequired;
	private String otherNotesotherNotes;
	public joinket(Long id, Long biodataId, String emergencyContactName, String emergencyContactPhone,
			String expectedSalary, Boolean isNegotiable, String startWorking, Boolean isApplyOtherPlace,
			Boolean isReadyToOutoftown, String applyPlace, String selectionPhase, Boolean isEverBadlySick,
			String diseaseName, String diseaseTime, Boolean isEverPsychotest, String psychotestNeeds,
			String psychotestTime, String requirementesRequired, String otherNotesotherNotes) {
		this.id = id;
		this.biodataId = biodataId;
		this.emergencyContactName = emergencyContactName;
		this.emergencyContactPhone = emergencyContactPhone;
		this.expectedSalary = expectedSalary;
		this.isNegotiable = isNegotiable;
		this.startWorking = startWorking;
		this.isApplyOtherPlace = isApplyOtherPlace;
		this.isReadyToOutoftown = isReadyToOutoftown;
		this.applyPlace = applyPlace;
		this.selectionPhase = selectionPhase;
		this.isEverBadlySick = isEverBadlySick;
		this.diseaseName = diseaseName;
		this.diseaseTime = diseaseTime;
		this.isEverPsychotest = isEverPsychotest;
		this.psychotestNeeds = psychotestNeeds;
		this.psychotestTime = psychotestTime;
		this.requirementesRequired = requirementesRequired;
		this.otherNotesotherNotes = otherNotesotherNotes;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getBiodataId() {
		return biodataId;
	}
	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}
	public String getEmergencyContactName() {
		return emergencyContactName;
	}
	public void setEmergencyContactName(String emergencyContactName) {
		this.emergencyContactName = emergencyContactName;
	}
	public String getEmergencyContactPhone() {
		return emergencyContactPhone;
	}
	public void setEmergencyContactPhone(String emergencyContactPhone) {
		this.emergencyContactPhone = emergencyContactPhone;
	}
	public String getExpectedSalary() {
		return expectedSalary;
	}
	public void setExpectedSalary(String expectedSalary) {
		this.expectedSalary = expectedSalary;
	}
	public Boolean getIsNegotiable() {
		return isNegotiable;
	}
	public void setIsNegotiable(Boolean isNegotiable) {
		this.isNegotiable = isNegotiable;
	}
	public String getStartWorking() {
		return startWorking;
	}
	public void setStartWorking(String startWorking) {
		this.startWorking = startWorking;
	}
	public Boolean getIsApplyOtherPlace() {
		return isApplyOtherPlace;
	}
	public void setIsApplyOtherPlace(Boolean isApplyOtherPlace) {
		this.isApplyOtherPlace = isApplyOtherPlace;
	}
	public Boolean getIsReadyToOutoftown() {
		return isReadyToOutoftown;
	}
	public void setIsReadyToOutoftown(Boolean isReadyToOutoftown) {
		this.isReadyToOutoftown = isReadyToOutoftown;
	}
	public String getApplyPlace() {
		return applyPlace;
	}
	public void setApplyPlace(String applyPlace) {
		this.applyPlace = applyPlace;
	}
	public String getSelectionPhase() {
		return selectionPhase;
	}
	public void setSelectionPhase(String selectionPhase) {
		this.selectionPhase = selectionPhase;
	}
	public Boolean getIsEverBadlySick() {
		return isEverBadlySick;
	}
	public void setIsEverBadlySick(Boolean isEverBadlySick) {
		this.isEverBadlySick = isEverBadlySick;
	}
	public String getDiseaseName() {
		return diseaseName;
	}
	public void setDiseaseName(String diseaseName) {
		this.diseaseName = diseaseName;
	}
	public String getDiseaseTime() {
		return diseaseTime;
	}
	public void setDiseaseTime(String diseaseTime) {
		this.diseaseTime = diseaseTime;
	}
	public Boolean getIsEverPsychotest() {
		return isEverPsychotest;
	}
	public void setIsEverPsychotest(Boolean isEverPsychotest) {
		this.isEverPsychotest = isEverPsychotest;
	}
	public String getPsychotestNeeds() {
		return psychotestNeeds;
	}
	public void setPsychotestNeeds(String psychotestNeeds) {
		this.psychotestNeeds = psychotestNeeds;
	}
	public String getPsychotestTime() {
		return psychotestTime;
	}
	public void setPsychotestTime(String psychotestTime) {
		this.psychotestTime = psychotestTime;
	}
	public String getRequirementesRequired() {
		return requirementesRequired;
	}
	public void setRequirementesRequired(String requirementesRequired) {
		this.requirementesRequired = requirementesRequired;
	}
	public String getOtherNotesotherNotes() {
		return otherNotesotherNotes;
	}
	public void setOtherNotesotherNotes(String otherNotesotherNotes) {
		this.otherNotesotherNotes = otherNotesotherNotes;
	}
	
	
	
}
