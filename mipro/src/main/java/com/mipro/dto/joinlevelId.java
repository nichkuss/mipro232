package com.mipro.dto;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class joinlevelId {
	
	private long id;
	private long biodataId;
	private String schoolname;
	private String name;
	private String entryYear;
	private String graduationYear;
	private String major;
	private Double gpa;
	public joinlevelId(long id, long biodataId, String schoolname, String name, String entryYear, String graduationYear,
			String major, Double gpa) {
		this.id = id;
		this.biodataId = biodataId;
		this.schoolname = schoolname;
		this.name = name;
		this.entryYear = entryYear;
		this.graduationYear = graduationYear;
		this.major = major;
		this.gpa = gpa;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getBiodataId() {
		return biodataId;
	}
	public void setBiodataId(long biodataId) {
		this.biodataId = biodataId;
	}
	public String getSchoolname() {
		return schoolname;
	}
	public void setSchoolname(String schoolname) {
		this.schoolname = schoolname;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEntryYear() {
		return entryYear;
	}
	public void setEntryYear(String entryYear) {
		this.entryYear = entryYear;
	}
	public String getGraduationYear() {
		return graduationYear;
	}
	public void setGraduationYear(String graduationYear) {
		this.graduationYear = graduationYear;
	}
	public String getMajor() {
		return major;
	}
	public void setMajor(String major) {
		this.major = major;
	}
	public Double getGpa() {
		return gpa;
	}
	public void setGpa(Double gpa) {
		this.gpa = gpa;
	}
	
	
		
}

