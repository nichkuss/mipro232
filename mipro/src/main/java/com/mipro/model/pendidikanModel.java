package com.mipro.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "x_pendidikan")
public class pendidikanModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", length=11, nullable=false)
	private Long id;
	
	@Column(name = "biodata_id", length=11, nullable=false)
	private Long biodataId;
	
	@Column(name = "school_name", length=100, nullable=true)
	private String schoolname;
	
	@Column(name = "education_level_id", length=11, nullable=true)
	private Long educationLevelId;
	
	@Column(name = "entry_year", length=10, nullable=true)
	private String entryYear;
	
	@Column(name = "major", length=100, nullable=true)
	private String major;
	
	@Column(name = "gpa")
	private Double gpa;
	
	@Column(name = "city", length=50, nullable=true)
	private String city;
	
	@Column(name = "country", length=50, nullable=true)
	private String country;

	@Column(name = "graduation_year", length=10, nullable=true)
	private String graduationYear;
	
	@Column(name = "notes", length=1000, nullable=true)
	private String notes;
	
	@Column(name = "created_by", length=11, nullable=false)
	private Long createdBy;
	
	@Column(name = "created_on",  nullable=false)
	private Date createdOn;
	
	@Column(name = "modified_by", length=11, nullable=true)
	private Long modifiedBy;
	
	@Column(name = "modified_on", nullable=true)
	private Date modifiedOn;
	
	@Column(name = "deleted_by", length=11, nullable=true)
	private Long deletedBy;
	
	@Column(name = "deleted_on",  nullable=true)
	private Date deletedOn;
	
	@Column(name = "is_delete", nullable=false, columnDefinition = "boolean default false")
	private boolean isDelete;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getBiodataId() {
		return biodataId;
	}

	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}

	public String getSchoolname() {
		return schoolname;
	}

	public void setSchoolname(String schoolname) {
		this.schoolname = schoolname;
	}

	public Long getEducationLevelId() {
		return educationLevelId;
	}

	public void setEducationLevelId(Long educationLevelId) {
		this.educationLevelId = educationLevelId;
	}

	public String getEntryYear() {
		return entryYear;
	}

	public void setEntryYear(String entryYear) {
		this.entryYear = entryYear;
	}

	public String getMajor() {
		return major;
	}

	public void setMajor(String major) {
		this.major = major;
	}

	public Double getGpa() {
		return gpa;
	}

	public void setGpa(Double gpa) {
		this.gpa = gpa;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getGraduationYear() {
		return graduationYear;
	}

	public void setGraduationYear(String graduationYear) {
		this.graduationYear = graduationYear;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	
	
}
