package com.mipro.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "x_timesheet")
public class timesheetModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", length=11, nullable=false)
	private Long id;
	
	@Column(name = "status", length=15, nullable=false)
	private String status;
	
	@Column(name = "placement_id", length=11, nullable=false)
	private Long placementId;
	
	@Column(name = "timesheet_date", nullable=false)
	private Date timesheetDate;
	
	@Column(name = "start", length=5, nullable=false)
	private String start;
	
	@Column(name="end", length=5, nullable=false)
	private String end;
	
	@Column(name = "overtime", nullable=true)
	private Boolean overtime;
	
	@Column(name = "start_ot", length=5, nullable=true)
	private String startOt;
	
	@Column(name = "end_ot", length=5, nullable=true)
	private String endOt;
	
	@Column(name = "activity", length=255, nullable=false)
	private String activity;
	
	@Column(name = "user_approval", length=50, nullable=true)
	private String userApproval;
	
	@Column(name = "submitted_on", nullable=false)
	private Date submittedOn;
	
	@Column(name = "approved_on", nullable=true)
	private Date approvedOn;

	@Column(name = "ero_status", length=50, nullable=true)
	private String eroStatus;
	
	@Column(name = "sent_on", nullable=true)
	private Date sentOn;
	
	@Column(name = "collected_on", nullable=true)
	private Date collectedOn;
	
	@Column(name = "created_by", length=11, nullable=false)
	private Long createdBy;
	
	@Column(name = "created_on",  nullable=false)
	private Date createdOn;
	
	@Column(name = "modified_by", length=11, nullable=true)
	private Long modifiedBy;
	
	@Column(name = "modified_on", nullable=true)
	private Date modifiedOn;
	
	@Column(name = "deleted_by", length=11, nullable=true)
	private Long deletedBy;
	
	@Column(name = "deleted_on",  nullable=true)
	private Date deletedOn;
	
	@Column(name = "is_delete", nullable=false, columnDefinition = "boolean default false")
	private Boolean isDelete;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getPlacementId() {
		return placementId;
	}

	public void setPlacementId(Long placementId) {
		this.placementId = placementId;
	}

	public Date getTimesheetDate() {
		return timesheetDate;
	}

	public void setTimesheetDate(Date timesheetDate) {
		this.timesheetDate = timesheetDate;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public Boolean getOvertime() {
		return overtime;
	}

	public void setOvertime(Boolean overtime) {
		this.overtime = overtime;
	}

	public String getStartOt() {
		return startOt;
	}

	public void setStartOt(String startOt) {
		this.startOt = startOt;
	}

	public String getEndOt() {
		return endOt;
	}

	public void setEndOt(String endOt) {
		this.endOt = endOt;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public String getUserApproval() {
		return userApproval;
	}

	public void setUserApproval(String userApproval) {
		this.userApproval = userApproval;
	}

	public Date getSubmittedOn() {
		return submittedOn;
	}

	public void setSubmittedOn(Date submittedOn) {
		this.submittedOn = submittedOn;
	}

	public Date getApprovedOn() {
		return approvedOn;
	}

	public void setApprovedOn(Date approvedOn) {
		this.approvedOn = approvedOn;
	}

	public String getEroStatus() {
		return eroStatus;
	}

	public void setEroStatus(String eroStatus) {
		this.eroStatus = eroStatus;
	}

	public Date getSentOn() {
		return sentOn;
	}

	public void setSentOn(Date sentOn) {
		this.sentOn = sentOn;
	}

	public Date getCollectedOn() {
		return collectedOn;
	}

	public void setCollectedOn(Date collectedOn) {
		this.collectedOn = collectedOn;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	
	
}
