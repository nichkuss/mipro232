package com.mipro.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "x_timesheet_assessment")
public class timesheetassesmentModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", length=11, nullable=false)
	private Long id;
	
	@Column(name = "year", nullable=false)
	private Integer year;
	
	@Column(name = "month", nullable=false)
	private Integer month;
	
	@Column(name = "placement_id", length=11, nullable=false)
	private Long placementId;
	
	@Column(name = "target_result", nullable=false)
	private Integer targetResult;
	
	@Column(name = "competency", nullable=false)
	private Integer competencyCompetency;
	
	@Column(name = "discipline", nullable=false)
	private Integer discipline;
	
	@Column(name = "created_by", length=11, nullable=false)
	private Long createdBy;
	
	@Column(name = "created_on",  nullable=false)
	private Date createdOn;
	
	@Column(name = "modified_by", length=11, nullable=true)
	private Long modifiedBy;
	
	@Column(name = "modified_on", nullable=true)
	private Date modifiedOn;
	
	@Column(name = "deleted_by", length=11, nullable=true)
	private Long deletedBy;
	
	@Column(name = "deleted_on",  nullable=true)
	private Date deletedOn;
	
	@Column(name = "is_delete", nullable=false, columnDefinition = "boolean default false")
	private Boolean isDelete;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public Long getPlacementId() {
		return placementId;
	}

	public void setPlacementId(Long placementId) {
		this.placementId = placementId;
	}

	public Integer getTargetResult() {
		return targetResult;
	}

	public void setTargetResult(Integer targetResult) {
		this.targetResult = targetResult;
	}

	public Integer getCompetencyCompetency() {
		return competencyCompetency;
	}

	public void setCompetencyCompetency(Integer competencyCompetency) {
		this.competencyCompetency = competencyCompetency;
	}

	public Integer getDiscipline() {
		return discipline;
	}

	public void setDiscipline(Integer discipline) {
		this.discipline = discipline;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	
	
}
