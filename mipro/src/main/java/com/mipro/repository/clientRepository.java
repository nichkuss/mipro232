package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mipro.model.clientModel;
import com.mipro.model.jenjangpendidikanModel;

public interface clientRepository extends JpaRepository<clientModel, Long> {

	@Query("select j from clientModel j where j.isDelete=false")
	List<clientModel>semuaclient();
}
