package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mipro.model.jenjangpendidikanModel;
import com.mipro.model.pendidikanModel;

public interface jenjangpendidikanRepository extends JpaRepository<jenjangpendidikanModel, Long>{

	@Query("select j from jenjangpendidikanModel j where j.isDelete=false")
	List<jenjangpendidikanModel>semuajenjangpendidikan();
}
