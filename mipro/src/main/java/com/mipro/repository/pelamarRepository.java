package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mipro.model.pelamarModel;

public interface pelamarRepository extends JpaRepository<pelamarModel, Long> {

	@Query("select p from pelamarModel p order by p.id ") 
	List<pelamarModel>semuapelamar();

	@Query("select s from pelamarModel s where s.id=?1")  // k  terakhir = singakatan
	pelamarModel pelamarbyid(long ids);
}
