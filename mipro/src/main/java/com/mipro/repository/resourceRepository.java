package com.mipro.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.mipro.dto.projectList;
import com.mipro.model.resourceModel;

public interface resourceRepository extends JpaRepository<resourceModel, Long> {
	@Query("select r from resourceModel r where r.isDelete=false order by r.id")
	List<resourceModel>semuaresource();
	
	@Query("select new com.mipro.dto.projectList (r.id, r.clientId, c.name, r.location,"
			+ "r.department, r.picName, r.projectName, r.startProject, r.endProject,"
			+ "r.projectRole, r.projectPhase, r.projectDescription, r.projectTechnology,"
			+ "r.mainTask) from resourceModel r INNER join clientModel c on r.clientId = c.id where r.isDelete=false")
	List<projectList>semuaresourceJ();
	
	
	@Query("select new com.mipro.dto.projectList (r.id, r.clientId, c.name, r.location,"
			+ "r.department, r.picName, r.projectName, r.startProject, r.endProject,"
			+ "r.projectRole, r.projectPhase, r.projectDescription, r.projectTechnology,"
			+ "r.mainTask) from resourceModel r INNER join clientModel c on r.clientId = c.id where r.isDelete=false and"
			+ "(r.startProject BETWEEN ?1 and ?2)"
			+ "or (r.endProject BETWEEN ?1 and ?2)")
	List<projectList>semuaresourceJsrc(Date start,Date end);
	
	@Query("select new com.mipro.dto.projectList (r.id, r.clientId, c.name, r.location,"
			+ "r.department, r.picName, r.projectName, r.startProject, r.endProject,"
			+ "r.projectRole, r.projectPhase, r.projectDescription, r.projectTechnology,"
			+ "r.mainTask) from resourceModel r INNER join clientModel c on r.clientId = c.id where r.isDelete=false and r.id=?1 ")
	projectList resourceJ(Long ids);
	
	@Query("select r from resourceModel r where r.id=?1") //si resourceModel disingkat menjadi "r"
	resourceModel resourcebyid(long ids);
	
	@Query("select r from resourceModel r where r.clientId=?1")
	Long clientid(Long ids);
	
//	@Query("select p from projectList p where p.id=?1") //si projectList disingkat menjadi "p"
//	projectList projectbyid(long ids);
	
	@Modifying
	@Query("update resourceModel set isDelete = true where id=?1")
	long deleteflag(long ids);
	
}
