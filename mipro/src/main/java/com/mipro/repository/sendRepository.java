package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.mipro.dto.joinsend;
import com.mipro.model.timesheetModel;

public interface sendRepository extends JpaRepository <timesheetModel, Long> {

	@Query("select new com.mipro.dto.joinsend(t.status, c.name, t.timesheetDate)"
			+ "from timesheetModel t "
			+ "INNER JOIN placementModel p on t.placementId=p.id "
			+ "INNER JOIN clientModel c on p.clientId=c.id "
			+ "INNER JOIN employeeModel e on p.employeeId=e.id  "
			+ "where t.isDelete=false and t.userApproval = 'approved' and "
			+ "(EXTRACT(year from t.timesheetDate)=?1 AND EXTRACT(MONTH from t.timesheetDate)=?2)")
	List<joinsend>semuatimesheetJ(int year,int month);
	
	
	@Modifying
	@Query(value = "UPDATE x_timesheet t "
			+ " SET modified_by=1, "
			+ " modified_on=now(), "
			+ " ero_status='Send', "
			+ " submitted_on=now()	"
			+ " FROM x_placement p "	
					+ " JOIN x_client c "
					+ " ON c.id = p.client_id "
					+ " JOIN x_employee e"
					+ " ON e.id = p.employee_id"
			+ " WHERE p.id = t.placement_id AND t.is_delete = false"
			+ " AND (extract(month FROM t.timesheet_date) = ?1 "
			+ " AND extract(year FROM t.timesheet_date) = ?2) AND t.user_approval = 'approved' AND t.ero_status IS NULL ", 
		    nativeQuery = true)
	int submit(Integer Bulan, Integer tahun);
	
	@Query("select distinct new com.mipro.dto.joinsend(e.eroEmail)"
			+ "from timesheetModel t "
			+ "INNER JOIN placementModel p on t.placementId=p.id "
			+ "INNER JOIN clientModel c on p.clientId=c.id "
			+ "INNER JOIN employeeModel e on p.employeeId=e.id  "
			+ "where t.isDelete=false and t.userApproval = 'approved' and "
			+ "(EXTRACT(year from t.timesheetDate)=?1 AND EXTRACT(MONTH from t.timesheetDate)=?2)")
	joinsend semuatimesheetJ1(int year,int month);
	
//	@Query(value = "select DISTINCT c.name "
//			+ "from x_timesheet t "
//			+ "INNER JOIN x_placement p on t.placement_id=p.id "
//			+ "INNER JOIN x_client c on p.client_id=c.id "
//			+ "INNER JOIN x_employee e on p.employee_id=e.id "
//			+ "where t.is_delete=false and t.user_approval = 'approved' and "
//			+ "(EXTRACT(year from t.timesheet_date)=?1 AND EXTRACT(MONTH from t.timesheet_date)=?2)", nativeQuery = true)
//	joinsend semuatimesheetJ2(int year,int month);

	
}
