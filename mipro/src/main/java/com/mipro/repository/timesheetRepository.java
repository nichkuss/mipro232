package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.mipro.dto.joinlevelId;
import com.mipro.dto.joints;
import com.mipro.model.pendidikanModel;
import com.mipro.model.timesheetModel;
import com.mipro.model.timesheetassesmentModel;

public interface timesheetRepository extends JpaRepository<timesheetModel, Long> {
	
//	@Query("select j from timesheetModel j where j.isDelete=false")
//	List<timesheetModel>semuatimesheet();
	
	@Query("select new com.mipro.dto.joints(b.id, b.status, b.timesheetDate, b.start, b.end,"
			+ " b.overtime, b.startOt, b.endOt, b.activity, c.name, b.placementId) "
			+ "from timesheetModel b "
			+ "INNER JOIN placementModel d on b.placementId = d.id "
			+ "INNER JOIN clientModel c on c.id = d.clientId "
			+ "where b.isDelete= false AND EXTRACT(year from b.timesheetDate)=?1 AND EXTRACT(MONTH from b.timesheetDate)=?2 "
			+ "AND b.userApproval='submitted' ORDER BY b.timesheetDate ")
	List<joints>semuatimesheet(int year,int month);
	
	
	//setujui
	@Modifying
	@Query(value = "UPDATE x_timesheet t "
			+ " SET modified_by=1, "
			+ " modified_on=now(), "
			+ " user_approval='approved', "
			+ " approved_on=now()	"
			+ " FROM x_placement p "	
					+ " JOIN x_client c "
					+ " ON c.id = p.client_id "
					+ " JOIN x_employee e"
					+ " ON e.id = p.employee_id"
			+ " WHERE p.id = t.placement_id AND t.is_delete = false AND t.id=?1"
			+ " AND (extract(month FROM t.timesheet_date) = ?2 "
			+ " AND extract(year FROM t.timesheet_date) = ?3) ", 
		    nativeQuery = true)
	int submit(Long id, Integer Bulan, Integer tahun);
	
	
	//tolak
	@Modifying
	@Query(value = "UPDATE x_timesheet t "
			+ " SET modified_by=1, "
			+ " modified_on=now(), "
			+ " user_approval='rejected', "
			+ " approved_on=now()	"
			+ " FROM x_placement p "	
					+ " JOIN x_client c "
					+ " ON c.id = p.client_id "
					+ " JOIN x_employee e"
					+ " ON e.id = p.employee_id"
			+ " WHERE p.id = t.placement_id AND t.is_delete = false AND t.id=?1"
			+ " AND (extract(month FROM t.timesheet_date) = ?2 "
			+ " AND extract(year FROM t.timesheet_date) = ?3) ", 
		    nativeQuery = true)
	int tolak(Long id, Integer Bulan, Integer tahun);
	
	@Query("select new com.mipro.dto.joints(b.id, b.status, b.timesheetDate, b.start, b.end, b.overtime, b.startOt , b.endOt, b.activity, "
			+ "c.name, b.placementId) from timesheetModel b INNER JOIN placementModel d on b.placementId = d.id INNER JOIN clientModel c on c.id = d.clientId "
			+ "where b.isDelete= false and b.id=?1")
	joints semuatimesheetassesmentJ1(Long ids);
	
}
