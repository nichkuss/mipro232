package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.mipro.dto.joints;
import com.mipro.model.jenjangpendidikanModel;
import com.mipro.model.timesheetModel;
import com.mipro.model.timesheetassesmentModel;

public interface timesheetassesmentRepository extends JpaRepository<timesheetassesmentModel, Long> {

	@Query("select a from timesheetassesmentModel a where a.isDelete=false order by a.id")
	List<timesheetassesmentModel>semuatimesheetassesment();
	
	
//	@Query("select new com.mipro.dto.joints(b.id, b.status, b.timesheetDate, b.start, b.end,"
//			+ " b.overtime, b.startOt, b.endOt, b.activity, c.name, b.placementId) "
//			+ "from timesheetModel b "
//			+ "INNER JOIN placementModel d on b.placementId = d.id "
//			+ "INNER JOIN clientModel c on c.id = d.clientId "
//			+ "where b.isDelete= false AND EXTRACT(year from b.timesheetDate)=?1 AND EXTRACT(MONTH from b.timesheetDate)=?2 "
//			+ "AND b.userApproval='submitted'")
//	List<joints>semuatimesheetassesmentJ(int year,int month);
//	
//	@Query("select new com.mipro.dto.joints(b.id, b.status, b.timesheetDate, b.start, b.end, b.overtime, b.startOt , b.endOt, b.activity, "
//			+ "c.name, b.placementId) from timesheetModel b INNER JOIN placementModel d on b.placementId = d.id INNER JOIN clientModel c on c.id = d.clientId "
//			+ "where b.isDelete= false and b.id=?1")
//	joints semuatimesheetassesmentJ1(Long ids);
	
	@Query("select a from timesheetassesmentModel a where a.id=?1")
	timesheetassesmentModel timesheetassesmentbyid(Long ids);
	
	@Modifying
	@Query("update timesheetassesmentModel set isDelete=true where id=?1")
	int deleteflag();
	
	@Modifying
	@Query("update timesheetModel set userApproval=approve where id=?1")
	int user(Long ids);
	
	@Modifying
	@Query("update timesheetModel set userApproval=reject where id=?1")
	int user1(Long ids);
	
}
