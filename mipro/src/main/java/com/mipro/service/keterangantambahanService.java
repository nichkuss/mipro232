package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.dto.bid;
import com.mipro.dto.joinket;
import com.mipro.model.keterangantambahanModel;
import com.mipro.model.referensiModel;
import com.mipro.repository.keterangantambahanRepository;
import com.mipro.repository.referensiRepository;

@Service
@Transactional
public class keterangantambahanService {

	@Autowired
	private keterangantambahanRepository kr;
	
	public List<keterangantambahanModel>semuaketerangantambahan(){
		return kr.semuaketerangantambahan();
	}
	
	public keterangantambahanModel simpanketerangantambahan(keterangantambahanModel keterangantambahanModel) {
		return kr.save(keterangantambahanModel);
	}
	
	public keterangantambahanModel keterangantambahanbyid(Long ids) {
		return kr.keterangantambahanbyid(ids);
	}
	
	public void hapusketerangantambahan(Long ids) {
		kr.deleteflag(ids);
	}
	
	public List<joinket> semuaketeranganJ(Long ids){
		return kr.semuaketeranganJ(ids);
	}
	
	public Long biodataid(Long ids) {
		return kr.biodataid(ids);
	}
	
}
