package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.model.jenjangpendidikanModel;
import com.mipro.model.placementModel;
import com.mipro.repository.jenjangpendidikanRepository;
import com.mipro.repository.placementRepository;

@Service
@Transactional
public class placementService {

	@Autowired
	private placementRepository j;
	
	public 	List<placementModel>semuaplacement(){
		return j.semuaplacement();
	}
}
