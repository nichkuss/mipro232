package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.model.religionModel;
import com.mipro.repository.religionRepository;

@Service
@Transactional
public class religionService {
	
	@Autowired
	private religionRepository rr;
	
	//by custom
	public List<religionModel> semuareligion(){
		return rr.semuareligion();
	}
	
	//save by jpa
	public religionModel simpanreligion(religionModel religionModel) {
		return rr.save(religionModel);
	}
	
	public religionModel religionbyid(long ids) {
		return rr.religionbyid(ids);
	}
	
	public void hapusreligion(long ids) {
		rr.deleteflag(ids);
	}

}
