package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.model.roleModel;
import com.mipro.repository.roleRepository;

@Service
@Transactional
public class roleService {
	
	@Autowired
	private roleRepository rr;
	
	//by custom
	public List<roleModel> semuarole(){
		return rr.semuarole();
	}
	
	//save by jpa
	public roleModel simpanrole(roleModel roleModel) {
		return rr.save(roleModel);
	}
	
	public roleModel rolebyid(long ids) {
		return rr.rolebyid(ids);
	}
	
	public void hapusrole(long ids) {
		rr.deleteflag(ids);
	}
	
	public String cekname(String name) {
		return rr.cekname(name);
	}
	
	public String cekcode(String code) {
		return rr.cekcode(code);
	}

}
