package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.dto.joinlevelId;
import com.mipro.dto.joints;
import com.mipro.model.pendidikanModel;
import com.mipro.model.timesheetModel;
import com.mipro.model.timesheetassesmentModel;
import com.mipro.repository.pendidikanRepository;
import com.mipro.repository.timesheetRepository;
import com.mipro.repository.timesheetassesmentRepository;

@Service
@Transactional
public class timesheetService {

	@Autowired
	private timesheetRepository j;
	
//	public 	List<timesheetModel>semuatimesheet(){
//		return j.semuatimesheet();
//	}
	
	public List<joints>semuatimesheet(int year,int month){
		return j.semuatimesheet(year, month);
	}
	
	public joints semuatimesheetassesmentJ1(Long ids){
		return j.semuatimesheetassesmentJ1(ids);
	}
	
	public int submit(Long id, Integer Bulan, Integer tahun) {
		return j.submit(id, Bulan, tahun);
	}
	
	public int tolak(Long id, Integer Bulan, Integer tahun) {
		return j.tolak(id, Bulan, tahun);
	}
	
}
