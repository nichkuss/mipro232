package com.mipro.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class basisController 
{
	@RequestMapping("/")
	public String awal()
	{
		return "awal";
	}
	
	@RequestMapping("/pelamar")
	public String pelamar()
	{
		return "pelamar";
	}
	
	@RequestMapping("/modalDetailPelamar")
	public String modalDetailPelamar()
	{
		return "modalDetailPelamar";
	}
}
