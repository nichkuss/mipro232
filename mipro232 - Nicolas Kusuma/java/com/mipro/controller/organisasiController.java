package com.mipro.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mipro.model.organisasiModel;
import com.mipro.service.organisasiService;

@Controller
public class organisasiController 
{
	@Autowired
	private organisasiService os;
	
	@RequestMapping("organisasi")
	public String organisasi()
	{
		return "organisasi/organisasi";
	}
	
	@RequestMapping("organisasiList")
	public String organisasiList(Model model)
	{
		List <organisasiModel> semuaOrganisasi = os.semuaOrganisasi();
		model.addAttribute("semuaOrganisasi", semuaOrganisasi);
		return "organisasi/organisasiList";
	}
	
	@RequestMapping("organisasiAdd")
	public String organisasiAdd(Model model)
	{
		isiTahunMasuk(model);
		isiTahunKeluar(model);
		return "organisasi/organisasiAdd";
	}
	
	@ResponseBody
	@RequestMapping("simpanOrganisasi")
	public String simpanOrganisasi(@ModelAttribute() organisasiModel organisasiModel)
	{
		long user = 55415052;
		organisasiModel.setCreatedBy(user);
		
		Timestamp waktuSekarang = new Timestamp(System.currentTimeMillis());
		organisasiModel.setCreatedOn(waktuSekarang);
		
		os.simpanOrganisasi(organisasiModel);
		return "";
	}
	
	public void isiTahunMasuk(Model model) 
	{
		List <Integer> tahunMasuk = new ArrayList();
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		
		//dimulai dari 1990 sampai tahun sekarang
		for (int i = year; i > 1990; i--) 
		{
			tahunMasuk.add(i);
		}
		model.addAttribute("tahunMasuk", tahunMasuk);
	}
	
	public void isiTahunKeluar(Model model)
	{
		List <Integer> tahunKeluar = new ArrayList();
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		
		//dimulai dari 1990 sampai tahun sekarang
		for(int i = year; i > 1990; i--)
		{
			tahunKeluar.add(i);
		}
		model.addAttribute("tahunKeluar", tahunKeluar);
	}
	
	
	@RequestMapping("organisasiEdit/{id}")
	public String organisasiEdit(Model model, @PathVariable(name = "id") int id)
	{
		organisasiModel organisasiById = os.organisasiById(id);
		model.addAttribute("organisasiById", organisasiById);
		return "organisasi/organisasiEdit";
	}
	
	@ResponseBody
	@RequestMapping("ubahOrganisasi")
	public String ubahOrganisasi(@ModelAttribute() organisasiModel organisasiModel, Model model)
	{
		//panggil created by dan created on
		organisasiModel organisasiById = os.organisasiById(organisasiModel.getId());
		organisasiModel.setCreatedBy(organisasiById.getCreatedBy());
		organisasiModel.setCreatedOn(organisasiById.getCreatedOn());
		
		long user = 55415052;
		organisasiModel.setModifiedBy(user);
		
		Timestamp waktuSekarang = new Timestamp(System.currentTimeMillis());
		organisasiModel.setModifiedOn(waktuSekarang);
		os.simpanOrganisasi(organisasiModel);
		return "";
	}
}