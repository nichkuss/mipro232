package com.mipro.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "x_organisasi")
public class organisasiModel 
{
	@Id
	@NotNull
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", length = 11, nullable = false)
	private long id;
	
	@Column(name = "created_by", length = 11, nullable = false)
	private long createdBy;
	
	@Column(name = "created_on", nullable = false)
	private Timestamp createdOn;
	
	@Column(name = "modified_by", length = 11, nullable = true)
	private long modifiedBy;
	
	@Column(name = "modified_on", nullable = true)
	private Timestamp modifiedOn;
	
	@Column(name = "deleted_by", length = 11, nullable = true)
	private long deletedBy;
	
	@Column(name = "deleted_on", nullable = true)
	private Timestamp deletedOn;
	
	@Column(name = "is_delete", nullable = false)
	private boolean isDelete;
	
	@Column(name = "biodata_id", length = 11, nullable = false)
	private long biodataId;
	
	@Column(name = "name", length = 100, nullable = true)
	private String name;
	
	@Column(name = "position", length = 100, nullable = true)
	private String position;
	
	@Column(name = "entry_year", length = 10, nullable = true)
	private String entryYear;
	
	@Column(name = "exit_year", length = 10, nullable = true)
	private String exitYear;
	
	@Column(name = "responsbility", length = 100, nullable = true)
	private String responsbility;
	
	@Column(name = "notes", length = 1000, nullable = true)
	private String notes;

	public long getId() 
	{
		return id;
	}

	public void setId(long id) 
	{
		this.id = id;
	}

	public long getCreatedBy() 
	{
		return createdBy;
	}

	public void setCreatedBy(long createdBy) 
	{
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedOn() 
	{
		return createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) 
	{
		this.createdOn = createdOn;
	}

	public long getModifiedBy() 
	{
		return modifiedBy;
	}

	public void setModifiedBy(long modifiedBy) 
	{
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedOn() 
	{
		return modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) 
	{
		this.modifiedOn = modifiedOn;
	}

	public long getDeletedBy() 
	{
		return deletedBy;
	}

	public void setDeletedBy(long deletedBy) 
	{
		this.deletedBy = deletedBy;
	}

	public Timestamp getDeletedOn() 
	{
		return deletedOn;
	}

	public void setDeletedOn(Timestamp deletedOn) 
	{
		this.deletedOn = deletedOn;
	}

	public boolean isDelete() 
	{
		return isDelete;
	}

	public void setDelete(boolean isDelete) 
	{
		this.isDelete = isDelete;
	}

	public long getBiodataId() 
	{
		return biodataId;
	}

	public void setBiodataId(long biodataId) 
	{
		this.biodataId = biodataId;
	}

	public String getName() 
	{
		return name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}

	public String getPosition() 
	{
		return position;
	}

	public void setPosition(String position) 
	{
		this.position = position;
	}

	public String getEntryYear() 
	{
		return entryYear;
	}

	public void setEntryYear(String entryYear) 
	{
		this.entryYear = entryYear;
	}

	public String getExitYear() 
	{
		return exitYear;
	}

	public void setExitYear(String exitYear) 
	{
		this.exitYear = exitYear;
	}

	public String getResponsbility() 
	{
		return responsbility;
	}

	public void setResponsbility(String responsbility) 
	{
		this.responsbility = responsbility;
	}

	public String getNotes() 
	{
		return notes;
	}

	public void setNotes(String notes) 
	
	{
		this.notes = notes;
	}
}
