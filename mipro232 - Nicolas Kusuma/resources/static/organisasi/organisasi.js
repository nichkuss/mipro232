organisasiList();

function organisasiList() 
{
	$.ajax({
		url : '/organisasiList',
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#organisasiList').html(result);
		}
	});
}

function addForm() 
{
//	$.ajax({
//		url : '/organisasiAdd',
//		type : 'get',
//		dataType : 'html',
//		success : function(result) 
//		{
//			$('#organisasiList').html(result);	
//		}
//	});
	
	$.ajax({
		url : '/organisasiAdd',
		type : 'get',
		dataType : 'html',
		success : function(result) 
		{
				$('#organisasiModal').modal();
				$('#nonOrganisasiList').html(result);
		}
	});
}

function closeModal()
{
	$('organisasiModal').hide();
	$('.modal-backdrop').hide();
	$('.modal-backdrop').remove();
	organisasiList();
}

function refreshPage()
{
	$.ajax({
		url : '/organisasi',
		type : 'get',
		dataType : 'html',
		success : function(result) {
			closeModal();
			$('#fragment').html(result);
		}
	});
	return false;
}